#ifndef EGREEDY_H
#define EGREEDY_H

#include "experiment.h"

int egreedyAct(kBandit prob);
double egreedyStep(int action, kBandit *prob);
void egreedyFree(kBandit prob);
void egreedyInit(kBandit *prob, double initVal, Distribution dist, double eps);

#endif
