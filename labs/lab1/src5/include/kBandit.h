#ifndef KBANDIT_H
#define KBANDIT_H

#include <gsl/gsl_rng.h>

#include "enums.h"
#include "vectors.h"

typedef struct kBandit kBandit;

typedef int (*Act)(kBandit kb);
typedef double (*Reward)(int action, kBandit kb);
typedef double (*Step)(int action, kBandit *kb);
typedef void (*Free)(kBandit kb);

struct kBandit {
  int k, time;
  double param, avgReward;
  vectorI N, optActions;
  vectorD qstar, qt, pref;
  gsl_rng *r;
  Act act;
  Reward bandit;
  Step step;
  Free free;
};

double uGaussianVariate(double meanShift, gsl_rng *r);
void initRandState(kBandit *kb, int seed);
double rewardGauss(int action, kBandit prob);
double rewardBern(int action, kBandit prob);
void basicInit(kBandit *kb, Act actFn, Step updateFn, Free freeFn,
               Distribution dist);
void basicFree(kBandit prob);

#endif
