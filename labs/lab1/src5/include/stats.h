#ifndef STATS_H
#define STATS_H

#include "../include/kBandit.h"

typedef struct Stats {
  vectorD avgReward;
  vectorI optActionFreq;
} Stats;

Stats initStats(int iters, int reps);
void freeStats(Stats s);
Stats *multipleStats(int iters, int reps);
void freeStatsArr(Stats *s, int len);
void learningStats(int action, double payoff, kBandit problem, Stats s);

#endif
