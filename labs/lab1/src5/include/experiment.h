#ifndef EXPERIMENT_H
#define EXPERIMENT_H

#include "enums.h"
#include "kBandit.h"
#include "stats.h"

typedef struct Experiment Experiment;

struct Experiment {
  int epochs, steps, seed;
  double param, initVal;
  char *fout;
  kBandit *kb;
  Stats *stats;
  Algorithm algo;
  Distribution dist;
};

Experiment makeExperiment(int k, int epochs, int steps, Algorithm algo,
                          Distribution dist, double param, double initVal,
                          char *outfile);
void runExperiment(Experiment *exp);
void freeExperiment(Experiment exp);
void dumpStats(Experiment exp);
/* void printExperiment(Experiment e); */

#endif
