#ifndef LIBUTILS_H
#define LIBUTILS_H

#include <stdlib.h>

void *safeMalloc(size_t size);
void resetIntArr(int until, int val, int *arr);
int compareDouble(double a, double b);
void printIntArr(int len, void *store);
void printDoubleArr(int len, void *store);

#endif
