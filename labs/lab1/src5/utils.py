import os
import multiprocessing as mp


HOME = os.path.dirname(os.path.abspath(__file__))


def multiproc_fn(fn: callable, args: list, workers=6):
    with mp.Pool(workers) as p:
        p.map(fn, args)
        p.close()
        p.join()


def merge_lists_pairwise(*lists) -> list:
    return [elt for t in zip(*lists) for elt in t]
