#!/usr/bin/sh
HOME=$(pwd)

cd src
echo "Compiling source..."
make

echo "Done, installing required python packages..."
pip install --user -r "$HOME/requirements.txt"

cd ../
python pipeline.py

echo "Checking for installed R dependencies..."
/usr/bin/Rscript pkg_installer.R

echo "Starting data analysis"
/usr/bin/Rscript data_analysis.R
