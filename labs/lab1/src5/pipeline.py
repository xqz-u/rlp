#!/usr/bin/python
import os
import subprocess as sp
import itertools as it

import numpy as np

import utils as u
from utils import HOME


src = os.path.join(HOME, "src")
data_dir = os.path.join(HOME, "data")
experiment_source = os.path.join(src, "executable")


def run_experiment(cfg: list):
    print(cfg)
    sp.run([experiment_source, *cfg])


def grid_search_configs() -> list:
    algos = range(1, 5)
    distributions = range(1, 3)
    arms = [10]
    steps = [10000]
    runs = [1000]
    params = np.linspace(0.01, 0.1, num=15)

    configs = [algos, distributions, arms, steps, runs, params]
    options = ["A", "D", "k", "s", "e", "p", "i", "f"]

    return [
        u.merge_lists_pairwise(
            map(lambda o: f"-{o}", options),
            map(
                lambda x: str(x),
                [*comb, 5 if comb[0] == 2 else 0, os.path.join(data_dir, f"{i}.csv")],
            ),
        )
        for i, comb in enumerate(it.product(*configs))
    ]


def main():
    if os.path.exists(data_dir):
        print("Using default iter_data.csv and run_data.csv for data analysis")
        return

    os.makedirs(data_dir)
    print("Starting hyper-parameters exploration...")
    u.multiproc_fn(run_experiment, grid_search_configs())
    print("Done!")


if __name__ == "__main__":
    main()
