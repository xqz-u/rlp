#include <assert.h>

#include "../include/UCB.h"
#include "../include/eGreedy.h"
#include "../include/experiment.h"
#include "../include/rcomp.h"
#include "../include/utils.h"

void dumpStats(Experiment exp) {
  FILE *fp;
  int i, j;
  Stats *s;

  fp = freopen(exp.fout, "w", stdout);
  assert(fp);

  printf("time,step_reward,step_opt_freq,param,algo,dist\n");

  for (i = 0; i < exp.epochs; ++i) {
    s = &exp.stats[i];
    for (j = 0; j < exp.steps; ++j) {
      printf("%d,%.3lf,%d,%.3lf,%d,%d\n", j, s->avgReward.vals[j],
             s->optActionFreq.vals[j], exp.param, exp.algo, exp.dist);
    }
  }

  fclose(fp);
}

void initKbandit(Experiment exp) {
  initRandState(exp.kb, exp.seed);
  switch (exp.algo) {
  case OIV:
  case EGREEDY:
    egreedyInit(exp.kb, exp.initVal, exp.dist, exp.param);
    break;
  case UCB:
    ucbInit(exp.kb, exp.dist, exp.param);
    break;
  case RCOMP:
    rcompInit(exp.kb, exp.dist, exp.param);
    break;
  default:
    perror("Algorithm not implemented!\n");
    exit(1);
  }
}

Experiment makeExperiment(int k, int epochs, int steps, Algorithm algo,
                          Distribution dist, double param, double initVal,
                          char *outfile) {

  kBandit *kb = safeMalloc(sizeof(*kb));
  *kb = (kBandit){.k = k, .time = 0};

  Experiment exp = (Experiment){.epochs = epochs,
                                .steps = steps,
                                .seed = 0,
                                .param = param,
                                .initVal = initVal,
                                .kb = kb,
                                .algo = algo,
                                .dist = dist,
                                .stats = multipleStats(steps, epochs),
                                .fout = outfile};
  initKbandit(exp);
  return exp;
}

void freeExperiment(Experiment exp) {
  freeStatsArr(exp.stats, exp.epochs);
  exp.kb->free(*exp.kb);
  free(exp.kb);
}

void refreshExperiment(Experiment *exp) {
  exp->kb->free(*exp->kb);
  exp->seed += 1;
  initKbandit(*exp);
}

void runAlgo(int steps, kBandit *kb, Stats s) {
  int action;
  double payoff;
  for (kb->time = 0; kb->time < steps; ++kb->time) {
    action = kb->act(*kb);
    payoff = kb->step(action, kb);
    learningStats(action, payoff, *kb, s);
  }
}

void runExperiment(Experiment *exp) {
  int i;
  for (i = 0; i < exp->epochs; ++i) {
    runAlgo(exp->steps, exp->kb, exp->stats[i]);
    refreshExperiment(exp);
  }
}

/* void printExperiment(Experiment e) { */
/*   printf("Algorithm: %s\nDistribution: %s\nk: %d\nsteps: %d\nepochs: " */
/*          "%d\nparam: %.3lf\ninitial value: %.3lf\n", */
/*          algos[e.algo], distname[e.dist], e.kb->k, e.steps, e.epochs,
 * e.param, */
/*          e.initVal); */
/* } */
