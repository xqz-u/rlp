#include "../../include/eGreedy.h"

int egreedyAct(kBandit prob) {
  if (gsl_rng_uniform(prob.r) < prob.param)
    return gsl_rng_uniform_int(prob.r, prob.k);
  vectorI bestIdxs = argmaxD(prob.qt);
  int ret = bestIdxs.vals[gsl_rng_uniform_int(prob.r, bestIdxs.len)];
  free(bestIdxs.vals);
  return ret;
}

double egreedyStep(int action, kBandit *prob) {
  double payoff = prob->bandit(action, *prob);
  int *timesChosen = &prob->N.vals[action];
  double *valueEstimate = &prob->qt.vals[action];

  *timesChosen += 1;
  *valueEstimate += ((payoff - *valueEstimate) / *timesChosen);

  return payoff;
}

void egreedyFree(kBandit prob) {
  free(prob.N.vals);
  free(prob.qt.vals);
  basicFree(prob);
}

void egreedyInit(kBandit *prob, double initVal, Distribution dist, double eps) {
  prob->N = makeVectorI(prob->k);
  prob->param = eps;
  prob->qt =
      initVal == 0 ? makeVectorD(prob->k) : makeVectorDFill(prob->k, initVal);
  basicInit(prob, egreedyAct, egreedyStep, egreedyFree, dist);
}
