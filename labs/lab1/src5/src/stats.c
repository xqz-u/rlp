#include "../include/stats.h"
#include "../include/kBandit.h"
#include "../include/utils.h"

Stats initStats(int iters, int reps) {
  return (Stats){.avgReward = makeVectorD(iters),
                 .optActionFreq = makeVectorI(iters)};
}

void freeStats(Stats s) {
  free(s.avgReward.vals);
  free(s.optActionFreq.vals);
}

Stats *multipleStats(int iters, int reps) {
  int i;
  Stats *s = safeMalloc(reps * sizeof(*s));
  for (i = 0; i < reps; ++i)
    s[i] = initStats(iters, reps);
  return s;
}

void freeStatsArr(Stats *s, int len) {
  int i;
  for (i = 0; i < len; ++i)
    freeStats(s[i]);
  free(s);
}

void learningStats(int action, double payoff, kBandit problem, Stats s) {
  s.avgReward.vals[problem.time] += payoff;
  s.optActionFreq.vals[problem.time] += in(action, problem.optActions);
}
