#include <assert.h>
#include <float.h>
#include <math.h>
#include <stdio.h>

#include "../include/utils.h"

void *safeMalloc(size_t size) {
  void *store = calloc(size, 1);
  assert(store);
  return store;
}

void resetIntArr(int len, int val, int *store) {
  int i;
  for (i = 0; i < len; ++i)
    store[i] = val;
}

/* NOTE makes sense only for small double values; as they get larger, the
 * difference  does too, and epsilon is not a good threshold anymore */
int compareDouble(double a, double b) { return fabs(a - b) < DBL_EPSILON; }

void printIntArr(int len, void *store) {
  printf("[%d", *(int *)store);
  int i;
  for (i = 1; i < len; ++i)
    printf(",%d", *((int *)store + i));
  printf("]\n");
}

void printDoubleArr(int len, void *store) {
  printf("[%.3lf", *(double *)store);
  int i;
  for (i = 1; i < len; ++i)
    printf(",%.3lf", *((double *)store + i));
  printf("]\n");
}
