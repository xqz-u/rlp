#! /usr/bin/python
import os
import subprocess as sp
import itertools as it

import numpy as np

from data_process import process_results
import utils as u
from utils import HOME

exp_src = os.path.join(HOME, "src")


def grid_search_param(exec_path: str):
    algos = range(1, 3)
    distributions = range(1, 3)
    arms = [10]
    steps = [1000]
    runs = [2000]
    params = np.linspace(0.01, 0.1, num=3)

    configs = [algos, distributions, arms, steps, runs, params]
    options = ["A", "D", "k", "s", "e", "p", "i"]

    for comb in it.product(*configs):
        switches = map(lambda o: f"-{o}", options)
        values = map(lambda x: str(x), [*comb, 5 if comb[0] == 3 else 0])
        cfg = u.merge_lists_pairwise(switches, values)
        # print(f"Config: {cfg}")
        sp.run([exec_path, *cfg])


def main():
    print("Starting parameter exploration...")
    grid_search_param(os.path.join(exp_src, "executable"))
    print("Starting data analysis...")
    # process_results()
    print("Done!")


if __name__ == "__main__":
    main()
