import os


HOME = os.path.dirname(os.path.abspath(__file__))


def vectorize_first(fn):
    def inner(first_list, *args, **kwargs):
        return (
            fn(first_list, *args, **kwargs)
            if not hasattr(first_list, "__iter__") or isinstance(first_list, str)
            else [fn(p, *args, **kwargs) for p in first_list]
        )

    return inner


@vectorize_first
def make_dir(full_path):
    if not os.path.exists(full_path):
        os.makedirs(full_path)


# TODO add possibility to enforce args of same length
def dict_from_lists(names, values):
    return dict(zip(names, values))


def extract_dict_values(d: dict, names: list) -> dict:
    return {k: d.pop(k, None) for k in names}


def merge_lists_pairwise(*lists) -> list:
    return [elt for t in zip(*lists) for elt in t]
