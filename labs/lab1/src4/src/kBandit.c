#include <gsl/gsl_randist.h>

#include "../include/kBandit.h"
#include "../include/utils.h"

double uGaussianVariate(double meanShift, gsl_rng *r) {
  return gsl_ran_ugaussian(r) + meanShift;
}

vectorD nUGaussianVariates(int n, double meanShift, gsl_rng *r) {
  double *variates = safeMalloc(n * sizeof(*variates));
  int i;
  for (i = 0; i < n; ++i)
    variates[i] = uGaussianVariate(meanShift, r);
  return bundleVectorD(n, variates);
}

vectorD nUniformVariates(int n, gsl_rng *r) {
  double *variates = safeMalloc(n * sizeof(*variates));
  int i;
  for (i = 0; i < n; ++i)
    variates[i] = gsl_rng_uniform(r);
  return bundleVectorD(n, variates);
}

double rewardGauss(int action, kBandit prob) {
  return uGaussianVariate(prob.qstar.vals[action], prob.r);
}

double rewardBern(int action, kBandit prob) {
  return gsl_rng_uniform(prob.r) < prob.qstar.vals[action];
}

void initRandState(kBandit *kb, int seed) {
  gsl_rng *r = gsl_rng_alloc(gsl_rng_default);
  gsl_rng_set(r, seed);
  kb->r = r;
}

void basicInit(kBandit *kb, Act actFn, Step updateFn, Free freeFn,
               Distribution dist) {
  kb->qstar = dist == Gaussian ? nUGaussianVariates(kb->k, 0, kb->r)
                               : nUniformVariates(kb->k, kb->r);
  kb->optActions = argmaxD(kb->qstar);
  kb->act = actFn;
  kb->bandit = dist == Gaussian ? rewardGauss : rewardBern;
  kb->step = updateFn;
  kb->free = freeFn;
}

void basicFree(kBandit prob) {
  free(prob.qstar.vals);
  free(prob.optActions.vals);
  gsl_rng_free(prob.r);
}
