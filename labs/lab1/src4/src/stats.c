#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "../include/kBandit.h"
#include "../include/stats.h"
#include "../include/utils.h"

char *algos[] = {"EGREEDY", "OIV", "UCB", "RCOMP"};
char *distname[] = {"Gaussian", "Bernoulli"};

Stats initStats(int iters, int reps) {
  return (Stats){.epochReward = 0,
                 .epochActionFreq = 0,
                 .avgReward = makeVectorD(iters),
                 .optActionFreq = makeVectorD(iters),
                 .epochRewards = makeVectorD(reps),
                 .epochActionFreqs = makeVectorD(reps)};
}

void freeStats(Stats s) {
  free(s.avgReward.vals);
  free(s.optActionFreq.vals);
  free(s.epochRewards.vals);
  free(s.epochActionFreqs.vals);
}

void learningStats(int action, double payoff, int time, kBandit problem,
                   Stats *s) {
  s->epochReward += payoff;
  s->epochActionFreq += in(action, problem.optActions);
  s->avgReward.vals[time] += payoff;
  s->optActionFreq.vals[time] += in(action, problem.optActions);
}

void totalReward(Stats *s, int trial) {
  s->epochRewards.vals[trial] = s->epochReward;
  s->epochActionFreqs.vals[trial] = s->epochActionFreq;
  s->epochReward = s->epochActionFreq = 0;
}

void printAverageStats(Stats s, double param, int k, Algorithm algo,
                       Distribution dist) {
  int i, epochs = s.epochRewards.len;
  double meanReward, qArmFreq;
  for (i = 0; i < s.avgReward.len; ++i) {
    meanReward = s.avgReward.vals[i] / epochs;
    qArmFreq = s.optActionFreq.vals[i] / epochs * 100;
    printf("%d,%lf,%lf,%lf,%d,%s,%s\n", i, meanReward, qArmFreq, param, k,
           algos[algo], distname[dist]);
  }
}

void printTotalStats(Stats s, double param, int k, Algorithm algo,
                     Distribution dist) {
  int i, steps = s.optActionFreq.len;
  for (i = 0; i < s.epochRewards.len; ++i)
    printf("%d,%lf,%lf,%lf,%d,%s,%s\n", i, s.epochRewards.vals[i],
           s.epochActionFreqs.vals[i] / steps * 100, param, k, algos[algo],
           distname[dist]);
}

void writeHeader(char *fout, char *header) {
  FILE *fp = freopen(fout, "w", stdout);
  assert(fp);
  printf("%s", header);
  fclose(fp);
}

void appendStatsOut(char *outfile, printer_fn pf, Stats s, double param, int k,
                    Algorithm algo, Distribution dist) {
  FILE *fp = freopen(outfile, "a", stdout);
  assert(fp);
  pf(s, param, k, algo, dist);
  fclose(fp);
}
