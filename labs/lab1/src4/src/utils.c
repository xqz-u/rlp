#include <assert.h>
#include <float.h>
#include <math.h>
#include <stdarg.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "../include/utils.h"

void *safeMalloc(size_t size) {
  void *store = calloc(size, 1);
  assert(store);
  return store;
}

/* NOTE the format specifiers in `fmt` need to match the variadic arguments
 * passed by the caller, and the returned string needs to be freed */
char *strInterpolate(char *fmt, ...) {
  va_list lp;
  va_start(lp, fmt);
  // determine output string length
  size_t needed = vsnprintf(NULL, 0, fmt, lp) + 1;
  assert(needed);
  char *str = safeMalloc(needed);
  va_end(lp);
  // write the string
  va_start(lp, fmt);
  assert(vsprintf(str, fmt, lp));
  va_end(lp);
  return str;
}

void makeDir(char *dirname) {
  struct stat st = {0};
  if (stat(dirname, &st))
    mkdir(dirname, 0777);
}

int exists(char *filename) { return access(filename, F_OK) == 0; }

void printDoubleArr(int len, void *store) {
  printf("[%.3lf", *(double *)store);
  int i;
  for (i = 1; i < len; ++i)
    printf(",%.3lf", *((double *)store + i));
  printf("]\n");
}

void printIntArr(int len, void *store) {
  printf("[%d", *(int *)store);
  int i;
  for (i = 1; i < len; ++i)
    printf(",%d", *((int *)store + i));
  printf("]\n");
}

void printWithHeader(int len, void *store, char *header,
                     void (*printer)(int, void *)) {
  printf("%s", header);
  printer(len, store);
}

void resetIntArr(int len, int val, int *store) {
  int i;
  for (i = 0; i < len; ++i)
    store[i] = val;
}

/* NOTE makes sense only for small double values; as they get larger, the
 * difference  does too, and epsilon is not a good threshold anymore */
int compareDouble(double a, double b) { return fabs(a - b) < DBL_EPSILON; }
