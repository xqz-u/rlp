#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "../include/utils.h"
#include "../include/vectors.h"

/* TODO macro approach for the different types */

vectorD makeVectorD(int sz) {
  return (vectorD){sz, safeMalloc(sz * sizeof(double))};
}

vectorI makeVectorI(int sz) {
  return (vectorI){sz, safeMalloc(sz * sizeof(int))};
}

vectorD makeVectorDFill(int sz, double fillValue) {
  vectorD ret = makeVectorD(sz);
  int i;
  for (i = 0; i < sz; ++i)
    ret.vals[i] = fillValue;
  return ret;
}

vectorI makeVectorIFill(int sz, int fillValue) {
  vectorI ret = makeVectorI(sz);
  int i;
  for (i = 0; i < sz; ++i)
    ret.vals[i] = fillValue;
  return ret;
}

vectorD bundleVectorD(int sz, double *arr) { return (vectorD){sz, arr}; }

vectorI bundleVectorI(int sz, int *arr) { return (vectorI){sz, arr}; }

vectorI bundleVectorI(int sz, int *arr);

void printVectorD(vectorD v) { printDoubleArr(v.len, v.vals); }

void printVectorI(vectorI v) { printIntArr(v.len, v.vals); }

void resizeVectorI(vectorI *vp, int newlen) {
  vp->len = newlen;
  vp->vals = realloc(vp->vals, sizeof *(vp->vals) * newlen);
  assert(vp->vals);
}

vectorI argmaxD(vectorD v) {
  int i, max, strike;
  max = strike = 0;

  vectorI best = makeVectorIFill(v.len, -1);

  for (i = 0; i < v.len; ++i) {
    if (v.vals[i] > v.vals[max]) {
      max = i;
      resetIntArr(strike, -1, best.vals);
      strike = 0;
      best.vals[strike++] = max;
    } else if (compareDouble(v.vals[i], v.vals[max])) {
      best.vals[strike++] = i;
    }
  }
  resizeVectorI(&best, strike);

  return best;
}

double sum(vectorD v) {
  double ret = 0;
  int i;
  for (i = 0; i < v.len; ++i)
    ret += v.vals[i];
  return ret;
}

int in(int what, vectorI v) {
  int i;
  for (i = 0; i < v.len; ++i)
    if (v.vals[i] == what)
      return 1;
  return 0;
}
