#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>

#include "../include/experiment.h"

void printHelp(char *progname) {
  char *usage =
      "-A -D -p -k -s -e -i.\n\t-A\t\tAlgorithm: a value "
      "in "
      "the range [1,4]. Available algorithms: [EGREEDY, OIV, UCB, "
      "RCOMP]\n\t-D\t\tDistribution: a value in the range [1,2]. Available "
      "distributions: [Gaussian, Bernoulli]\n\t-p\t\tParameter: the parameter "
      "to run the algorithms with (epsilon for EGREEDY and OIV, c for UCB, "
      "alpha for "
      "RCOMP)\n\t-k\t\tNumber of arms for the "
      "bandit problem\n\t-s\t\tNumber of steps to run an "
      "algorithm\n\t-e\t\tNumber of epochs an algorithm "
      "should "
      "be repeated\n\t-i\t\tInitial value: initial value "
      "estimate for the "
      "different "
      "arms, supported by EGREEDY and OIV\n";
  printf("Usage: %s %s", progname, usage);
}

/* TODO make getopt work with default options, i.e. using :: syntax on some
 * switch to avoid passing all parameters (or using bash) */
/* TODO error checking on options values (saniification) */
int main(int argc, char **argv) {
  Algorithm algo;
  Distribution dist;
  int k, steps, runs, argsGiven = 0;
  double param, initVal;
  char c, *missError = "Missing value for option", *progname = argv[0];

  while ((c = getopt(argc, argv, ":A:D:p:k:s:e:i:h")) != -1) {
    switch (c) {
    case 'A':
      algo = atoi(optarg) - 1;
      if (algo < 0 || algo > 3) {
        fprintf(stderr, "Algorithm needs to be in range [1,4]\n");
        return 1;
      }
      argsGiven += 1;
      break;
    case 'D':
      dist = atoi(optarg) - 1;
      if (dist < 0 || dist > 1) {
        fprintf(stderr, "Distribution needs to be in range [1,2]\n");
        return 1;
      }
      argsGiven += 1;
      break;
    case 'k':
      k = atoi(optarg);
      argsGiven += 1;
      break;
    case 's':
      steps = atoi(optarg);
      argsGiven += 1;
      break;
    case 'e':
      runs = atoi(optarg);
      argsGiven += 1;
      break;
    case 'p':
      param = atof(optarg);
      argsGiven += 1;
      break;
    case 'i':
      initVal = atof(optarg);
      argsGiven += 1;
      break;
    case 'h':
      printHelp(progname);
      return 0;
    case ':':
      fprintf(stderr, "%s `-%c`\n", missError, optopt);
      return 1;
    case '?':
      fprintf(stderr, "Unknown option `-%c`.\n", optopt);
      return 1;
    default:
      printHelp(progname);
      return 1;
    }
  }

  if (argsGiven < 7) {
    printHelp(progname);
    return 1;
  }

  if (initVal > 0 && algo > 1) {
    fprintf(stderr, "Only EGREEDY and OIV algorithms support an initial "
                    "value estimate\n");
    return 1;
  }

  Experiment exp = makeExperiment(k, runs, steps, algo, dist, param, initVal);
  /* printExperiment(exp); */
  runExperiment(&exp);
  freeExperiment(exp);

  return 0;
}
