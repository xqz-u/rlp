#include <math.h>
#include <stdio.h>

#include "../../include/rcomp.h"

int rcompAct(kBandit prob) {
  double p, sumLogsPrefs, sumProb = 0;
  int i;

  vectorD logsPref = makeVectorD(prob.k);
  for (i = 0; i < prob.k; ++i)
    logsPref.vals[i] = exp(prob.pref.vals[i]);

  sumLogsPrefs = sum(logsPref);
  p = gsl_rng_uniform(prob.r);

  for (i = 0; i < prob.k; i = (i + 1) % prob.k) {
    sumProb += logsPref.vals[i] / sumLogsPrefs;
    if (p <= sumProb)
      break;
  }

  free(logsPref.vals);
  return i;
}

double rcompStep(int action, kBandit *prob) {
  double payoff = prob->bandit(action, *prob);
  double *actionPref = &prob->pref.vals[action];
  double rewardDiff = payoff - prob->avgReward;

  *actionPref += (prob->param * rewardDiff);
  prob->avgReward += (prob->param * rewardDiff);

  return payoff;
}

void rcompFree(kBandit prob) {
  prob.avgReward = 0;
  free(prob.pref.vals);
  basicFree(prob);
}

void rcompInit(kBandit *prob, Distribution dist, double alpha) {
  prob->param = alpha;
  prob->avgReward = 0;
  prob->pref = makeVectorD(prob->k);
  basicInit(prob, rcompAct, rcompStep, rcompFree, dist);
}
