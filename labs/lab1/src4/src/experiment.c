#include "../include/experiment.h"
#include "../include/UCB.h"
#include "../include/eGreedy.h"
#include "../include/rcomp.h"
#include "../include/utils.h"

char *data_dir =
    "/home/marcog/Desktop/uni/thirdYear/block-1b/RLP/labs/lab1/src4/data";
char *avg_filename = "/home/marcog/Desktop/uni/thirdYear/block-1b/RLP/labs/"
                     "lab1/src4/data/avg_stats.csv";
char *tot_filename = "/home/marcog/Desktop/uni/thirdYear/block-1b/RLP/labs/"
                     "lab1/src4/data/tot_stats.csv";
char *avg_header = "time,mean_reward,opt_arm_freq,param,k,algo,dist\n";
char *tot_header = "epoch,total_reward,total_opt_arm_freq,param,k,algo,dist\n";

void initKbandit(Experiment exp) {
  initRandState(exp.kb, exp.seed);
  switch (exp.algo) {
  case OIV:
  case EGREEDY:
    egreedyInit(exp.kb, exp.initVal, exp.dist, exp.param);
    break;
  case UCB:
    ucbInit(exp.kb, exp.dist, exp.param);
    break;
  case RCOMP:
    rcompInit(exp.kb, exp.dist, exp.param);
    break;
  default:
    perror("Algorithm not implemented!\n");
    exit(1);
  }
}

Experiment makeExperiment(int k, int epochs, int steps, Algorithm algo,
                          Distribution dist, double param, double initVal) {

  kBandit *kb = safeMalloc(sizeof(*kb));
  *kb = (kBandit){.k = k, .time = 0};

  Stats *s = safeMalloc(sizeof(*s));
  *s = initStats(steps, epochs);

  Experiment exp = (Experiment){.epochs = epochs,
                                .steps = steps,
                                .seed = 0,
                                .param = param,
                                .initVal = initVal,
                                .kb = kb,
                                .algo = algo,
                                .dist = dist,
                                .stats = s};
  initKbandit(exp);
  makeDir(data_dir);
  return exp;
}

void freeExperiment(Experiment exp) {
  freeStats(*exp.stats);
  free(exp.stats);
  exp.kb->free(*exp.kb);
  free(exp.kb);
}

void refreshExperiment(Experiment *exp) {
  exp->kb->free(*exp->kb);
  exp->seed += 1;
  initKbandit(*exp);
  exp->kb->time = 0;
}

void runAlgo(int steps, kBandit *kb, Stats *s) {
  int i, action;
  double payoff;
  for (i = 0; i < steps; ++i) {
    action = kb->act(*kb);
    payoff = kb->step(action, kb);
    learningStats(action, payoff, kb->time, *kb, s);
    kb->time += 1;
  }
}

void runExperiment(Experiment *exp) {
  int i;
  for (i = 0; i < exp->epochs; ++i) {
    runAlgo(exp->steps, exp->kb, exp->stats);
    totalReward(exp->stats, i);
    refreshExperiment(exp);
  }
  printStats(exp, avg_filename, avg_header, printAverageStats);
  printStats(exp, tot_filename, tot_header, printTotalStats);
}

void printStats(Experiment *e, char *statsfile, char *header, printer_fn pfn) {
  if (!exists(statsfile))
    writeHeader(statsfile, header);
  appendStatsOut(statsfile, pfn, *e->stats, e->param, e->kb->k, e->algo,
                 e->dist);
}

void printExperiment(Experiment e) {
  printf("Algorithm: %s\nDistribution: %s\nk: %d\nsteps: %d\nepochs: "
         "%d\nparam: %.3lf\ninitial value: %.3lf\n",
         algos[e.algo], distname[e.dist], e.kb->k, e.steps, e.epochs, e.param,
         e.initVal);
}
