#! /usr/bin/python
import os
import json

import pandas as pd
import matplotlib.pyplot as plt

import utils as u
from utils import HOME


data_dir = os.path.join(HOME, "data.d")
plots_dir, tables_dir = [
    os.path.join(HOME, dirname) for dirname in ["plots", "tex_dumps"]
]


def parse_algo_data() -> list:
    return [
        pd.read_csv(os.path.join(data_dir, fname))
        for fname in os.listdir(data_dir)
        if fname.endswith(".csv")
    ]


def plot_save_fig(df: pd.DataFrame, filename: str, debug=False, **kwargs):
    ax_info = u.extract_dict_values(kwargs, ["title", "xlabel", "ylabel"])
    fig_info = u.extract_dict_values(kwargs, ["suptitle"])

    fig, ax = plt.subplots()
    fig.suptitle(fig_info["suptitle"])

    df.plot(ax=ax)
    ax.set(**ax_info)

    fig.savefig(filename)
    if debug:
        plt.show()


def plots_by_distr(df: pd.DataFrame, colname: str, props: dict, **kwargs):
    for i, distr in enumerate(df["dist"].unique()):
        data = df[df["dist"] == distr]
        metric_data = pd.pivot_table(data, values=colname, **props)
        figname = f"{colname}_{distr}.png"
        plot_save_fig(
            metric_data,
            os.path.join(HOME, "plots", figname),
            **{"suptitle": f"{kwargs['ylabel']} ({distr} distribution)", **kwargs},
        )


def make_plots(plot_data: pd.DataFrame, debug=False, **kwargs):
    y_axes = ["Mean reward", "Optimal action selection %"]
    pivot_table_info = {"index": "time", "columns": "algo", "aggfunc": "mean"}
    for metric_col, yname in zip(["mean_reward", "opt_arm_freq"], y_axes):
        plots_by_distr(
            plot_data,
            metric_col,
            pivot_table_info,
            debug=debug,
            **{"ylabel": yname, **kwargs},
        )


def make_tables(table_data: pd.DataFrame, debug=False):
    aggregations = {
        "Mean total reward": ("total_reward", "mean"),
        "Total reward stderr": ("total_reward", "sem"),
        "Mean optimal action selection %": ("total_opt_arm_freq", "mean"),
    }
    for distr in ["Gaussian", "Bernoulli"]:
        df = table_data[table_data["dist"] == distr]
        table = df.groupby("algo").agg(**aggregations)
        if debug:
            print(table)
        table.to_latex(
            buf=os.path.join(HOME, "tex_dumps", f"{distr}_table.tex"),
            caption=f"Algorithm results by epoch with {distr} distribution",
            label=f"{distr}_table",
        )


def save_optimal_params(df: pd.DataFrame, filename: str, debug=False) -> pd.DataFrame:
    by_algo_param = df.groupby(["algo", "param", "dist"]).agg(
        Mean_payoff=("total_reward", "mean"),
        Mean_best_select=("total_opt_arm_freq", "mean"),
    )
    zscores = by_algo_param.apply(lambda x: (x - x.mean()) / x.std(), axis=0)
    zscores_mean = zscores[["Mean_payoff", "Mean_best_select"]].mean(axis=1)
    for col in ["EGREEDY", "UCB", "OIV"]:
        zscores_mean[col]["Bernoulli"].plot()
        plt.show()


def get_k_epochs(df: pd.DataFrame) -> tuple:
    return (
        df["k"].unique()[0],
        df[df["algo"] == "EGREEDY"][df["dist"] == "Gaussian"].shape[0],
    )


def process_results():
    u.make_dir([plots_dir, tables_dir])
    tot_data, avg_data = parse_algo_data()
    save_optimal_params(tot_data, os.path.join(HOME, "optimal_params.json"), True)
    k, epochs = get_k_epochs(tot_data)
    plot_info = {
        "xlabel": "time",
        "title": f"k = {k}, averages over {epochs} runs",
    }
    make_plots(avg_data, **plot_info)
    make_tables(tot_data, True)
