#! /usr/bin/sh
HOME=$(pwd)

cd src
echo "Compiling source..."
make
echo "Done, installing required python packages..."
pip install --user -r "$HOME/requirements.txt"
cd ../
python pipeline.py
