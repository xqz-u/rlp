#ifndef EXPERIMENT_H
#define EXPERIMENT_H

#include "enums.h"
#include "kBandit.h"
#include "stats.h"

typedef struct Experiment Experiment;

struct Experiment {
  int epochs, steps, seed;
  double param, initVal;
  kBandit *kb;
  Stats *stats;
  Algorithm algo;
  Distribution dist;
};

Experiment makeExperiment(int k, int epochs, int steps, Algorithm algo,
                          Distribution dist, double param, double initVal);
void runExperiment(Experiment *exp);
void printStats(Experiment *e, char *statsfile, char *header, printer_fn pfn);
void freeExperiment(Experiment exp);
void printExperiment(Experiment e);

#endif
