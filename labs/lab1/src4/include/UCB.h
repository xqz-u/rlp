#ifndef UCB_H
#define UCB_H

#include "experiment.h"

int ucbAct(kBandit prob);
void ucbInit(kBandit *prob, Distribution dist, double c);

#endif
