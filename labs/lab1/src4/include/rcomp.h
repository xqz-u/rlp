#ifndef RCOMP_H
#define RCOMP_H

#include "experiment.h"

int rcompAct(kBandit prob);
double rcompStep(int action, kBandit *prob);
void rcompFree(kBandit prob);
void rcompInit(kBandit *prob, Distribution dist, double alpha);

#endif
