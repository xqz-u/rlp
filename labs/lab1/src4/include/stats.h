#ifndef STATS_H
#define STATS_H

#include "../include/kBandit.h"

typedef struct Stats {
  double epochReward, epochActionFreq;
  vectorD avgReward, optActionFreq, epochRewards, epochActionFreqs;
} Stats;

typedef void (*printer_fn)(Stats, double, int, Algorithm, Distribution);

extern char *algos[], *distname[];

Stats initStats(int iters, int reps);
void freeStats(Stats s);
void learningStats(int action, double payoff, int time, kBandit problem,
                   Stats *s);
void totalReward(Stats *s, int trial);
void printAverageStats(Stats s, double param, int k, Algorithm algo,
                       Distribution dist);
void printTotalStats(Stats s, double param, int k, Algorithm algo,
                     Distribution dist);
void writeHeader(char *fout, char *header);
void appendStatsOut(char *outfile, printer_fn pf, Stats s, double param, int k,
                    Algorithm algo, Distribution dist);

#endif
