#ifndef ENUMS_H
#define ENUMS_H

typedef enum { EGREEDY, OIV, UCB, RCOMP } Algorithm;

typedef enum { Gaussian, Bernoulli } Distribution;

#endif
