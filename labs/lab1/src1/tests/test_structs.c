#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef struct Dummy {
  int val1;
  double val2;
} Dummy;

typedef struct Dummier {
  int len;
  Dummy *dummies;
} Dummier;

void printAction(Dummy action) {
  printf("(%.2f, %d)", action.val2, action.val1);
}

Dummy *makeDummies(int many) {
  Dummy *store = calloc(many, sizeof(*store));
  for (int i = 0; i < many; ++i)
    store[i] = (Dummy){.val1 = 0, .val2 = 0};
  return store;
}

void freeDummier(Dummier d) { free(d.dummies); }

void printDummies(Dummy *d, int len) {
  printAction(d[0]);
  for (int i = 1; i < len; ++i) {
    printf(", ");
    printAction(d[i]);
  }
}

void printKbandit(Dummier problem) {
  printf("<K: %d>[", problem.len);
  printDummies(problem.dummies, problem.len);
  printf("]\n");
}

void dummierModify(Dummier *d) {
  for (int i = 0; i < d->len; ++i) {
    d->dummies[i].val1 += 1;
    d->dummies[i].val2 += random() % d->len;
  }
}

void dummierModify2(Dummier d) {
  for (int i = 0; i < d.len; ++i) {
    d.dummies[i].val1 += 1;
    d.dummies[i].val2 += random() % d.len;
  }
}

void dummiesModify(Dummy *store, int len) {
  for (int i = 0; i < len; ++i) {
    store[i].val1 += 1;
    store[i].val2 += random() % len;
  }
}

void test() {
  int len = 10;
  Dummy *dStore = makeDummies(len);
  Dummier d = (Dummier){.len = len, .dummies = makeDummies(len)};

  printf("Dummier b4 modify: ");
  printKbandit(d);
  dummierModify2(d);
  printf("After: ");
  printKbandit(d);

  printf("Dummies b4 modify: ");
  printDummies(dStore, len);
  dummiesModify(dStore, len);
  printf("\n");
  printf("After: ");
  printDummies(dStore, len);
  printf("\n");

  free(dStore);
  freeDummier(d);
}

Dummy *changeDummyP(Dummier d) { return &d.dummies[d.len - 1]; }

void test1() {
  int len = 10;
  Dummier d = (Dummier){.len = len, .dummies = makeDummies(len)};
  dummierModify2(d);
  printKbandit(d);
  Dummy *dp = &d.dummies[4];
  printAction(*dp);
  printf("\n");
  dp = changeDummyP(d);
  printAction(*dp);
  printf("\n");
  freeDummier(d);
}

int main(void) {
  srand(time(NULL));
  test1();
  return 0;
}
