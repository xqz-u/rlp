#include <assert.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

void *safeMalloc(size_t size) {
  void *store = calloc(size, 1);
  if (!store) {
    perror("Fatal error: safeMalloc failed");
    exit(EXIT_FAILURE);
  }
  return store;
}

char *strInterpolate(char *fmt, ...) {
  va_list lp;
  va_start(lp, fmt);
  // determine output string length
  size_t needed = vsnprintf(NULL, 0, fmt, lp) + 1;
  assert(needed);
  char *str = safeMalloc(needed);
  va_end(lp);
  // write the string
  va_start(lp, fmt);
  assert(vsprintf(str, fmt, lp));
  va_end(lp);
  return str;
}

void test(char *mode) {
  FILE *fp = freopen("example.css", mode, stdout);
  assert(fp);
  printf("belllaaaaaaa\n");
  fclose(fp);
}

void test1() {
  int a = 8;
  char *s = "ciao";
  char *out = strInterpolate("d: %d s: %s", a, s);
  char *pippo = strInterpolate("%s_%s.css", "mimmo", "PIPPO");
  printf("%s\n%s\n", out, pippo);
  free(out);
  free(pippo);
}

int main(void) {
  /* test("w"); */
  test1();
  return 0;
}
