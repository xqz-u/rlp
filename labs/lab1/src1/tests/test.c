#define _GNU_SOURCE
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void test() {
  int *arr = calloc(10, sizeof(*arr));
  arr[0] = 15;
  arr[2] = 14;
  int *p1, **p2;
  p1 = &arr[0];
  printf("%d %p %p\n", *p1, p1, &p1);
  p2 = &p1;
  printf("%d %p %p\n", **p2, *p2, p2);
  p1 = &arr[2];
  printf("%d %p - %p - %d %p\n", *p1, p1, *p2, **p2, p2);
  free(arr);
}

void printIntArray(int *store, int len) {
  int i;
  printf("[");
  for (i = 0; i < len; ++i)
    printf("%d ", store[i]);
  printf("]\n");
}

int *randArray(int len) {
  int i, *ret = calloc(len, sizeof(*ret));
  for (i = 0; i < len; ++i)
    ret[i] = rand() % len;
  return ret;
}

int *dummySelect(int *store, int len, int *best) {
  if (!best || rand() % 4)
    return &store[rand() % len];
  return best;
}

void transform(int *var, int range) { *var += rand() % range; }

int *update(int *opt, int *any) { return (!opt || *any > *opt) ? any : opt; }

void test2() {
  srand(time(NULL));
  int i, len = 10, iters = len;
  int *test = randArray(len);
  int *curr, *best = NULL;
  printf("original: ");
  printIntArray(test, len);
  for (i = 0; i < iters; ++i) {
    curr = dummySelect(test, len, best);
    printf("curr: %d (%p), best address: %p\n", *curr, curr, best);
    transform(curr, 28);
    printIntArray(test, len);
    best = update(best, curr);
    /* if (!best || *curr > *best) { */
    /*   printf("UPDATE %p\n", best); */
    /*   best = curr; */
    /* } */
    printf("best is: %d (%p)\n", *best, best);
    printf("--------\n");
  }
  free(test);
}

void test_asprintf(char *name) {
  char *string;
  asprintf(&string, "%s.css", name);
  printf("%s\n", string);
  FILE *fp = freopen(string, "w", stdout);
  free(string);
  assert(fp);
  printf("this printing will go to file!\n");
  fclose(fp);
}

void test3() {
  int a = 6, b = 10;
  int *curr = &a, *best = NULL;
  printf("%p %p\n", &a, curr);
  best = update(best, curr);
  update(best, curr);
  printf("best %p %d\n", best, *best);
  curr = &b;
  best = update(best, curr);
  printf("best %p %d\n", best, *best);
}

void test4() {
  double x = 0, y = 0, len = 10;
  for (int i = 1; i <= len; ++i) {
    printf("x %lf\n", x);
    x += (1.0 / i) * (len + i - x);
    printf("y %lf\n", y);
    y = y + (1.0 / i) * (len + i - y);
  }
}

void myfree(int *a) {
  printf("%p\n", a);
  free(a);
}

void my_realloc(int **a, int sz) { *a = realloc(*a, 2 * sz); }

void test5() {
  int *mimmo = malloc(10 * sizeof(*mimmo));
  printf("%p\n", mimmo);
  mimmo[1] = 8;
  my_realloc(&mimmo, 10);
  printf("After realloc\n");
  myfree(mimmo);
  printf("After free\n");
}

int main(void) {
  /* test1(); */
  /* test2(); */
  /* test3(); */
  /* test4(); */
  test5();
  /* test_asprintf("test_file"); */
  return 0;
}
