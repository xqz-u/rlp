#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>

#include "../include/utils.h"

int main(int argc, char **argv) {
  int algos[4] = {0, 0, 0, 0}, chosenAlgo;
  char dist[2][1];
  int *epsilons = safeMalloc(3 * sizeof(*epsilons));
  int k, epochs, runs;

  int index;
  char c, *opts = "A:d:E:k:e:r:h";

  while ((c = getopt(argc, argv, opts)) != -1) {
    switch (c) {
    case 'A':
      printf("Algo: %s\n", optarg);
      chosenAlgo = strtol(optarg, NULL, 10);
      if (chosenAlgo < 1 && chosenAlgo > 4) {
        fprintf(stderr, "Algorithm needs to be in range [1, 4]\n");
        return 1;
      }
      if (algos[chosenAlgo])
        break;
      algos[chosenAlgo - 1] += 1;
      break;
    default:
      fprintf(stderr, "USAGE\n");
      return 1;
    }
  }

  for (index = 0; index < 4; ++index)
    if (algos[index])
      printf("chosen algo %d\n", index + 1);

  for (index = optind; index < argc; index++)
    printf("Non-option argument %s\n", argv[index]);

  free(epsilons);

  return 0;
}
