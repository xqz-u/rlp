#ifndef LIBKBANDIT_H
#define LIBKBANDIT_H

#include <gsl/gsl_rng.h>

typedef struct Arm {
  double q, qStar;
  int times;
} Arm;

typedef struct Kbandit {
  int k;
  gsl_rng *randomGen;
  Arm *actions;
} Kbandit;

Kbandit initKbandit(int k, int seed);
void freeKbandit(Kbandit problem);
void printKbandit(Kbandit problem);

#endif
