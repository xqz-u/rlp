#ifndef LIBEGREEDY_H
#define LIBEGREEDY_H

#include "libKbandit.h"
#include "libStats.h"

typedef struct Runner {
  int epochs, runs, nEps;
  Kbandit problem;
  StatObject *stats, *currStats;
} Runner;

Runner initRunner(int nArms, int iterations, int repetitions, int nEps,
                  double *epsilons, int seed);
void freeRunner(Runner r);
void refreshRunner(Runner *runner, int newSeed);
void dumpCSV(char *filename, Runner r);

#endif
