#ifndef LIBSTATS_H
#define LIBSTATS_H

typedef struct StatItem {
  double expectedReward;
  int qArmFreq;
} StatItem;

typedef struct StatObject {
  StatItem *epochStats;
  double eps, *trialStats;
} StatObject;

typedef void (*statsPrinter)(StatObject, int, int);

/* TODO instead of exposing the `statsPrinter` function, export an `enum` that
 * maps to them */

StatObject *makeStats(int iters, int reps, int nEps, double *epsilons);
void freeStatObject(StatObject s);
void printAverageStats(StatObject stats, int iters, int reps);
void printTotalStats(StatObject stats, int iters, int reps);
void withFnDumpCSV(statsPrinter fn, StatObject *s, int iters, int reps,
                   int nEps, char *header, char *outfile);

#endif
