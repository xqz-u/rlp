#ifndef LIBUTILS_H
#define LIBUTILS_H

#include <stdarg.h>

#include "stdlib.h"

void *safeMalloc(size_t size);
/* char *strInterpolate(char *fmt, ...); */
void makeDir(char *dirname);

#endif
