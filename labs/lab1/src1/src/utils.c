#include <assert.h>
#include <stdarg.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "../include/utils.h"

void *safeMalloc(size_t size) {
  void *store = calloc(size, 1);
  assert(store);
  return store;
}

/* NOTE the format specifiers in `fmt` need to match the variadic arguments
 * passed by the caller, and the returned string needs to be freed */
char *strInterpolate(char *fmt, ...) {
  va_list lp;
  va_start(lp, fmt);
  // determine output string length
  size_t needed = vsnprintf(NULL, 0, fmt, lp) + 1;
  assert(needed);
  char *str = safeMalloc(needed);
  va_end(lp);
  // write the string
  va_start(lp, fmt);
  assert(vsprintf(str, fmt, lp));
  va_end(lp);
  return str;
}

void makeDir(char *dirname) {
  struct stat st = {0};
  if (stat(dirname, &st))
    mkdir(dirname, 0777);
}
