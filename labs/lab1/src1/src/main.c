#include <gsl/gsl_randist.h>
#include <gsl/gsl_rng.h>
#include <stdio.h>

#include "../include/libeGreedy.h"

Arm *chooseAction(Arm *bestAction, Runner runner, int iter) {
  gsl_rng *r = runner.problem.randomGen;
  if (!bestAction || (gsl_rng_uniform(r) <= runner.currStats->eps))
    return &runner.problem.actions[gsl_rng_uniform_int(r, runner.problem.k)];
  /* runner.currStats->epochStats[iter].qArmFreq += 1; */
  return bestAction;
}

double bandit(gsl_rng *r, double meanShift) {
  return gsl_ran_ugaussian(r) + meanShift;
}

void newactionEstimate(Arm *action, double reward, Runner runner, int iter) {
  action->times += 1;
  action->q += (1.0 / action->times) * (reward - action->q);
  runner.currStats->epochStats[iter].expectedReward += action->q;
}

void eGreedy(Runner runner) {
  Arm *action, *qAction = NULL;
  double reward;
  int i;
  for (i = 0; i < runner.epochs; ++i) {
    action = chooseAction(qAction, runner, i);
    reward = bandit(runner.problem.randomGen, action->qStar);
    newactionEstimate(action, reward, runner, i);
    if (!qAction || (action->q > qAction->q))
      qAction = action;
  }
}

void totalReward(Runner r, int trial) {
  int i;
  for (i = 0; i < r.epochs; ++i)
    r.currStats->trialStats[trial] += r.currStats->epochStats[i].expectedReward;
}

void runExperiment(int nActions, int iterations, int repetitions,
                   int epsilonLen, double *epsilon, char *fout) {
  int i, j, seed = 0;
  Runner runner =
      initRunner(nActions, iterations, repetitions, epsilonLen, epsilon, seed);
  for (i = 0; i < epsilonLen; ++i) {
    /* runner.currStats = &runner.stats[i]; */
    for (j = 0; j < repetitions; ++j) {
      eGreedy(runner);
      totalReward(runner, j);
      refreshRunner(&runner, ++seed);
    }
    runner.currStats = &runner.stats[(i + 1) % epsilonLen];
  }
  dumpCSV(fout, runner);
  freeRunner(runner);
}

void test(double *epsilon) {
  runExperiment(10, 1000, 1, 3, epsilon, "eGreedy_stats_test");
}

int main(void) {
  double epsilon[3] = {0, 0.01, 0.1};
  char *expname = "eGreedy_stats";
  /* test(epsilon); */
  runExperiment(10, 1000, 2000, sizeof(epsilon) / sizeof(*epsilon), epsilon,
                expname);
  return 0;
}
