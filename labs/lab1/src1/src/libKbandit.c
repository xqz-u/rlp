#include <gsl/gsl_randist.h>
#include <gsl/gsl_rng.h>

#include "../include/libKbandit.h"
#include "../include/utils.h"

void printAction(Arm action) {
  printf("(%.2f -> %.2f, %d)", action.qStar, action.q, action.times);
}

Arm *initActions(int k, gsl_rng *r) {
  Arm *actions = safeMalloc(k * sizeof(Arm));
  int i;
  for (i = 0; i < k; ++i)
    actions[i].qStar = gsl_ran_ugaussian(r);
  return actions;
}

Kbandit initKbandit(int k, int seed) {
  gsl_rng *r = gsl_rng_alloc(gsl_rng_default);
  gsl_rng_set(r, seed);
  return (Kbandit){.k = k, .randomGen = r, .actions = initActions(k, r)};
}

void freeKbandit(Kbandit problem) {
  gsl_rng_free(problem.randomGen);
  free(problem.actions);
}

void printKbandit(Kbandit problem) {
  printf("<K: %d>[", problem.k);
  printAction(problem.actions[0]);
  int i;
  for (i = 1; i < problem.k; ++i) {
    printf(", ");
    printAction(problem.actions[i]);
  }
  printf("]\n");
}
