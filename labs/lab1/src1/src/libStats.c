#define _GNU_SOURCE
#include <assert.h>
#include <stdio.h>

#include "../include/libStats.h"
#include "../include/utils.h"

StatObject initStatObject(int iters, int reps, double epsilon) {
  return (StatObject){.eps = epsilon,
                      .epochStats = safeMalloc(iters * sizeof(StatItem)),
                      .trialStats = safeMalloc(reps * sizeof(double))};
}

void freeStatObject(StatObject s) {
  free(s.epochStats);
  free(s.trialStats);
}

StatObject *makeStats(int iters, int reps, int nEps, double *epsilons) {
  int i;
  StatObject *epsilonStats = safeMalloc(nEps * sizeof(*epsilonStats));
  for (i = 0; i < nEps; ++i)
    epsilonStats[i] = initStatObject(iters, reps, epsilons[i]);
  return epsilonStats;
}

void printAverageStats(StatObject stats, int iters, int reps) {
  int i;
  double meanReward, qArmFreq;
  for (i = 0; i < iters; ++i) {
    meanReward = stats.epochStats[i].expectedReward / reps;
    qArmFreq = 100 * ((double)stats.epochStats[i].qArmFreq / reps);
    printf("%lf,%d,%lf,%lf\n", stats.eps, i, meanReward, qArmFreq);
  }
}

void printTotalStats(StatObject stats, int iters, int reps) {
  int i;
  for (i = 0; i < reps; ++i)
    printf("%lf,%d,%lf\n", stats.eps, i, stats.trialStats[i]);
}

void withFnDumpCSV(statsPrinter fn, StatObject *s, int iters, int reps,
                   int nEps, char *header, char *outfile) {
  char *outPath, *dataDir = "../../data_process/CSV";
  makeDir(dataDir);
  assert(asprintf(&outPath, "%s/%s.csv", dataDir, outfile));
  FILE *fp = freopen(outPath, "w", stdout);
  assert(fp);
  printf("%s\n", header);
  int i;
  for (i = 0; i < nEps; ++i)
    fn(s[i], iters, reps);
  free(outPath);
  fclose(fp);
}
