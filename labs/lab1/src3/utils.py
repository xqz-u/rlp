import os


def vectorize_first(fn):
    def inner(first_list, *args, **kwargs):
        return (
            fn(first_list, *args, **kwargs)
            if not hasattr(first_list, "__iter__") or isinstance(first_list, str)
            else [fn(p, *args, **kwargs) for p in first_list]
        )

    return inner


def over_dirs(data_folder: str) -> list:
    def decorator(fn):
        def inner():
            return [
                fn(fullpath)
                for fullpath in map(
                    lambda fold: os.path.join(data_folder, fold),
                    os.listdir(data_folder),
                )
            ]

        return inner

    return decorator


@vectorize_first
def make_dir(full_path):
    if not os.path.exists(full_path):
        os.makedirs(full_path)


# TODO add possibility to enforce args of same length
def dict_from_lists(names, values):
    return dict(zip(names, values))


def __on_dict_helper(_dict: dict, keys: list, fn: callable, *args, **kwargs) -> dict:
    return dict_from_lists(
        keys, map(lambda x: fn(x, *args, **kwargs), map(lambda k: _dict.pop(k), keys))
    )


# NOTE keys must exist in `_dict`, else a `KeyError` is raised TODO error handle
def on_dict_values(_dict: dict, keys: list, fn: callable, *args, **kwargs) -> dict:
    d = _dict.copy()
    return {**__on_dict_helper(d, keys, fn, *args, **kwargs), **d}


def on_dict_values_se(_dict: dict, keys: list, fn: callable, *args, **kwargs):
    _dict.update(**__on_dict_helper(_dict, keys, fn, *args, **kwargs))
