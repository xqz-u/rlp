#ifndef EXPERIMENT_H
#define EXPERIMENT_H

#include "enums.h"
#include "kBandit.h"
#include "printers.h"
#include "stats.h"

typedef struct Experiment Experiment;

struct Experiment {
  int epochs, steps, seed, expParam;
  vectorD params;
  double initVal;
  kBandit *kb;
  Stats *stats, *currStats;
  Algorithm algo;
  Distribution dist;
  Printer print;
};

Experiment makeExperiment(int k, int epochs, int steps, Algorithm algo,
                          Distribution dist, vectorD params, double initVal);
void runExperiment(Experiment *exp);
void printMean(Experiment *e);
void printTotal(Experiment *e);
void freeExperiment(Experiment exp);

#endif
