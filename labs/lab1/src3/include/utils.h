#ifndef LIBUTILS_H
#define LIBUTILS_H

/* #include <stdarg.h> */
#include <stdlib.h>

void *safeMalloc(size_t size);
/* char *strInterpolate(char *fmt, ...); */
void makeDir(char *dirname);
void printDoubleArr(int len, void *store);
void printIntArr(int len, void *store);
void printWithHeader(int len, void *store, char *header,
                     void (*printer)(int, void *));
void resetIntArr(int until, int val, int *arr);
int compareDouble(double a, double b);
int exists(char *filename);

#endif
