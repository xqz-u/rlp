#ifndef VECTORS_H
#define VECTORS_H

typedef struct vectorD {
  int len;
  double *vals;
} vectorD, *vectorDP;

typedef struct vectorI {
  int len, *vals;
} vectorI, *vectorIP;

vectorD makeVectorD(int sz);
vectorI makeVectorI(int sz);
vectorD makeVectorDFill(int sz, double fillValue);
vectorI makeVectorIFill(int sz, int fillValue);
vectorD bundleVectorD(int sz, double *arr);
vectorI bundleVectorI(int sz, int *arr);
void printVectorD(vectorD v);
void printVectorI(vectorI v);
void resizeVectorI(vectorI *vp, int newlen);
vectorI argmaxD(vectorD v);
double sum(vectorD v);
int in(int what, vectorI v);

#endif
