#ifndef PRINTERS_H
#define PRINTERS_H

#include "enums.h"
#include "stats.h"

typedef struct Printer {
  char *avg_file, *tot_file;
  printer_fn printAvg, printTot;
} Printer;

Printer makePrinter(Algorithm algo, Distribution dist);
void freePrinter(Printer p);
void writeHeader(char *fout, char *header);
void appendStatsOut(char *outfile, printer_fn pf, Stats s, double param);

#endif
