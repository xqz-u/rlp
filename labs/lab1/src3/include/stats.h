#ifndef STATS_H
#define STATS_H

#include "../include/kBandit.h"

typedef struct Stats {
  double epochReward;
  vectorD avgReward, optActionFreq, trialStats;
} Stats;

typedef void (*printer_fn)(Stats, double);

Stats *multipleStats(int iters, int reps, int nParam);
void freeStatsArr(Stats *ss, int len);
void learningStats(int action, double payoff, int time, kBandit problem,
                   Stats *s);
void totalReward(Stats *s, int trial);
void printAverageStats(Stats s, double param);
void printTotalStats(Stats s, double param);

#endif
