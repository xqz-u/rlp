#include "../include/eGreedy.h"
#include "../include/kBandit.h"
#include "../include/utils.h"

void test(void) {
  Algorithm a = EGREEDY;
  Distribution d = Gaussian;
  int k = 10, seed = 41;

  kBandit kb = emptyBandit(k, a, d);

  initRandState(&kb, seed);
  /* egreedyInit(&kb, 0); */

  printKbandit(kb);

  kb.free(kb);
}

void run(int steps) {
  Algorithm a = EGREEDY;
  Distribution d = Gaussian;
  int k = 10, seed = 42;
  double eps = 0;

  kBandit kb = emptyBandit(k, a, d);

  initRandState(&kb, seed);
  egreedyInit(&kb, 0, eps);

  int i, action;
  double payoff;

  for (i = 0; i < steps; ++i) {
    egreedyPrint(kb);
    action = kb.act(kb);
    printf("chosen action %d\n", action);
    payoff = kb.step(action, kb);
    printf("payoff %.3lf\n", payoff);
    /* learn(action, payoff, kb, epochStats); */
    printf("-----------\n");
  }

  kb.free(kb);
}

int main(void) {
  run(4);
  return 0;
}
