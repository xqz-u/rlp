#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "../include/utils.h"
#include "../include/vectors.h"

double *randArrayD(int len) {
  int i;
  double *ret = calloc(len, sizeof(*ret));
  for (i = 0; i < len; ++i)
    ret[i] = rand() % len;
  return ret;
}

int *randArrayI(int len) {
  int i, *ret = calloc(len, sizeof(*ret));
  for (i = 0; i < len; ++i)
    ret[i] = rand() % len;
  return ret;
}

vectorI ex() {
  vectorI ret = (vectorI){4, safeMalloc(4 * sizeof(int))};
  resetIntArr(4, rand(), ret.vals);
  return ret;
}

void test_argmax(void) {
  int len = 10;
  vectorD t = bundleVectorD(len, randArrayD(len));
  printVectorD(t);
  vectorI bestIdx = argmaxD(t);
  printVectorI(bestIdx);
  free(t.vals);
  free(bestIdx.vals);
}

typedef struct Cont {
  int someInt;
  vectorI indices;
} Cont;

void resizer(Cont *store, int sz) { resizeVectorI(&store->indices, sz); }

void testone(void) {
  Cont c = (Cont){6, bundleVectorI(15, randArrayI(15))};
  printVectorI(c.indices);
  resizer(&c, 6);
  printVectorI(c.indices);
  free(c.indices.vals);
}

void testardo(void) {
  int i;
  for (i = 0; i < 10; i = (i + 1)
    ;
  printf("%d\n", i);
}

int main(void) {
  srand(time(NULL));
  /* test_argmax(); */
  /* testone(); */
  testardo();
  return 0;
}
