#include "../include/experiment.h"
#include "../include/misc.h"

void testPrinter(void) {
  Experiment test = (Experiment){.algo = OIV, .dist = Bernoulli};
  Printer p = makePrinter(test);
  printf("%s %s\n", p.avg_file, p.tot_file);
  printf("%p -> %p, %p -> %p", p.printAvg, printAverageStats, p.printTot,
         printTotalStats);
  freePrinter(p);
}

int main(void) {
  testPrinter();
  /* setupFS(); */
  return 0;
}
