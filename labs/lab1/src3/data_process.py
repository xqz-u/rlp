#! /usr/bin/python
import os
import json

import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle

import utils as u

# HOME = os.path.dirname(os.path.abspath(__file__))
HOME = "/home/marcog/Desktop/uni/thirdYear/block-1b/RLP/labs/lab1/src3"

# needed for all plots
distributions = ["Bernoulli", "Gaussian"]
y_axes = ["Mean reward", "Optimal action selection %"]
props = {"index": "time", "columns": "param", "aggfunc": "mean"}

# needed for all tables
aggregations = {"Average total reward": "mean", "Total reward stderr": "sem"}


@u.over_dirs(os.path.join(HOME, "data"))
def parse_algo_data(algo_dir: str) -> dict:
    fnames = os.listdir(algo_dir)
    data = [
        pd.read_csv(os.path.join(algo_dir, fname))
        for fname in fnames
        if fname.endswith(".csv")
    ]
    fnames = map(lambda fname: fname[:-4], fnames)  # strip .csv

    # gather data by algo name and metric class
    zipped_data = list(zip(fnames, data))
    headers = ["avg", "tot"]
    avg_stats = dict(filter(lambda kv: "avg" in kv[0], zipped_data))
    tot_stats = dict(filter(lambda kv: avg_stats.get(kv[0]) is None, zipped_data))
    return {algo_dir.split("/")[-1]: u.dict_from_lists(headers, [avg_stats, tot_stats])}


def experiment_tables(tot_stats: dict, supertitle: str, epochs: int, save_path: str):
    processed_metrics = [
        df.groupby("param")["total_reward"].agg(**aggregations)
        for df in tot_stats.values()
    ]
    for distname, m in zip(["Gaussian", "Bernoulli"], processed_metrics):
        m.to_latex(
            os.path.join(save_path, f"{supertitle}_{epochs}_{distname}_table.tex")
        )


def plot_learning(
    avg_stats: dict, supertitle: str, epochs: int, debug=False, save_path=None
):
    pivot_on_values = lambda dfs, vals: [
        df.pivot_table(values=vals, **props) for df in dfs
    ]

    # NOTE rows will display metric, columns distribution
    plottable_dfs = [
        pivot_on_values(avg_stats.values(), colname)
        for colname in ["mean_reward", "opt_arm_freq"]
    ]

    fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(10, 10))
    fig.suptitle(f"{supertitle}\n(average over {epochs} runs)")

    for i, (metric_data) in enumerate(plottable_dfs):
        for j, df in enumerate(metric_data):
            axes[i, j].set_ylabel(y_axes[i])
            df.plot(
                ax=axes[i, j],
                title=distributions[j],
                legend=False,
            )

    # from Stack
    extra = Rectangle((0, 0), 1, 1, fc="w", fill=False, edgecolor="none", linewidth=0)
    handles, labels = axes[1, 1].get_legend_handles_labels()
    fig.legend([extra, *handles], ("param", *labels), loc="upper right")

    if debug:
        plt.show()
    if save_path and isinstance(save_path, str):
        plt.savefig(os.path.join(save_path, f"{supertitle}_{epochs}_report_plots.png"))


@u.vectorize_first
def do_stats(algo_data: dict, plot_savepath: str, tex_savepath: str):
    algo_name, algo_stats = next(iter(algo_data.items()))
    avg_stats, tot_stats = algo_stats.values()
    nrows = tot_stats["tot_gauss"].shape[0]
    nparam = len(tot_stats["tot_gauss"]["param"].unique())
    epochs = nrows // nparam
    plot_learning(avg_stats, algo_name, epochs, save_path=plot_savepath)
    experiment_tables(tot_stats, algo_name, epochs, tex_savepath)


# TODO print debug info to file (make an overall pd.DataFrame...)
@u.vectorize_first
def best_params(algo_data: dict, debug=False):
    algo_name, algo_stats = next(iter(algo_data.items()))
    _, tot_stats = algo_stats.values()

    means_by_param = {
        f"{distname[4:]}_param": df.groupby("param")["total_reward"].agg("mean")
        for distname, df in tot_stats.items()
    }

    if debug:
        print(algo_name)
        print(means_by_param)

    return {
        algo_name: u.on_dict_values(
            means_by_param, means_by_param.keys(), lambda v: v.idxmax()
        )
    }


def main():
    plots_dir, tables_dir = [
        os.path.join(HOME, dirname) for dirname in ["plots", "tex_dumps"]
    ]
    u.make_dir([plots_dir, tables_dir])
    algo_data = parse_algo_data()
    do_stats(algo_data, plots_dir, tables_dir)
    optimal = best_params(algo_data, True)
    with open(os.path.join(HOME, "optimal_params.json"), "w") as fp:
        json.dump(optimal, fp, ensure_ascii=False, indent=4)


if __name__ == "__main__":
    main()
