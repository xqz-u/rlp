#include <math.h>

#include "../../include/UCB.h"
#include "../../include/eGreedy.h"

int ucbAct(kBandit prob) {
  int i, action;
  vectorD values = makeVectorD(prob.k);

  for (i = 0; i < values.len; ++i) {
    if (!prob.N.vals[i]) { // when action has not been tried yet, choose it
      free(values.vals);
      return i;
    }
    values.vals[i] =
        prob.qt.vals[i] + (prob.param * sqrt(log(prob.time)) / prob.N.vals[i]);
  }

  vectorI actions = argmaxD(values);
  action = actions.vals[gsl_rng_uniform_int(prob.r, actions.len)];

  free(values.vals);
  free(actions.vals);
  return action;
}

void ucbInit(kBandit *prob, Distribution dist, double c) {
  prob->N = makeVectorI(prob->k);
  prob->param = c;
  prob->qt = makeVectorD(prob->k);
  basicInit(prob, ucbAct, egreedyStep, egreedyFree, dist);
}
