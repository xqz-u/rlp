#include <gsl/gsl_rng.h>
#include <math.h>
#include <stdio.h>

#include "../../include/rcomp.h"

int rcompAct(kBandit prob) {
  double p, sumLogsPrefs, sumProb = 0;
  int i;

  /* printf("time %d, actions prefs: ", prob.time); */
  /* printVectorD(prob.pref); */

  vectorD logsPref = makeVectorD(prob.k);
  for (i = 0; i < prob.k; ++i)
    logsPref.vals[i] = exp(prob.pref.vals[i]);

  sumLogsPrefs = sum(logsPref);
  p = gsl_rng_uniform(prob.r);
  /* printf("prob: %.3lf\n", p); */

  for (i = 0; i < prob.k; i = (i + 1) % prob.k) {
    /* printf("prob action %d -> %.3lf", i, logsPref.vals[i] / sumLogsPrefs); */
    sumProb += logsPref.vals[i] / sumLogsPrefs;
    /* printf(", interval: %.3lf\n", sumProb); */
    if (p <= sumProb)
      break;
  }
  /* printf("chosen action %d\n", i); */

  free(logsPref.vals);
  return i;
}

double rcompStep(int action, kBandit *prob) {
  double payoff = prob->bandit(action, *prob);
  /* printf("payoff is %.3lf\n", payoff); */
  double *actionPref = &prob->pref.vals[action];
  double rewardDiff = payoff - prob->avgReward;
  /* printf("reward diff %.3lf\n", rewardDiff); */

  *actionPref += (prob->param * rewardDiff);
  /* printf("new action pref %.3lf\n", *actionPref); */
  prob->avgReward += (prob->param * rewardDiff);
  /* printf("new avgReward: %.3lf\n", prob->avgReward); */
  /* printf("------------------\n"); */

  return payoff;
}

void rcompFree(kBandit prob) {
  prob.avgReward = 0;
  free(prob.pref.vals);
  basicFree(prob);
}

void rcompInit(kBandit *prob, Distribution dist, double alpha) {
  prob->param = alpha;
  prob->avgReward = 0;
  prob->pref = makeVectorD(prob->k);
  basicInit(prob, rcompAct, rcompStep, rcompFree, dist);
}
