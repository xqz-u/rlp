#include <stdio.h>
#include <sys/time.h>

#include "../include/experiment.h"

void test(Algorithm algo, vectorD params) {
  char *expname[] = {"EGREEDY", "OIV", "UCB", "RCOMP"};
  char *distname[] = {"Gaussian", "Bernoulli"};
  Experiment exp;

  int j;
  for (j = Gaussian; j <= Bernoulli; ++j) {
    printf("Starting %s with %s distribution\n", expname[algo], distname[j]);
    exp = makeExperiment(10, 2000, 1000, algo, j, params, algo == OIV ? 5 : 0);
    runExperiment(&exp);
    freeExperiment(exp);
  }
}

void simpleRunner(void) {
  char *expname[] = {"EGREEDY", "OIV"};
  char *distname[] = {"Gaussian", "Bernoulli"};
  Experiment exp;
  double initval = 0;

  vectorD params = makeVectorD(3);
  params.vals[1] = 0.01;
  params.vals[2] = 0.1;

  struct timeval start, end;
  int i, j;
  gettimeofday(&start, NULL);
  for (i = EGREEDY; i <= OIV; ++i) {
    if (i == OIV)
      initval = 5;
    for (j = Gaussian; j <= Bernoulli; ++j) {
      fprintf(stderr, "Starting with %s, distribution: %s\n", expname[i],
              distname[j]);
      exp = makeExperiment(10, 2000, 1000, i, j, params, initval);
      runExperiment(&exp);
      freeExperiment(exp);
    }
  }
  gettimeofday(&end, NULL);

  free(params.vals);

  fprintf(stderr, "time program took %f seconds to execute\n",
          end.tv_sec + end.tv_usec / 1e6 - start.tv_sec - start.tv_usec / 1e6);
}

int main(void) {
  vectorD params = makeVectorDFill(1, 0.1);
  test(RCOMP, params);
  free(params.vals);
  /* simpleRunner(); */
  return 0;
}
