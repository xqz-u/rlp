#define _GNU_SOURCE
#include <assert.h>

#include "../include/UCB.h"
#include "../include/eGreedy.h"
#include "../include/experiment.h"
#include "../include/rcomp.h"
#include "../include/utils.h"

void initKbandit(Experiment exp) {
  initRandState(exp.kb, exp.seed);
  double param = exp.params.vals[exp.expParam];
  switch (exp.algo) {
  case OIV:
  case EGREEDY:
    egreedyInit(exp.kb, exp.initVal, exp.dist, param);
    break;
  case UCB:
    ucbInit(exp.kb, exp.dist, param);
    break;
  case RCOMP:
    rcompInit(exp.kb, exp.dist, param);
    break;
  default:
    perror("Algorithm not implemented!\n");
    exit(1);
  }
}

Experiment makeExperiment(int k, int epochs, int steps, Algorithm algo,
                          Distribution dist, vectorD params, double initVal) {
  kBandit *kb = safeMalloc(sizeof(kBandit));
  *kb = (kBandit){.k = k, .time = 0};

  Experiment exp =
      (Experiment){.epochs = epochs,
                   .steps = steps,
                   .seed = 0,
                   .params = params,
                   .initVal = initVal,
                   .expParam = 0,
                   .kb = kb,
                   .algo = algo,
                   .dist = dist,
                   .stats = multipleStats(steps, epochs, params.len),
                   .print = makePrinter(algo, dist)};
  initKbandit(exp);
  exp.currStats = &exp.stats[0];
  return exp;
}

void freeExperiment(Experiment exp) {
  freeStatsArr(exp.stats, exp.params.len);
  exp.kb->free(*exp.kb);
  free(exp.kb);
  freePrinter(exp.print);
  free(exp.currStats); // NOTE why?????
}

void refreshExperiment(Experiment *exp) {
  exp->kb->free(*exp->kb);
  exp->seed += 1;
  initKbandit(*exp);
  exp->kb->time = 0;
}

void runAlgo(int steps, kBandit *kb, Stats *epochStats) {
  int i, action;
  double payoff;
  for (i = 0; i < steps; ++i) {
    action = kb->act(*kb);
    payoff = kb->step(action, kb);
    learningStats(action, payoff, kb->time, *kb, epochStats);
    kb->time += 1;
  }
}

void repeatAlgo(Experiment *exp) {
  int i;
  for (i = 0; i < exp->epochs; ++i) {
    runAlgo(exp->steps, exp->kb, exp->currStats);
    totalReward(exp->currStats, i);
    refreshExperiment(exp);
  }
  printMean(exp);
  printTotal(exp);
}

void runExperiment(Experiment *exp) {
  int i;
  for (i = 0; i < exp->params.len; ++i) {
    /* exp->currStats = &exp->stats[i]; */
    repeatAlgo(exp);
    exp->currStats = &exp->stats[(i + 1) % exp->params.len];
    exp->expParam += 1;
  }
}

void printMean(Experiment *e) {
  char *dumpfile = e->print.avg_file;
  if (!exists(dumpfile))
    writeHeader(dumpfile, "time,mean_reward,opt_arm_freq,param\n");
  appendStatsOut(dumpfile, e->print.printAvg, *e->currStats, e->kb->param);
}

void printTotal(Experiment *e) {
  char *dumpfile = e->print.tot_file;
  if (!exists(dumpfile))
    writeHeader(dumpfile, "epoch,total_reward,param\n");
  appendStatsOut(dumpfile, e->print.printTot, *e->currStats, e->kb->param);
}
