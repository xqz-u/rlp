#define _GNU_SOURCE

#include <assert.h>
#include <stdio.h>

#include "../include/printers.h"
#include "../include/utils.h"

char *algos[] = {"EGREEDY", "OIV", "UCB", "RCOMP"};
char *avg_fnames[] = {"avg_gauss", "avg_bern"};
char *tot_fnames[] = {"tot_gauss", "tot_bern"};

char *helper(char *algoName, char *distName) {
  char *ret;
  assert(asprintf(&ret, "../data/%s/%s.csv", algoName, distName));
  return ret;
}

Printer makePrinter(Algorithm algo, Distribution dist) {
  char *algo_dir;
  assert(asprintf(&algo_dir, "../data/%s", algos[algo]));
  makeDir(algo_dir);
  Printer p = (Printer){.avg_file = helper(algo_dir, avg_fnames[dist]),
                        .tot_file = helper(algo_dir, tot_fnames[dist]),
                        .printAvg = printAverageStats,
                        .printTot = printTotalStats};
  free(algo_dir);
  return p;
}

void freePrinter(Printer p) {
  free(p.avg_file);
  free(p.tot_file);
}

void writeHeader(char *fout, char *header) {
  FILE *fp = freopen(fout, "w", stdout);
  assert(fp);
  printf("%s", header);
  fclose(fp);
}

void appendStatsOut(char *outfile, printer_fn pf, Stats s, double param) {
  FILE *fp = freopen(outfile, "a", stdout);
  assert(fp);
  pf(s, param);
  fclose(fp);
}
