#define _GNU_SOURCE
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "../include/kBandit.h"
#include "../include/stats.h"
#include "../include/utils.h"

Stats initStats(int iters, int reps) {
  return (Stats){.epochReward = 0,
                 .avgReward = makeVectorD(iters),
                 .optActionFreq = makeVectorD(iters),
                 .trialStats = makeVectorD(reps)};
}

Stats *multipleStats(int iters, int reps, int nParam) {
  int i;
  Stats *paramStats = safeMalloc(nParam * sizeof(*paramStats));
  for (i = 0; i < nParam; ++i)
    paramStats[i] = initStats(iters, reps);
  return paramStats;
}

void freeStats(Stats s) {
  free(s.avgReward.vals);
  free(s.optActionFreq.vals);
  free(s.trialStats.vals);
}

void freeStatsArr(Stats *ss, int len) {
  int i;
  for (i = 0; i < len; ++i)
    freeStats(ss[i]);
}

void learningStats(int action, double payoff, int time, kBandit problem,
                   Stats *s) {
  s->epochReward += payoff;
  s->avgReward.vals[time] += payoff;
  s->optActionFreq.vals[time] += in(action, problem.optActions);
}

void totalReward(Stats *s, int trial) {
  s->trialStats.vals[trial] = s->epochReward;
  s->epochReward = 0;
}

void printAverageStats(Stats s, double param) {
  int i, epochs = s.trialStats.len;
  double meanReward, qArmFreq;
  for (i = 0; i < s.avgReward.len; ++i) {
    meanReward = s.avgReward.vals[i] / epochs;
    qArmFreq = s.optActionFreq.vals[i] / epochs * 100;
    printf("%d,%lf,%lf,%lf\n", i, meanReward, qArmFreq, param);
  }
}

void printTotalStats(Stats s, double param) {
  int i;
  for (i = 0; i < s.trialStats.len; ++i)
    printf("%d,%lf,%lf\n", i, s.trialStats.vals[i], param);
}
