#! /usr/bin/python

import itertools as it
import os
import sys

import matplotlib.pyplot as plt
import pandas as pd

from utils import dict_from_lists, make_dirs


def algos_CSVS_to_DFS(
    csv_directory: str, algos: list, headers: list, grouping_fn: callable
) -> dict:
    csv_files_fullpath = [
        f"{csv_directory}/{csv}"
        for csv in os.listdir(csv_directory)
        if csv.endswith(".csv")
    ]
    sorted_files = sorted(csv_files_fullpath, key=grouping_fn)
    measures = [list(g_csv) for (_, g_csv) in it.groupby(sorted_files, key=grouping_fn)]
    csvs = [[pd.read_csv(csv) for csv in group] for group in measures]
    csvs_with_header = [
        dict_from_lists(headers, algo_stats) for algo_stats in zip(*csvs)
    ]
    return dict_from_lists(algos, csvs_with_header)


def summarize_experiment(df: pd.DataFrame) -> plt.Axes:
    aggregations = {"total_reward_avg": "mean", "total_reward_stderr": "sem"}
    return df.groupby("epsilon")["total_reward"].agg(**aggregations)


# TODO proper images size
# TODO organize the two dfs together and plot them only once ?
def plot_learning(df: pd.DataFrame, debug=False):
    common = {"index": "epoch", "columns": "epsilon"}

    reward_by_eps = df.pivot(**common, values="mean_reward")
    qarm_by_eps = df.pivot(**common, values="optimal_arm_selection_freq")

    fig, axes = plt.subplots(ncols=2)

    reward_by_eps.plot(title="Mean reward by epoch", ax=axes[0])
    qarm_by_eps.plot(title="Percentage of optimal arm selection by epoch", ax=axes[1])

    if debug:
        plt.show()
    return fig


def algorithm_stats(algo_data: dict) -> dict:
    tot_data, avg_data = algo_data.values()
    return {"plots": plot_learning(avg_data), "table": summarize_experiment(tot_data)}


def experiments_stats(config: dict, save_files=False) -> list:
    csv_dir, tex_dir, pics_dir = config.pop("paths")
    experiment_data = algos_CSVS_to_DFS(csv_dir, *config.values())
    summaries = [algorithm_stats(algo_data) for algo_data in experiment_data.values()]
    if save_files:
        make_dirs(tex_dir, pics_dir)
        for (algo_name, algo_summ) in zip(config["algos"], summaries):
            plot, table = algo_summ.values()
            plt.savefig(f"{pics_dir}/{algo_name}_plot.png")
            table.to_latex(f"{tex_dir}/{algo_name}_table.tex")


def main():
    final_run = False
    if len(sys.argv) >= 2:
        final_run = sys.argv[1]
        assert final_run in ["0", "1"]
    paths = ["CSV", "tex_dumps", "pics"]
    config = {
        "paths": [f"{os.getcwd()}/{p}" for p in paths],
        "algos": ["EGREEDY"],
        "headers": ["tot", "avg"],
        "grouping_fn": lambda x: "avg" in x,
    }
    return experiments_stats(config, bool(int(final_run)))


if __name__ == "__main__":
    main()
