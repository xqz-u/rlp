import os


def make_dir(full_path):
    if not os.path.exists(full_path):
        os.makedirs(full_path)


def margs(fn):
    def inner(*args):
        for a in args:
            fn(a)

    return inner


@margs
def make_dirs(path):
    make_dir(path)


# TODO add possibility to enforce args of same length
def dict_from_lists(names, values):
    return dict(zip(names, values))
