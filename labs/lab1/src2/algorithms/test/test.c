#include <stdlib.h>
#include <time.h>

#include "../include/kBandit.h"
#include "../include/utils.h"

int *randArray(int len) {
  int i, *ret = calloc(len, sizeof(*ret));
  for (i = 0; i < len; ++i)
    ret[i] = rand() % len;
  return ret;
}

void resetIdxArr(int len, int *store) {
  int i;
  for (i = 0; i < len; ++i)
    store[i] = -1;
}

int findMax(int len, int *store) {
  int i, max, strike, length;
  max = strike = length = 0;

  int *bestIdx = safeMalloc(len * sizeof(*bestIdx));
  resetIdxArr(len, bestIdx);

  for (i = 0; i < len; ++i) {
    if (store[i] > store[max]) {
      printf("found new max at %d\n", i);
      max = i;
      resetIdxArr(strike, bestIdx);
      strike = 0;
      bestIdx[strike++] = max;
    } else if (store[i] == store[max]) {
      printf("found equal max at %d\n", i);
      bestIdx[strike++] = i;
    }
    printWithHeader(len, bestIdx, "in consruction: ", printIntArr);
  }

  for (i = 0; i < len; ++i)
    length += bestIdx[i] != -1 ? 1 : 0;

  printf("%d best indices, ", length);
  printWithHeader(len, bestIdx, "they are: ", printIntArr);

  max = rand() % (length + 1);
  free(bestIdx);
  return max;
}

void test(void) {
  srand(time(NULL));
  int len = 10;
  int *store = randArray(len);
  printIntArr(len, store);
  printWithHeader(len, store, "trial: ", printIntArr);
  free(store);
}

void testBestArms(void) {
  int len = 10;
  double arms[] = {0.4, -0.5, 1.3, 1.6, 1.2, 1.6, -1.3, 1.0, 1.6, -0.7};
  /* int *opts = optimalArms(len, arms); */
  /* printIntArr(len, opts); */
  /* free(opts); */
}

void another(void) {
  srand(time(NULL));
  /* int store[] = {8, 7, 4, 9, 10, 1, 2, 4, 10, 12}; */
  int *store = randArray(10);
  printIntArr(10, store);
  printf("chosen maxIdx is %d\n", findMax(10, store));
}

void testKbandit(void) {
  int k = 10, i;
  kBandit dummy;
  for (i = 0; i < 3; ++i) {
    dummy = initKbandit(k, i + 42);
    printKbandit(dummy);
    freeKbandit(dummy);
    printf("-------------\n");
  }
}

int main(void) {
  /* test(); */
  /* testBestArms(); */
  /* testKbandit(); */
  another();
  return 0;
}
