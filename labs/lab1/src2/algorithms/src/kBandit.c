#include <gsl/gsl_randist.h>
#include <stdio.h>

#include "../include/kBandit.h"
#include "../include/utils.h"

double uGaussianVariate(double meanShift, gsl_rng *r) {
  return gsl_ran_ugaussian(r) + meanShift;
}

double *nUGaussianVariates(int n, double meanShift, gsl_rng *r) {
  double *variates = safeMalloc(n * sizeof(*variates));
  int i;
  for (i = 0; i < n; ++i)
    variates[i] = uGaussianVariate(meanShift, r);
  return variates;
}

int optimalArm(int len, double *values) {
  int i, best = 0;
  for (i = 0; i < len; ++i)
    if (values[i] > values[best])
      best = i;
  return best;
}

kBandit initKbandit(int k, int seed) {
  gsl_rng *r = gsl_rng_alloc(gsl_rng_default);
  gsl_rng_set(r, seed);
  double *realMeans = nUGaussianVariates(k, 0, r);
  return (kBandit){.r = r,
                   .k = k,
                   .optimal = optimalArm(k, realMeans),
                   .N = safeMalloc(k * sizeof(int)),
                   .arms = safeMalloc(k * sizeof(double)),
                   .qStar = realMeans};
}

void freeKbandit(kBandit problem) {
  free(problem.N);
  free(problem.arms);
  free(problem.qStar);
  gsl_rng_free(problem.r);
}

void printKbandit(kBandit problem) {
  int len = problem.k;
  printWithHeader(len, problem.qStar, "real means: ", printDoubleArr);
  printWithHeader(len, problem.N, "times chosen: ", printIntArr);
  printWithHeader(len, problem.arms, "value estimates: ", printDoubleArr);
  printf("optimal action: %d\n", problem.optimal);
}
