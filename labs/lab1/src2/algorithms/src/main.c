#include "../include/eGreedy.h"
#include <gsl/gsl_rng.h>

void resetIdxArr(int len, int *store) {
  int i;
  for (i = 0; i < len; ++i)
    store[i] = -1;
}

int findMax(kBandit problem) {
  int i, max, strike, length;
  max = strike = length = 0;

  int *bestIdx = safeMalloc(problem.k * sizeof(*bestIdx));
  resetIdxArr(problem.k, bestIdx);

  for (i = 0; i < problem.k; ++i) {
    if (problem.arms[i] > problem.arms[max]) {
      max = i;
      resetIdxArr(strike, bestIdx);
      strike = 0;
      bestIdx[strike++] = max;
    } else if (problem.arms[i] == problem.arms[max])
      bestIdx[strike++] = i;
  }

  for (i = 0; i < problem.k; ++i)
    length += bestIdx[i] != -1 ? 1 : 0;

  max = bestIdx[gsl_rng_uniform_int(problem.r, length)];

  free(bestIdx);
  return max;
}

int chooseAction(Runner *runner) {
  gsl_rng *r = runner->problem.r;
  return (gsl_rng_uniform(r) < runner->currStats->eps)
             ? gsl_rng_uniform_int(r, runner->problem.k)
             : findMax(runner->problem);
}

/* int chooseAction(int bestAction, Runner *runner) { */
/*   gsl_rng *r = runner->problem.r; */
/*   return ((bestAction < 0 || (gsl_rng_uniform(r) < runner->currStats->eps))
 */
/*               ? gsl_rng_uniform_int(r, runner->problem.k) */
/*               : bestAction); */
/* } */

/* int argmax(int bestAction, int action, double *valueEstimates) { */
/*   return ((bestAction < 0) || */
/*           (valueEstimates[action] > valueEstimates[bestAction])) */
/*              ? action */
/*              : bestAction; */
/* } */

double bandit(kBandit problem, int action) {
  return uGaussianVariate(problem.qStar[action], problem.r);
}

void newActionEstimate(int action, double reward, kBandit problem) {
  int *timesChosen = &problem.N[action];
  double *valueEstimate = &problem.arms[action];
  *timesChosen += 1;
  *valueEstimate += ((reward - *valueEstimate) / *timesChosen);
}

void eGreedy(Runner *runner, int withStats) {
  int i, action, bestAction = -1;
  double reward;
  for (i = 0; i < runner->epochs; ++i) {
    action = chooseAction(runner);
    reward = bandit(runner->problem, action);
    /* printf("chose action %d, reward %.2lf\n", action, reward); */
    newActionEstimate(action, reward, runner->problem);
    /* bestAction = argmax(bestAction, action, runner->problem.arms); */
    /* printf("new best action is %d\n", bestAction); */
    /* printKbandit(runner->problem); */
    /* printf("--------------\n"); */
    if (withStats)
      learningStats(action, reward, i, runner->problem, runner->currStats);
  }
}

void runExperiment(Runner *runner, int *seed, int withStats) {
  int i;
  for (i = 0; i < runner->runs; ++i) {
    eGreedy(runner, withStats);
    if (withStats)
      totalReward(runner->currStats, runner->runs, i);
    refreshRunner(runner, ++(*seed));
  }
}

void runExperimentWithEpsilons(int k, int steps, int runs, int epsLen,
                               double *eps, char *fout) {
  int i, seed = 0;
  Runner runner = initRunner(k, steps, runs, epsLen, eps, seed);
  for (i = 0; i < epsLen; ++i) {
    /* runner.currStats = &runner.stats[i]; */
    runExperiment(&runner, &seed, 1);
    runner.currStats = &runner.stats[(i + 1) % epsLen];
  }
  dumpCSV(fout, runner);
  freeRunner(runner);
}

void test(void) {
  double epsilon[] = {0, 0.1, 0.01};
  char *expname = "eGreedy_stats";
  runExperimentWithEpsilons(10, 1000, 2000, 3, epsilon, expname);
}

int main(void) {
  test();
  return 0;
}
