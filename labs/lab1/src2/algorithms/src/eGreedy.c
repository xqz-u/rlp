#define _GNU_SOURCE
#include <assert.h>

#include "../include/eGreedy.h"

Runner initRunner(int nArms, int iterations, int repetitions, int nEps,
                  double *epsilons, int seed) {
  Runner r =
      (Runner){.epochs = iterations,
               .runs = repetitions,
               .nEps = nEps,
               .problem = initKbandit(nArms, seed),
               .stats = makeStats(iterations, repetitions, nEps, epsilons)};
  r.currStats = &r.stats[0];
  return r;
}

void freeRunner(Runner r) {
  freeKbandit(r.problem);
  int i;
  for (i = 0; i < r.nEps; ++i)
    freeStatObject(r.stats[i]);
  free(r.currStats);
}

void refreshRunner(Runner *runner, int newSeed) {
  freeKbandit(runner->problem);
  runner->problem = initKbandit(runner->problem.k, newSeed);
}

void dumpCSV(char *filename, Runner r) {
  char *out;
  char *avgHeader = "epsilon,epoch,mean_reward,optimal_arm_selection_freq";
  char *totHeader = "epsilon,run,total_reward";

  assert(asprintf(&out, "%s_avg", filename));
  withFnDumpCSV(printAverageStats, r.stats, r.epochs, r.runs, r.nEps, avgHeader,
                out);
  free(out);

  assert(asprintf(&out, "%s_tot", filename));
  withFnDumpCSV(printTotalStats, r.stats, r.epochs, r.runs, r.nEps, totHeader,
                out);
  free(out);
}
