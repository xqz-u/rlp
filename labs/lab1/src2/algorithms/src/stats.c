#define _GNU_SOURCE
#include <assert.h>
#include <stdio.h>

#include "../include/stats.h"
#include "../include/utils.h"

StatObject initStatObject(int iters, int reps, double epsilon) {
  return (StatObject){.eps = epsilon,
                      .epochStats = safeMalloc(iters * sizeof(StatItem)),
                      .trialStats = safeMalloc(reps * sizeof(double)),
                      .totReward = 0.0};
}

void freeStatObject(StatObject s) {
  free(s.epochStats);
  free(s.trialStats);
}

StatObject *makeStats(int iters, int reps, int nEps, double *epsilons) {
  int i;
  StatObject *epsilonStats = safeMalloc(nEps * sizeof(*epsilonStats));
  for (i = 0; i < nEps; ++i)
    epsilonStats[i] = initStatObject(iters, reps, epsilons[i]);
  return epsilonStats;
}

void learningStats(int action, double payoff, int epoch, kBandit problem,
                   StatObject *stats) {
  stats->totReward += payoff;
  stats->epochStats[epoch].avgReward += payoff;
  if (action == problem.optimal)
    stats->epochStats[epoch].optSelectionFreq += 1;
}

void totalReward(StatObject *stats, int epochs, int trial) {
  stats->trialStats[trial] = stats->totReward;
  stats->totReward = 0;
}

void printAverageStats(StatObject stats, int iters, int reps) {
  int i;
  double meanReward, qArmFreq;
  for (i = 0; i < iters; ++i) {
    meanReward = stats.epochStats[i].avgReward / reps;
    qArmFreq = stats.epochStats[i].optSelectionFreq / reps;
    printf("%lf,%d,%lf,%lf\n", stats.eps, i, meanReward, qArmFreq);
  }
}

void printTotalStats(StatObject stats, int iters, int reps) {
  int i;
  for (i = 0; i < reps; ++i)
    printf("%lf,%d,%lf\n", stats.eps, i, stats.trialStats[i]);
}

void withFnDumpCSV(statsPrinter fn, StatObject *s, int iters, int reps,
                   int nEps, char *header, char *outfile) {
  char *outPath, *dataDir = "../../data/CSV";
  makeDir(dataDir);
  assert(asprintf(&outPath, "%s/%s.csv", dataDir, outfile));
  FILE *fp = freopen(outPath, "w", stdout);
  assert(fp);
  printf("%s\n", header);
  int i;
  for (i = 0; i < nEps; ++i)
    fn(s[i], iters, reps);
  free(outPath);
  fclose(fp);
}
