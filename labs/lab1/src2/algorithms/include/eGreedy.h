#ifndef EGREEDY_H
#define EGREEDY_H

#include "kBandit.h"
#include "stats.h"

typedef struct Runner {
  int epochs, runs, nEps;
  kBandit problem;
  StatObject *stats, *currStats;
} Runner;

Runner initRunner(int nArms, int iterations, int repetitions, int nEps,
                  double *epsilons, int seed);
void freeRunner(Runner r);
void refreshRunner(Runner *runner, int newSeed);
void dumpCSV(char *filename, Runner r);

#endif
