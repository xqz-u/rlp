#ifndef STATS_H
#define STATS_H

#include "../include/kBandit.h"

typedef struct StatItem {
  double avgReward, optSelectionFreq;
} StatItem;

typedef struct StatObject {
  StatItem *epochStats;
  double eps, totReward, *trialStats;
} StatObject;

typedef void (*statsPrinter)(StatObject, int, int);

/* TODO instead of exposing the `statsPrinter` function, export an `enum` that
 * maps to them */

StatObject *makeStats(int iters, int reps, int nEps, double *epsilons);
void freeStatObject(StatObject s);
void learningStats(int action, double payoff, int epoch, kBandit problem,
                   StatObject *stats);
void totalReward(StatObject *stats, int epochs, int trial);
void printAverageStats(StatObject stats, int iters, int reps);
void printTotalStats(StatObject stats, int iters, int reps);
void withFnDumpCSV(statsPrinter fn, StatObject *s, int iters, int reps,
                   int nEps, char *header, char *outfile);

#endif
