#ifndef KBANDIT_H
#define KBANDIT_H

#include <gsl/gsl_rng.h>

typedef struct kBandit {
  gsl_rng *r;
  int k, optimal, *N;
  double *arms, *qStar;
} kBandit;

double uGaussianVariate(double meanShift, gsl_rng *r);
double *nUGaussianVariates(int n, double meanShift, gsl_rng *r);
int optimalArm(int len, double *values);
kBandit initKbandit(int k, int seed);
void freeKbandit(kBandit problem);
void printKbandit(kBandit problem);

#endif
