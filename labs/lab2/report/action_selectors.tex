% Softmax exploration with
% lower values for $T$ will most likely still lead to optimal action selection,
% however other actions are also ranked instead of randomly chosen.
% The $\e$-greedy exploration strategy provides a simple and explicit
% schema for tackling the exploration / exploitation problem. As such it is one of
% the most prevalent and widely used exploration strategies. At its core,
% $\e$-greedy utilizes $0 \leq \e \leq 1$ as a parameter, which
% denotes the probability of taking a random action at each time step. The means
% via which $\e$-greedy works is presented formally below.
% Where $\arg\max_a Q_t(s_t, a)$ denotes the action yielding the largest Q-value in
% state $s_t$. Moreover, as can be seen, $\e$ dictates the exploration.
% If $\e$ would be set to 0.1, the algorithm would choose random actions
% for 10\% of the total runs, while in the other 90\% it would choose the action
% yielding the largest Q-value in the current state.
\subsubsection*{2.2 Exploration strategies}
This section briefly describes the three action selection strategies used in the
study.

\noindent\textbf{$\e$-Greedy}
In state $s$ at time $t$, $\e$-Greedy selects action $a_t$ where
\begin{equation}
  a_t =
  \begin{cases}
    \arg\max_a Q_t(s_t,a) & \text{with probability 1 - \e (breaking ties randomly)}\\
    \text{a random action} & \text{with probability} \ \e \tag*{from~\cite{sutton2018reinforcement}}
  \end{cases}
  \label{eqn:egreedy}
\end{equation}
The parameter $\e$ controls the exploration rate (with $\e = 0$, the algorithm
always selects the greedy action). The main disadvantage of $\e$-Greedy is its
indiscriminate exploration of the action space, which does not favor the
acquired knowledge.

\noindent\textbf{UCB-1}
The UCB-1 exploration strategy keeps count of the number of times action
$a$ is taken in state $s_t$. The strategy first selects all actions once for
each state. Afterwards, an exploration bonus is calculated as
\begin{equation}
  bonus(s_t, a^i) = C \times \sqrt{2 \times \frac{\log(N(s_t))}{N(s_t, a^i)}} \tag*{from~\cite{tijsma2016comparing}}
\end{equation}
where $N(s_t) = \Sigma_i N(s_t, a^i)$ is the number of times state $s_t$ has
been visited, and $N(s_t, a^i)$ is the number of times action $a^i$ was
chosen in state $s_t$. The constant $C$ denotes the UCB-1 exploration rate; a
larger value of $C$ increases exploration, while $C = 0$ means greediness.
Action selection is then determined as
\begin{equation}
  a_t = \arg\max_a (Q(s_t, a) + bonus(s_t, a)) \tag*{from~\cite{tijsma2016comparing}}
\end{equation}
where $a_t$ is the action with the largest value.

\noindent\textbf{Boltzmann (Softmax)}
The Boltzmann or softmax exploration strategy uses the Boltzmann
distribution to assign probabilities to the available actions in order to create
a graded function of estimated values. The probability distribution for the
actions in state $s$ is obtained via
\begin{equation}
  \pi (s_t, a) = \frac{e^{Q_t(s_t,a)/T}}
  {\sum_{i = 1}^{m} e^{Q_t(s_t, a^i)/T}}
  \tag*{from~\cite{tijsma2016comparing}}
\end{equation}
\noindent
where $\pi(s_t, a)$ denotes the probability that the agent selects action $a$ in
state $s_t$. $T > 0$ is the temperature parameter used in the Boltzmann
distribution which influences the exploration rate. With $T
\rightarrow \infty$ the agent selects random actions, while with $T \rightarrow
0^+$ greedier actions are selected.
