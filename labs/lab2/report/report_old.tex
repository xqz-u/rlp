\documentclass{article}
\usepackage{bnaic}
\usepackage{notation}
\usepackage{hyperref}
\usepackage{graphicx}
\graphicspath{ {./assets/} }


\title{\textbf{\huge Reinforcement Learning Practical\\Lab session 1}}

\author{Marco Andrea Gallo (s3680622)}

\date{\textit{University of Groningen}}

\pagestyle{empty}

\begin{document}

\ttl

\thispagestyle{empty}

\section{Algorithms}
Four algorithms were implemented to study the \emph{k}-armed bandit problem.
Three of these perform learning using \emph{action-value methods} - they learn an
action's true value by adapting action-values estimates only. These are the
$\e$-Greedy, Optimistic Initial Values and UCB algorithms. All these algorithms
use an action-value update rule of the form
\begin{equation}
  Q_{t + 1}(a) = Q_t(a) + \frac{1}{N_t(a)} \left[ r_t - Q_t(a) \right]
  \label{eqn:general_update}
\end{equation}
where $a$ is the action chosen at time $t$, $r_t$ is the action's reward and
$N_t(a)$ is the number of times $a$ has been chosen up to time $t$.\\
Rather than forming action-value estimates, the fourth algorithm compares the
reward of an action to an overall \emph{reference reward}, following the idea
that a learning method should favor actions which yield high rewards. This is
the Reinforcement Comparison algorithm.

\subsection{$\e$-Greedy}
In a Greedy action selection policy, the action chosen at time $t$ is:
\begin{equation}
  a_t = a_t^o = \arg\max_a Q_t(a)
\end{equation}
However, this approach suffers from lack of exploration, and gets soon stuck in
local maxima. A better alternative is to control the exploration of the reward
space non-deterministically: exploit the current knowledge most of the time
(greedy), and occasionally sample a random action with probability $\e$.
According to the $\e$-Greedy action selection rule, the action chosen at time
$t$ is:
\begin{equation}
  a_t =
  \begin{cases}
    a_t^o & \text{with probability 1 - \e (breaking ties randomly)}\\
    \text{a random action} & \text{with probability} \e
  \end{cases}
\end{equation}

\subsection{Optimistic Initial Values}
The value estimations formed by the $\e$-Greedy method depend on $Q_0(a)$, the
value-estimates at $t = 0$. This means that this learning method is
\emph{biased} by the initial value-estimates. One way to address this issue is
to initialize the value-estimates optimistically (e.g. $Q_0(a) = 5$) for all
$a$, then to use an $\e$-greedy action selection policy. This approach favors
exploration from early on: the rewards received will be averagely high due to
the optimistic initialization, making the algorithm quickly converge to a high
absolute payoff. Being the optimal initial values tailored to the problem, this
strategy is hard to generalize, therefore rarely useful in other scenarios.

\subsection{UCB}
Both the $\e$-Greedy and Optimistic Initial Values methods perform exploration
in an unstructured manner: occasionally, an action other than the current best
is performed completely at random. The Upper Confidence Bound (UCB) learning
method addresses this issue by establishing a relation between the confidence in
a non-greedy action's goodness and the likelihood of selecting it. The action
selection rule thus becomes:
\begin{equation}
  a_t \defeq \arg\max_a \left[ Q_t(a) + c \sqrt{\frac{\ln{t}}{N_t(a)}} \right]
\end{equation}
The square root term controls the uncertainty in an action's value, while the
$c$ term controls the confidence on the latter (increasing it results in more
exploratory behavior). The more an action has been chosen (given by the
denominator of the square root term), the more certain the learner is of its
true value. Exploratory behavior can then use this additional information when
trying out new actions. Finally, this algorithm is hard to extend to
non-stationary problems and problems with a large state space, making it
ill-suited for other RL problems.

\subsection{Reinforcement Comparison}
Differently from the learning methods presented so far, Reinforcement Comparison
does not keep a record of value-estimates for each action. Rather, it keeps a
record of \emph{preferences} for each action, which is influenced by the reward
given by that action. This is accomplished by keeping track of a reference
reward $\bar r_t$ (e.g. the mean reward received so far): given an action $a_t$
and a reward $r_t$, $a_t$'s preference is strengthened or weakened depending on
$r_t - \bar r_t$. Preferences are then mapped to probabilities, and the action
selection rule becomes (using e.g. a softmax distribution):
\begin{equation}
  \pi_t(a) = Pr \{ a_t = a\} = \frac{e^{p_t(a)}}{\sum_{b = 1}^{n} e^{p_t(b)}}
\end{equation}
In this way, the action preferences are normalized to ultimately sum to 1, hence
representing probabilities. The preference update rule for this learning method
is
\begin{equation}
  \label{eqn:preference_update}
  p_{t + 1} = p_t(a_t) + \beta \left[r_t - \bar r_t \right]
\end{equation}
with $0 < \beta < 1$ being a step-size parameter, while the reference reward
update is given by
\begin{equation}
  \label{eqn:rew_update}
  \bar r_{t + 1} = \bar r_t + \alpha \left[r_t - \bar r_t \right]
\end{equation}
with $0 < \alpha < 1$ being another-step size parameter.

\section{Experimental setup}
Each experiment was tested with two distributions for the arms' rewards: a
standard normal and a Bernoulli distribution (the latter giving reward of 1 with
probability $\alpha$ and 0 with probability $1 - \alpha$). For every experiment,
$k = 10$ was chosen to easily compare experimental results with
\cite{sutton2018reinforcement}. Each experiment was performed for 10,000 action
selections or time steps; we call this a single run. The experiment was repeated
for 1000 runs in total. The seed for the random number generator used in the
reward functions was initialized to 0, then incremented by 1 for each run; the
pseudo-random number generator algorithm chosen is the default one from
\texttt{GSL}, the Gnu Scientific Library \cite{GSL}. The hyper-parameters
optimized for each distribution were:
\begin{itemize}
\item \textbf{$\e$-Greedy}: $\e$ (random action choice probability)
\item \textbf{Optimistic Initial Values}: $\e$ (random action choice
  probability). The initial values $Q_0(a)$ where kept constant at 5, as in
  \cite{sutton2018reinforcement}. Moreover, the step-size parameter for the
  action-value update rule is the basic sample average as in
  \ref{eqn:general_update} and not a constant value, by mistake of the author.
\item \textbf{UCB}: $c$ (action confidence level)
\item \textbf{Reinforcement Comparison}: $\beta$ (step-size parameter) from
  \ref{eqn:preference_update}. Moreover, for this experiment, the step-size
  $\alpha$ from \ref{eqn:rew_update} was kept equal to $\beta$.
\end{itemize}

\section{Findings}
The average reward and optimal action selection percentage per time step are
reported in figure \ref{pic:report}. Table \ref{table:gauss} and
\ref{table:bern} report the best hyper-parameters values for each
distribution. These come from a grid search in the hyper-parameters space using
15 values between 0.01 and 0.1, 4 algorithms and 2 distributions, for a total of
120 experiment configurations. The evaluation metric used in order to find the
best hyper-parameters for each algorithm and distribution is the average
cumulative reward across experiment runs, as by assignment description. In the
discussion another method is explored, and the results displayed in the
appendix.

\begin{figure}[h]
  \includegraphics[width=\textwidth]{report_plots}
  \label{pic:report}
\end{figure}

\begin{table}[h]
  \begin{tabular}{l|l|r|r|r}
    \hline
    algo & dist & param & run\_reward & run\_opt\_freq\\
    \hline
    eGreedy & Gaussian & 0.023 & 14889.07 & 83.64583\\
    \hline
    Optimistic initial values & Gaussian & 0.016 & 15066.62 & 83.49739\\
    \hline
    Reinforcement comparison & Gaussian & 0.094 & 15302.33 & 88.29511\\
    \hline
    UCB & Gaussian & 0.074 & 14679.89 & 73.15962\\
    \hline
  \end{tabular}
  \label{table:gauss}
\end{table}

\begin{table}[h]
  \begin{tabular}{l|l|r|r|r}
    \hline
    algo & dist & param & run\_reward & run\_opt\_freq\\
    \hline
    eGreedy & Bernoulli & 0.023 & 8827.750 & 78.53965\\
    \hline
    Optimistic initial values & Bernoulli & 0.010 & 8895.234 & 77.28033\\
    \hline
    Reinforcement comparison & Bernoulli & 0.100 & 8865.358 & 82.62062\\
    \hline
    UCB & Bernoulli & 0.094 & 8837.058 & 73.21543\\
    \hline
  \end{tabular}
  \label{table:bern}
\end{table}


\section{Discussion}
In the appendix, another evaluation metric is proposed for the experiment: the
combined normalized (z-scores) average cumulative reward and average optimal
action selection frequency. The best hyper-parameter configurations
corresponding to this can be found in the Appendix tables.

Reinforcement comparison suffers from outliers in the Bernoulli distribution
version of the problem.

No significance test was performed, since to target player was designed. A
random action selection learner would have been perfect for evaluation in this
case.

It seems that overall the $\e$-Greedy algorithm performs best. However, a very
limited portion of the parameter space was explored. A bigger exploration is
needed.

\bibliographystyle{plain}
\bibliography{bibl}

\section{Appendix}

\begin{figure}[h]
  \includegraphics[width=\textwidth]{metrics_trends}
  \caption{Experiment parameter vs. evaluation metrics value}
\end{figure}

\begin{figure}[h]
  \includegraphics[width=\textwidth]{metrics_space}
  \caption{Metrics space (average cumulative reward vs. average optimal action
    selection frequency, per run)}
\end{figure}

\begin{figure}[h]
  \includegraphics[width=\textwidth]{metrics_distributions}
  \caption{Metrics values distributions}
\end{figure}

\begin{table}[h]
  \begin{tabular}{l|l|r|r|r}
    \hline
    algo & dist & param & run\_reward & run\_opt\_freq\\
    \hline
    eGreedy & Bernoulli & 0.036 & 8815.226 & 80.35556\\
    \hline
    Optimistic initial values & Bernoulli & 0.023 & 8874.816 & 79.75576\\
    \hline
    Reinforcement comparison & Bernoulli & 0.100 & 8865.358 & 82.62062\\
    \hline
    UCB & Bernoulli & 0.100 & 8833.772 & 73.72950\\
    \hline
  \end{tabular}
\end{table}

\begin{table}
  \begin{tabular}{l|l|r|r|r}
    \hline
    algo & dist & param & run\_reward & run\_opt\_freq\\
    \hline
    eGreedy & Gaussian & 0.036 & 14816.52 & 85.19492\\
    \hline
    Optimistic initial values & Gaussian & 0.036 & 14920.88 & 87.01046\\
    \hline
    Reinforcement comparison & Gaussian & 0.061 & 15263.09 & 89.47684\\
    \hline
    UCB & Gaussian & 0.074 & 14679.89 & 73.15962\\
    \hline
  \end{tabular}
\end{table}







\end{document}
