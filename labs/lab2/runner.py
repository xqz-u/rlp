#!/usr/bin/python
import multiprocessing as mp
import os
import subprocess as sp
import time as t
from functools import reduce

import combiner as c
import configs as conf
import env_configs as econf

HOME = os.path.dirname(os.path.abspath(__file__))

c_agent_src = os.path.join(HOME, "agent", "src", "exec")
env_src = os.path.join(HOME, "env", "controller.py")

data_dir = os.path.join(HOME, "data")
if not os.path.exists(data_dir):
    os.makedirs(data_dir)


def run_agent(agent_conf: list, port, env_t):
    program = c_agent_src
    agent = [
        *[f"{flag} {v}" for flag, v in agent_conf.items() if not flag == "-f"],
        f"-p {port}",
        f"-E {env_t}",
    ]
    path = os.path.join(
        data_dir,
        reduce(lambda acc, y: acc + y, agent).replace(" ", "").replace("-", ""),
    )

    if os.path.exists(path):
        print("=======================")
        print(f"COLLISION SKIPPING CONF : {agent}")
        print("=======================")
        return

    agent_c = [*agent, f"-f {path}"]
    # print(f"Conf: {agent_c}")
    agent_proc = sp.Popen(
        program + reduce(lambda x, y: x + " " + y, agent_c, ""), shell=True
    )

    agent_proc.wait()
    print(program)
    print(path)
    print(f"Conf: {agent_c}")


def run(a_conf: tuple):
    proc_id = mp.current_process()._identity[0]
    port = 4000 + proc_id
    avg_time = 0
    for env_t, en in econf.envs3.items():
        env = list(c.env_configs(en))[0]
        env_conf = [*env, f"-p {port}"]
        # print(f"ENV conf: {env_conf}")
        env_proc = sp.Popen(
            env_src + reduce(lambda x, y: x + " " + y, env_conf, ""), shell=True
        )
        cur = t.time()
        run_agent(a_conf, port, env_t)
        cur = t.time() - cur
        avg_time = (avg_time + cur) / 2 if avg_time != 0 else cur
        print(avg_time)
        env_proc.terminate()
        env_proc.wait()


def runner(workers=16):
    for agent in conf.agents4:
        with mp.Pool(workers) as p:
            p.map(run, c.agent_configs(agent))
            p.close()
            p.join()


if __name__ == "__main__":
    runner()
