from agent import Gatherer
from environment import Environment
from reward import rewards_dict
from utils import multi_game_feature, parse_agents_config
from vector import Vector2H


class Game:
    env: Environment
    gatherer: Gatherer
    config: dict
    epoch: int
    refresh: int = -1

    def __init__(self, config):
        self.config = config
        self.env = Environment(*self.config["dimension"], self.config)
        self.start()

    def start(self) -> tuple:
        self.epoch = 0
        self.refresh += 1
        # print(f"refresh {self.refresh}")
        conf = incf_agents_seed(self.config, self.refresh)
        self.env.make_config(conf)
        self.gatherer = Gatherer(
            self.env.nodes, parse_agents_config(conf["gatherer"])[0]
        )
        # eat food if born on it
        self.env.food_left -= int(self.gatherer.loc.has_food)
        # print("=========== Game ready ============")
        return self.gatherer.percept(), *self.learning_features()

    def learning_features(self) -> tuple:
        # get the reward with the configured functions
        rwd = multi_game_feature(self, self.config["reward"], rewards_dict)[0]
        # game ends when the player is killed by a hunter or starves if the food
        # finishes, differentiate between latter two cases
        death_t = 0  # player alive
        if self.gatherer.loc.has_hunter:
            death_t = 1
        elif self.env.starve and not self.env.food_left:
            death_t = 2  # game won
        # if death_t:  # reset hunters last position
        #     for h in self.env.hunters:
        #         h.loc.set_hunter(False)
        return death_t, rwd, self.env.food_left / self.env.food_tot

    def update(self, direction: Vector2H) -> tuple:
        self.gatherer.act(direction)
        self.env.update(self.gatherer, self.epoch)
        self.epoch += 1
        return (self.gatherer.percept(), *self.learning_features())

    def __repr__(self):
        return f"{self.config}"


# messy stuff u dont wanna see
def incf_agents_seed(conf_d: dict, incr: int) -> dict:
    ret = conf_d.copy()  # do not modify original dict
    ret.pop("dimension", None)
    for k in ret.keys():
        if k not in ["hunter", "gatherer"]:
            continue
        conf = ret[k]
        if not conf:
            continue
        for pol_name in conf["policy"]:
            if (params := conf.get("params")) and params[pol_name].get("seed"):
                seeds = ret[k]["params"][pol_name]["seed"]
                ret[k]["params"][pol_name]["seed"] = (
                    [s + incr for s in seeds]
                    if isinstance(seeds, list)
                    else seeds + incr
                )
    return ret
