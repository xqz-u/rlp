import zmq


def bstr(s, mode: str):
    if mode == "e":
        s = f"{s}" if not isinstance(s, str) else s
        return s.encode("UTF-8")
    if mode == "d":
        return s.decode("UTF-8")
    return f"Error: invalid encode/decode mode {mode}"


def zmq_send(s: str, socket: zmq.Context.socket):
    socket.send(bstr(s, "e"))


# NOTE blocking on socket.rcv()
def zmq_recv(socket: zmq.Context.socket) -> str:
    return bstr(socket.recv(), "d")


def start_server(port=5555):
    ctx = zmq.Context()
    sock = ctx.socket(zmq.REP)
    sock.bind(f"tcp://*:{port}")
    print(f"server #[{port}] started")
    return sock
