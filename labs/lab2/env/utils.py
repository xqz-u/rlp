from functools import partial, reduce

from numpy import linspace

from vector import Vector2H


def merge_lists_pairwise(*lists) -> list:
    return [elt for t in zip(*lists) for elt in t]


def with_flatten_matrix(matrix: list, fn: callable = None):
    return [n if not fn else fn(n) for row in matrix for n in row]


def strfy_list(li: list) -> str:
    """
    Old `pippovate`: The line of the Vate
    Recursively concatenate a list of lists to a string
    """
    return reduce(
        lambda x, y: f"{x}{y}" if not isinstance(y, list) else x + strfy_list(y),
        li,
        "",
    )


def len_l(li):
    "length of a list of arbitrary nesting"
    return int(
        isinstance(li, list)
        and reduce(lambda acc, el: len_l(el) + acc, li, 0) + len(li)
    )


def intrange(start, stop, step=1) -> range:
    return range(int(start), int(stop), int(step))


def intmax(a, b) -> int:
    return max(int(a), int(b))


def equidistant_lines(n: int, v1: Vector2H, v2: Vector2H, axis: str) -> list:
    """Draw n equidistant lines on the x/y axis of a 2d surface described by two
    vertices v1 (lower left) and v2 (upper right)"""

    if axis == "x":
        offsets = linspace(v1.x, v2.x, n)
        return merge_lists_pairwise(offsets, [v1.y] * n, offsets, [v2.y] * n)
    if axis == "y":
        offsets = linspace(v1.y, v2.y, n)
        return merge_lists_pairwise([v1.x] * n, offsets, [v2.x] * n, offsets)
    print(f"Invalid axis: {axis}")
    return None


def inside_circle(p: Vector2H, center: Vector2H, radius: float) -> bool:
    """Compute distance from p to center: if greater than radius, p is outside
    the circle"""

    return (center - p).magnitude_squared() <= radius * radius


def grid_dist(p: Vector2H, loc: Vector2H) -> int:
    return int(abs(loc.x - p.x) + abs(loc.y - p.y))


# extension of https://www.redblobgames.com/grids/circle-drawing/
def with_squared_shape(
    center: Vector2H,
    ext: float,
    grid: list,
    fn: callable = lambda x: x,
    shape_fn: callable = lambda *_: True,
) -> list:
    """Perform `fn` on the tiles of a grid bound by a square with `center` and
    extension `ext` (so |border| = 2*ext). The shape of the wanted structure
    is determined by `shape_fn`, which must determine if any tile in the
    bounding square is inside the desired shape and accept *args, **kwargs."""

    width, height = len(grid[0]), len(grid)
    # compute bounding square with total extension 2*ext
    bottom = max(0, center.y - ext)
    top = min(height, center.y + ext)
    left = max(0, center.x - ext)
    right = min(width, center.x + ext)

    return [
        fn(grid[row][col])
        for row in intrange(bottom, top + 1)
        for col in intrange(left, right + 1)
        # check that point is inside grid (e.g. corner circles) and given shape
        if (col < width and row < height and shape_fn(Vector2H(col, row), center, ext))
    ]


with_grid_circle = partial(with_squared_shape, shape_fn=inside_circle)
with_distance_from = partial(
    with_squared_shape, shape_fn=lambda p, loc, d: grid_dist(p, loc) <= d
)


# accepted config dict:
# in = {policy_k: [name0, name1], props_k: {name0: {n_agents_k_0: _, ...},
# name1: {n_agents_k_1: _, ...}, ...}}
# returns:
# out = [{policy_k: name0, props_k: {...}}, {policy_k: name1, props_k: {...}},
# ...], where len(out) == len of wanted agent for each policy
def parse_agents_config(
    ag_conf: dict, policy_k="policy", props_k="params", n_agents_k="spawn"
) -> list:
    def policy_configs(policy_name):
        policy_params = ag_conf[props_k][policy_name]
        n_agents = len(policy_params[n_agents_k])
        # allow parameter list of length 1, meaning the same value of that
        # parameter for all agents of this policy
        norm_policy_params = map(
            lambda pv: pv * n_agents if len(pv) == 1 else pv,
            policy_params.values(),
        )
        # repeat policy_name in params just for compatibility with below fns
        return [
            {
                policy_k: policy_name,
                props_k: dict(zip(policy_params.keys(), one_agent_cfg)),
            }
            for one_agent_cfg in zip(*norm_policy_params)
        ]

    return [
        agent_conf
        for policy_agents in [policy_configs(pol) for pol in ag_conf[policy_k]]
        for agent_conf in policy_agents
    ]


# accepted config dict:
# {policy_k: name, props_k: {...}}
def game_feature(
    arg0, configurable: dict, mapper: dict, policy_k="policy", props_k="params"
):
    fn_name, fn_args = configurable[policy_k], configurable.get(props_k)
    return mapper[fn_name](arg0, **fn_args) if fn_args else mapper[fn_name](arg0)


# accepted config dict:
# {policy_k: [name0, name1, ...], props_k: {name0: {...}, ...}}
def multi_game_feature(
    arg0, configurable: dict, mapper: dict, policy_k="policy", props_k="params"
) -> list:
    policies, params = configurable[policy_k], configurable.get(props_k)
    return [
        game_feature(
            arg0,
            {policy_k: policy_name, props_k: params and params[policy_name]},
            mapper,
            policy_k,
            props_k,
        )
        for policy_name in policies
    ]
