from random import Random

from pyglet import graphics

from agent_configs import behavior_default_colors, hunter_behaviors
from constants import *
from draw import circle
from node import Node
from perception import Perception
from utils import game_feature
from vector import Vector2H


class Agent:
    grid: list
    prev_loc: Node
    loc: Node
    role: str
    rand_gen: Random
    group: graphics.Group

    # default: render in the foreground, on top of everything
    def __init__(
        self,
        grid,
        role,
        config,
        color=BLACK,
        group=graphics.OrderedGroup(2),
    ):
        self.grid = grid
        self.role = role
        self.color = color
        self.group = group
        self.spawn(*config["params"]["spawn"])
        # initialize a reproducible random generator for each new game if given
        self.rand_gen = Random(config["params"].get("seed"))

    def spawn(self, row, col):
        self.loc = self.grid[row - 1][col - 1]
        self.prev_loc = self.loc

    def act(self, direction: Vector2H):
        self.prev_loc = self.loc
        if new_loc := self.loc.neighbors.get(direction):
            self.loc = new_loc

    def render(self, batch, group=None, color=None):
        return circle(
            *self.loc.node_center[:],
            self.loc.default_radius,
            batch,
            group or self.group,
            color or self.color,
        )

    def __repr__(self):
        return f"{self.role}({self.loc})"


class Gatherer(Agent):
    perception: Perception = None

    def __init__(
        self,
        grid,
        gath_config,
        *args,
        group=graphics.OrderedGroup(3),
        **kwargs,
    ):
        super().__init__(
            grid,
            f"GATHERER@{id(self)}",
            gath_config,
            *args,
            **kwargs,
        )
        self.color = YELLOW
        if gath_config["policy"] != "no-vision":
            self.perception = Perception(grid, gath_config, self.loc.node_size)

    def act(self, direction: Vector2H):
        super().act(direction)
        # update previous location food for rendering, keep food on location to
        # compute reward
        if self.prev_loc.has_food:
            self.prev_loc.set_food(False)

    def percept(self) -> int:
        return self.perception.update(self.loc) if self.perception else self.loc

    def render(self, batch=None, group=None, color=None):
        if self.perception:
            self.perception.render(batch, group)
        return super().render(batch, group, self.color)


class Hunter(Agent):
    behavior: dict

    def __init__(
        self,
        grid,
        hunt_config,
        *args,
        **kwargs,
    ):
        super().__init__(
            grid,
            f"HUNTER@{id(self)}",
            hunt_config,
            *args,
            **kwargs,
        )
        self.color = (
            c
            if (c := hunt_config["params"].get("color"))
            else behavior_default_colors[hunt_config["policy"]]
        )
        # print(hunt_config)
        self.behavior = hunt_config

    def act(self, g: Gatherer):
        self.behavior["params"]["loc"] = self.loc
        self.behavior["params"]["gatherer_loc"] = g.loc
        self.behavior["params"]["rng"] = self.rand_gen
        # move the hunter according to its behavior
        self.loc.set_hunter(False)
        self.loc = game_feature(self.grid, self.behavior, hunter_behaviors)
        self.loc.set_hunter(True)
