from functools import partial

from constants import BLUE, PURPLE
from node import Node
from utils import with_distance_from, with_grid_circle, with_squared_shape


def shape_vision(nodes: list, fn: callable = lambda *args: args, **kwargs) -> list:
    center = kwargs["center"]
    ext = kwargs.get("r", 1)
    return fn(center, ext, nodes)


square_vision = partial(shape_vision, fn=with_squared_shape)
circular_vision = partial(shape_vision, fn=with_grid_circle)
distance_vision = partial(shape_vision, fn=with_distance_from)

vision_types = {
    "circle": circular_vision,
    "square": square_vision,
    "dist": distance_vision,
}

# TODO make compressors / state encoders configurable
# compressors = {"simple": simple_compress}


# HUNTERS
# NOTE nodes not used here, keep for compatibility (put default/pass instance?)
def random_hunter(_, **kwargs) -> Node:
    available_directions = [
        node for direction, node in kwargs["loc"].neighbors.items() if node
    ]
    rand_gen = kwargs["rng"]
    return available_directions[rand_gen.randint(0, len(available_directions) - 1)]


def directed_hunter(_, **kwargs) -> Node:
    available_directions = [
        node for direction, node in kwargs["loc"].neighbors.items() if node
    ]
    gatherer_node = kwargs["gatherer_loc"]
    short_row = 1000
    short_col = 1000
    node_row = 0
    node_col = 0
    for nodes in available_directions:
        if nodes.has_hunter:
            available_directions.remove(nodes)
    for nodes in available_directions:
        row_dif = abs(nodes.row - gatherer_node.row)
        if short_row > row_dif:
            short_row = row_dif
            node_row = nodes
        col_dif = abs(nodes.col - gatherer_node.col)
        if short_col > col_dif:
            short_col = col_dif
            node_col = nodes
    if short_row >= short_col:
        # move to node_row
        return node_row
    if short_row <= short_col:
        # move to node_col
        return node_col


hunter_behaviors = {"random": random_hunter, "directed": directed_hunter}
behavior_default_colors = {"random": PURPLE, "directed": BLUE}
