import pyglet
from pyglet.gl import glClearColor

from constants import WHITE
from game import Game


class GameWindow(pyglet.window.Window):
    batch: pyglet.graphics.Batch
    game: Game

    def __init__(self, game, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.game = game
        self.batch = pyglet.graphics.Batch()
        glClearColor(*WHITE, 1.0)

    # NOTE pyglet.shapes.whatever returns a gl object, which needs to be assigned
    # somewhere to be rendered
    def on_draw(self):
        self.clear()
        _ = self.game.gatherer.render(self.batch)
        __ = self.game.env.render(self.batch)
        self.batch.draw()
