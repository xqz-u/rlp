#!/usr/bin/python
# from pprint import pprint

import pyglet
from pyglet.window import key

import draw
import opts
from constants import *
from game import Game
from window import GameWindow
from zutils import *

# parse config for the environment
parser = opts.init_argparse()
args = parser.parse_args()
conf = opts.parse_config(parser, args)

# print("environment config:")
# pprint(conf)

# NOTE tried giving this privately to KeyControl, rendering is called but event
# handling not
game = Game(conf["game"])


def handle_killing() -> tuple:
    global game
    draw.clear_batch()
    for row in game.env.nodes:
        for n in row:
            n.set_hunter(False)
            n.set_food(False)
    return game.start()


class KeyControl(key.KeyStateHandler):
    key_map: dict = {key.UP: UP, key.DOWN: DOWN, key.LEFT: LEFT, key.RIGHT: RIGHT}

    # NOTE hack, should be used to move caret on text input
    def on_text_motion(self, motion):
        direction = self.key_map.get(motion)
        if direction:
            new_s, dead, rwd, food_frac = game.update(direction)
            print(f"{game.epoch},{game.gatherer.loc}: {new_s},{dead},{rwd},{food_frac}")
            if dead:
                handle_killing()

    def on_close(self):
        handle_killing()


class ZControl:
    protocol: dict = {
        "0": UP,
        "1": DOWN,
        "2": LEFT,
        "3": RIGHT,
        "4": "start",
        "5": "end",
    }
    wind: GameWindow
    sock: None

    def __init__(self, wind, port=5555):
        self.sock = start_server(port)
        self.wind = wind

    def die(self):
        if self.wind:
            self.wind.close()
        pyglet.app.exit()

    def handle_message(self, message):
        if not message:
            print("ERROR unparsable message")
            self.die()
            return
        # # close communication
        if message == "end":
            # print(f"env dies, epoch {game.epoch}")
            zmq_send("", self.sock)  # don't die during experiment
            return
        new_s, dead, rwd, food_frac = (
            handle_killing() if message == "start" else game.update(message)
        )
        # print(f"{game.epoch}:new state: {new_s},{dead},{rwd},{food_frac}")
        zmq_send(f"{new_s},{dead},{rwd},{food_frac}", self.sock)

    def on_receive(self, _):
        message = zmq_recv(self.sock)
        # print(f"received: {message}")
        self.handle_message(self.protocol.get(message))


def main():
    wind = None
    port = conf["port"]

    if conf["render"]:
        wind = GameWindow(game, **conf["window"])
        if not port:
            in_handler = KeyControl()
            wind.push_handlers(in_handler)

    if port:
        pyglet.clock.schedule_interval(ZControl(wind, port).on_receive, 1 / 60000000)

    pyglet.app.run()


if __name__ == "__main__":
    main()
