from utils import grid_dist, strfy_list
from vector import Vector2H


def distance_layers(agent_loc: Vector2H, dist: int, vision_field: list) -> dict:
    layers = range(1, dist + 1)
    ret = dict(zip(layers, [[[] for _ in range(4)] for _ in layers]))

    for loc in vision_field:
        dir_idx = []
        # do not save agent's own position, does not really influence state
        if loc.row > agent_loc.y:
            dir_idx.append(0)  # above agent
        if loc.row < agent_loc.y:
            dir_idx.append(1)  # below agent
        if loc.col < agent_loc.x:
            dir_idx.append(2)  # left of agent
        if loc.col > agent_loc.x:
            dir_idx.append(3)  # right of agent
        d = grid_dist(Vector2H(loc.col, loc.row), agent_loc)
        for i in dir_idx:
            ret[d][i].append(loc)

    return ret


def simple_compress(nodes: list) -> int:
    """
    'compression' rule:
    food > hunters -> food (state 1)
    food <= hunters -> hunter (state 2)
    totally empty -> emtpty (state 0)
    """

    food, hunters = 0, 0
    for n in nodes:
        # if node has both food and hunter, it really has only hunter
        food += n.has_food and not n.has_hunter
        hunters += n.has_hunter
    return 0 if not food and not hunters else 1 if food > hunters else 2


def dist_based_state(
    agent_loc: Vector2H, dist: int, vision_field: list, debug=False
) -> int:
    layers_by_dist = distance_layers(agent_loc, dist, vision_field)
    compr_layers = {
        dist: [simple_compress(dir_nodes) for dir_nodes in layer]
        for dist, layer in layers_by_dist.items()
    }
    hashable_compr = strfy_list(compr_layers.values())
    if debug:
        print(f"agent at {agent_loc}")
        print(f"vision around: {layers_by_dist}")
        print(f"compressed: {compr_layers}")
        print(f"unhashed final state: {hashable_compr}")

    return hash(hashable_compr)
