from pyglet import shapes
from pyglet.gl import *

from constants import BLACK
from utils import equidistant_lines
from vector import Vector2H

# need to store all VertexList returned by batch.add() to manipulate them...
verteces = []


def grid(w, h, nrows, ncols, batch, group=None, color=BLACK):
    win_bl, win_tr = Vector2H(0), Vector2H(w, h)

    def batch_add_lines(n, axis):
        origins = equidistant_lines(n, win_bl, win_tr, axis)
        nlines = len(origins) // 2
        batch.add(
            nlines,
            GL_LINES,
            group,
            ("v2f", origins),
            ("c3B", color * nlines),
        )

    batch_add_lines(ncols + 1, "x")
    batch_add_lines(nrows + 1, "y")


def quad(x, y, w, h, batch, group=None, color=BLACK):
    verteces.append(
        batch.add_indexed(
            4,
            GL_TRIANGLES,
            group,
            [0, 1, 2, 3, 1, 2],
            ("v2f", (x, y, x + w, y, x, y + h, x + w, y + h)),
            ("c3f", color * 4),
        )
    )


def circle(x, y, radius, batch, group=None, color=BLACK):
    return shapes.Circle(x, y, radius, batch=batch, group=group, color=color)


def clear_batch():
    global verteces
    for el in verteces:
        el.delete()
    verteces = []
