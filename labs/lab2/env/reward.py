from functools import partial, reduce


def reward_skel(game, **kwargs):
    agent = game.gatherer
    if game.env.starve and not game.env.food_left:
        return kwargs.get("win", 200)  # win this hell
    if agent.loc.has_food:
        return kwargs.get("food", 1)  # small positive
    if agent.loc.has_hunter:
        return kwargs.get("hunt", -1)  # super negative
    return kwargs.get("empty", 0)  # really small negative or 0


# NOTE wall reward = wall + empty
def bounce_reward(game, **kwargs):
    return (
        kwargs.get("wall", -1) if game.gatherer.prev_loc == game.gatherer.loc else 0
    )  # small negative or really small negative


# give a list of reward functions here to mix them together with reward_skel
def mix_rewards(fns: list, game, **kwargs):
    return reduce(lambda acc, fn: fn(game, **kwargs) + acc, fns, 0)


bounce_walls = partial(mix_rewards, [reward_skel, bounce_reward])


rewards_dict = {
    "basic": reward_skel,
    "basic+walls": bounce_walls,
}
