from os import environ

environ["PYGAME_HIDE_SUPPORT_PROMPT"] = "1"

import pygame


class Vector2H(pygame.math.Vector2):
    "Hashable pygame.math.Vector2"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def __hash__(self):
        return id(self)
