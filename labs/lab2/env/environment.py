from functools import reduce

from pyglet import graphics

from agent import Gatherer, Hunter
from draw import grid
from node import node_group
from object_configs import growers_dict
from utils import multi_game_feature, parse_agents_config, with_flatten_matrix


class Environment:
    nodes: list
    config: dict
    food_tot: int
    food_left: int
    starve: bool = False
    hunters: list = []

    def __init__(self, rows, cols, config):
        self.nodes = node_group(rows, cols)
        self.config = config

    @property
    def rows(self):
        return len(self.nodes)

    @property
    def cols(self):
        return len(self.nodes[0])

    def make_config(self, config):
        if not config:
            return  # empty env with just gatherer
        if (food_conf := config.get("food")) :
            self.config_food(food_conf)
        if (hunt_config := config.get("hunter")) :
            self.config_hunters(hunt_config)

    def config_hunters(self, hunt_conf):
        self.hunters = [
            Hunter(self.nodes, cfg) for cfg in parse_agents_config(hunt_conf)
        ]

    def config_food(self, food_conf):
        multi_game_feature(self.nodes, food_conf, growers_dict)
        # save total food for death by starvation
        self.food_tot = reduce(
            lambda acc, n: int(n.has_food) + acc,
            with_flatten_matrix(self.nodes, lambda x: x),
            0,
        )
        self.food_left = self.food_tot
        # if any of the food will regrow, the gatherer won't starve
        self.starve = all(
            map(
                lambda pol: not food_conf["params"][pol].get("dt_regrowth"),
                food_conf["policy"],
            )
        )
        # print(f"tot food: {self.food_tot}, starve: {self.starve}")

    # FIXME rethink food growth + "policy" keyword should not be used
    # here
    def update(self, g: Gatherer, time: int):
        # update total food count if agent ate
        self.food_left -= int(g.loc.has_food)
        # hunt
        for h in self.hunters:
            h.act(g)
        # when time > 0, re-grow food for each policy if re-growth interval
        # given
        if (food_conf := self.config.get("food")) and (
            params := food_conf.get("params")
        ):
            for food_pol in food_conf["policy"]:
                dt_grow = params[food_pol].get("dt_regrowth")
                if dt_grow and time and time % dt_grow == 0:
                    with_flatten_matrix(
                        self.nodes, lambda n: n.set_food(True) if n.had_food else None
                    )

    def render(self, batch, group=None):
        tile_size = self.nodes[0][0].node_size
        grid(
            self.cols * tile_size.x,
            self.rows * tile_size.y,
            self.rows,
            self.cols,
            batch,
            group or graphics.OrderedGroup(1),
        )
        return with_flatten_matrix(self.nodes, lambda n: n.render(batch, group)), [
            h.render(batch, group) for h in self.hunters
        ]
