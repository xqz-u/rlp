from pyglet import graphics

import constants as c
from draw import circle
from vector import Vector2H


class Node:
    row: int
    col: int
    position: Vector2H
    group: graphics.Group
    had_food: bool = False
    has_food: bool = False
    has_hunter: bool = False
    neighbors: dict = None

    # default: render objects on node not top of agent perception
    def __init__(self, row, col, node_size: Vector2H, group=graphics.OrderedGroup(1)):
        self.row, self.col = row, col
        # upper right corner of tile, absolute coordinates
        self.position = Vector2H(col * node_size.x, row * node_size.y)
        self.group = group

    @property
    def node_size(self):
        return Vector2H(self.position.x / self.col, self.position.y / self.row)

    @property
    def node_center(self):
        return self.position - (self.node_size / 2)

    @property
    def default_radius(self) -> float:
        return (lambda x, y: x + y)(*(self.node_size / 10)[:])

    def set_food(self, food: bool):
        self.has_food = food
        self.had_food = not food

    def set_hunter(self, hunter: bool):
        self.has_hunter = hunter

    def render(self, batch, group=None, color=c.RED):
        if not self.has_food:
            return None
        return circle(
            *self.node_center[:],
            self.default_radius,
            batch,
            group or self.group,
            color,
        )

    def __hash__(self):
        return id(self)

    def __repr__(self):
        return f"<Node({self.col},{self.row})>"


# -------------------------------------------------------------------------------


def neighborhood(node: Node, n, s, w, e):
    node.neighbors = {c.UP: n, c.DOWN: s, c.LEFT: w, c.RIGHT: e}


def node_group(rows, cols, node_size: Vector2H = c.TILE_SIZE) -> list:
    global BOTTOM, LEFT, TOP, RIGHT  # bad but u get the idea
    BOTTOM = LEFT = 0
    TOP, RIGHT = rows - 1, cols - 1

    nodes = [
        [Node(r, c, node_size) for c in range(1, cols + 1)] for r in range(1, rows + 1)
    ]

    connect_borders(nodes)
    for x in range(1, TOP):
        for y in range(1, RIGHT):
            neighborhood(
                nodes[x][y],
                nodes[x + 1][y],
                nodes[x - 1][y],
                nodes[x][y - 1],
                nodes[x][y + 1],
            )
    return nodes


def connect_corners(nodes: list):
    neighborhood(
        nodes[BOTTOM][LEFT],
        nodes[BOTTOM + 1][LEFT],
        None,
        None,
        nodes[BOTTOM][LEFT + 1],
    )
    neighborhood(
        nodes[TOP][LEFT],
        None,
        nodes[TOP - 1][LEFT],
        None,
        nodes[TOP][LEFT + 1],
    )
    neighborhood(
        nodes[BOTTOM][RIGHT],
        nodes[BOTTOM + 1][RIGHT],
        None,
        nodes[BOTTOM][RIGHT - 1],
        None,
    )
    neighborhood(
        nodes[TOP][RIGHT],
        None,
        nodes[TOP - 1][RIGHT],
        nodes[TOP][RIGHT - 1],
        None,
    )


def connect_borders(nodes: list):
    connect_corners(nodes)
    for i in range(1, max(TOP, RIGHT)):
        # init top/bottom borders
        if i < RIGHT:
            neighborhood(
                nodes[BOTTOM][i],
                nodes[BOTTOM + 1][i],
                None,
                nodes[BOTTOM][i - 1],
                nodes[BOTTOM][i + 1],
            )
            neighborhood(
                nodes[TOP][i],
                None,
                nodes[TOP - 1][i],
                nodes[TOP][i - 1],
                nodes[TOP][i + 1],
            )
        # init left/right borders
        if i < TOP:
            neighborhood(
                nodes[i][LEFT],
                nodes[i + 1][LEFT],
                nodes[i - 1][LEFT],
                None,
                nodes[i][LEFT + 1],
            )
            neighborhood(
                nodes[i][RIGHT],
                nodes[i + 1][RIGHT],
                nodes[i - 1][RIGHT],
                nodes[i][RIGHT - 1],
                None,
            )
