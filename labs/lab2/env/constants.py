from vector import Vector2H

# NOTE grid starts at (1, 1) because I am fckn dumb

UP = Vector2H(0, 1)
DOWN = Vector2H(0, -1)
LEFT = Vector2H(-1, 0)
RIGHT = Vector2H(1, 0)

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED = (255, 0, 0)
YELLOW = (255, 255, 0)
GREEN = (0, 255, 0)
GREY = (211, 211, 211)
PURPLE = (128, 0, 128)
BLUE = (0, 0, 255)


TILE_SIZE = Vector2H(40, 40)
