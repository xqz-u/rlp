from pyglet import graphics

from agent_configs import vision_types
from compressor import dist_based_state
from constants import GREEN, WHITE
from draw import quad
from node import Node
from utils import game_feature
from vector import Vector2H


class Perception:
    full_env: list  # NOTE used to simplify vision updates
    config: dict
    tile_size: Vector2H
    group: graphics.Group
    vision_field: list = []
    previous_field: list = []

    # default: render in the background, other objects will be on top
    def __init__(
        self, full_env, percept_cfg, tile_size, group=graphics.OrderedGroup(0)
    ):
        self.full_env = full_env
        self.config = percept_cfg
        self.tile_size = tile_size
        self.group = group

    def update(self, loc: Node) -> int:
        # dynamically set current position for vision grid calculation
        center = Vector2H(loc.col, loc.row)
        self.config["params"]["center"] = center - Vector2H(1)
        self.previous_field = self.vision_field
        self.vision_field = game_feature(self.full_env, self.config, vision_types)
        # return vision based state representation TODO make configurable
        return dist_based_state(center, self.config["params"]["r"], self.vision_field)

    def __render(
        self,
        abs_loc: Vector2H,
        color,
        batch,
        group=None,
    ):
        quad(
            *(abs_loc - self.tile_size)[:],
            *self.tile_size[:],
            batch,
            group or self.group,
            color,
        )

    def render(self, batch, group=None):
        def inner(locations, color):
            for loc in locations:
                self.__render(loc.position, color, batch, group)

        inner(set(self.previous_field).difference(self.vision_field), WHITE)
        inner(set(self.vision_field).difference(self.previous_field), GREEN)
