import argparse as argp
import json
import os
from pprint import pprint

import yaml

# names of the command line switches NOTE order must correspond to create_conf()
# positional parameters rn
game_conf_attrs = [
    "tile_size",
    "dimension",
    "food",
    "gatherer",
    "hunter",
    "reward",
]


# FIXME monkey patch
def patch_seed(fn):
    seed = None

    def inner(_dict, start_seed=None):
        ret = fn(_dict)
        if start_seed is None or not ret.get("params"):
            return ret
        nonlocal seed
        if seed is None:
            seed = start_seed
        for pol_name in ret["policy"]:
            n_agent = ret["params"][pol_name].get("spawn")
            if n_agent:
                ret["params"][pol_name]["seed"] = list(range(seed, seed + len(n_agent)))
                seed += len(n_agent)
            else:
                ret["params"][pol_name]["seed"] = seed
                seed += 1

        return ret

    return inner


# perform a custom action
def do_action(fn):
    class OverrideAction(argp.Action):
        def __call__(self, parser, namespace, values, option_string=None):
            # print(f"{namespace}, {values}, {self.dest}")
            setattr(namespace, self.dest, values)
            fn(values)

    return OverrideAction


# NOTE dict str should be enclosed with '', inner strings with "" for valid json
class JsonDict:
    def __call__(self, string):
        return json.loads(string)


# load config from YAML file
def file_conf(fname: str):
    with open(f"{fname}", "r") as ymlfile:
        global file_config
        file_config = yaml.safe_load(ymlfile)


def init_argparse() -> argp.ArgumentParser:
    pars = argp.ArgumentParser()

    pars.add_argument("-p", "--port", help="port for the zmq server", type=int)
    pars.add_argument("-s", "--seed", help="set the seed for randomness", type=int)
    pars.add_argument(
        "-F",
        "--file",
        help="read configuration from .yml file",
        action=do_action(file_conf),
    )
    pars.add_argument(
        "-R", "--render", help="show the pyglet environment", action="store_true"
    )
    pars.add_argument(
        "-d",
        "--dimension",
        help="size of the environment (rows x columns)",
        type=int,
        nargs=2,
        default=[10, 10],
    )
    pars.add_argument(
        "-ts",
        "--tile_size",
        help="size of tile in the environment (width, height)",
        type=int,
        nargs=2,
        default=[40, 40],
    )
    pars.add_argument(
        "-g",
        "--gatherer",
        help="(dict) specification for the gatherer",
        type=JsonDict(),
    )
    pars.add_argument(
        "-hn",
        "--hunter",
        help="(dict) specification for the hunter(s)",
        type=JsonDict(),
    )
    pars.add_argument(
        "-r",
        "--reward",
        help="(dict) specification for the reward system",
        type=JsonDict(),
    )
    pars.add_argument(
        "-f", "--food", help="(dict) specification for the food growth", type=JsonDict()
    )
    pars.add_argument(
        "-v", "--verbose", help="echo the current configuration", action="store_true"
    )
    return pars


def check_legal_cl_args(args, pars) -> dict:
    # check illegal configuration when both file and command line configs are
    # passed NOTE seed, port and render are still allowed
    if args.file and (args.gatherer or args.reward):
        pars.error("Invalid configuration: either give -F 'file' or -g and -r")
    # check for necessary configs if a config file was not passed
    if not (args.file or (args.gatherer and args.reward)):
        pars.error("Error: YAML config / gatherer and reward specifications not given")
    return {attr: getattr(args, attr) for attr in game_conf_attrs}


# NOTE port, seed and render must be given on the command line
def check_legal_file_args() -> dict:
    # check that the game configs are saved under known headers
    ret = {}
    for k in file_config.keys():
        assert (
            k in game_conf_attrs
        ), f"Invalid key: {k}, valid keys are: {game_conf_attrs}"
        ret[k] = file_config[k]
    return ret


# accept integer tuples of length 2 / configuration dictionaries of the form:
# {"policy": [name0, name1, ...], "params": {name0: {...}, name1: {...}, ...}}
# or:
# {"policy": [name0, name1, ...]}
@patch_seed
def verify_conf(conf, pol_k="policy", props_k="params") -> dict:
    # print(conf)
    assert isinstance(conf, dict)
    keys = list(conf.keys())
    assert keys in ([pol_k, props_k], [pol_k])
    policies = conf[pol_k]
    assert isinstance(policies, list)
    for pol_name in policies:
        if (params := conf.get(props_k)) is not None:
            assert isinstance(params, dict)
            pol_params = params.get(pol_name)
            assert pol_params
            assert isinstance(pol_params, dict)
    return conf


# create complete environment config with command line args, return game config
# and window config. game non-necessary fields are set to None if not given
def create_conf(argp_d: dict, seed) -> dict:
    # these are given by default so won't ever be None
    rows, cols = argp_d.pop("dimension")
    width, height = argp_d.pop("tile_size")
    return {
        "game": {
            "dimension": (rows, cols),
            **{
                feat_k: verify_conf(conf_d, seed)
                if (conf_d and conf_d != "None")
                else None
                for feat_k, conf_d in argp_d.items()
            },
        },
        "window": {
            "width": cols * width,
            "height": rows * height,
            "caption": "Hunter-gatherer",
        },
    }


def parse_config(parser: argp.ArgumentParser, args) -> dict:
    # set the seed (None is its default values anyways)
    os.environ["PYTHONHASHSEED"] = str(args.seed)
    # echo back some input
    if args.verbose:
        print(args)
        print(f"PYTHONHASHSEED value: {os.environ.get('PYTHONHASHSEED')}")
        if args.file:
            print(f"'{args.file}' content:")
            pprint(file_config)
    # get the values for the game configurations
    conf_fields_vals = (
        check_legal_file_args() if args.file else check_legal_cl_args(args, parser)
    )
    # adjust for case where port is not given and render has default value False
    # (aka player is human, avoid specifying -R again to view the game)
    return {
        "port": args.port,
        "render": True if not (args.port or args.render) else args.render,
        "seed": args.seed,
        **create_conf(conf_fields_vals, args.seed),
    }
