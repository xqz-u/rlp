import random as r

from utils import with_flatten_matrix, with_grid_circle
from vector import Vector2H


def grow_lumps(nodes: list, **kwargs) -> list:
    rows, cols = kwargs["grid_size"]
    # row / column spacing between lumps
    space_x, space_y = kwargs["spacing"]
    radius = kwargs["radius"]
    sep = kwargs.get("sep", False)
    # add possible spacing between lumps
    if sep and (isinstance(sep, (float, int))):
        print(f"sep = True, radius: {radius} -> {radius - sep}")
        radius -= sep
    # compute center of lumps (grow as circles)
    lump_centers = [
        Vector2H(x, y)
        for x in range(space_y, rows, space_y)
        for y in range(space_x, cols, space_x)
    ]
    # fill the circular lumps with food
    return [
        with_grid_circle(center, radius, nodes, fn=lambda node: node.set_food(True))
        for center in lump_centers
    ]


def grow_random(nodes: list, **kwargs) -> list:
    p = kwargs.get("p", 0.3)
    seed = kwargs.get("seed")
    r.seed(seed)
    return with_flatten_matrix(
        nodes, lambda n: n.set_food(True) if r.random() < p else None
    )


growers_dict = {"lumps": grow_lumps, "random": grow_random}
