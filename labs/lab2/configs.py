import numpy as np

base_agent = {
    "e": [4000],  # episodes
    "s": [150],  # steps
    "A": [0, 2, 3],  # algorithm
    "x": [0.6, 0.8, 0.9],  # np.linspace(0.5, 1, num=3)
    "g": [0.7, 0.85, 0.95],
}  # np.linspace(0.65, 1, num=3)

soft_agent = {
    **base_agent,
    "a": [2],  # action selector
    "t": [0.01, 0.005, 0.0005],
}  # np.linspace(0.05, 0.5, num=3)

greedy_agent = {
    **base_agent,
    "a": [1],  # action selector
    "y": [0.1, 0.05, 0.005],
}  # np.linspace(0.05, 0.5, num=3)

ucb_agent = {
    **base_agent,
    "a": [3],  # action selector
    "c": np.linspace(0.1, 1.5, num=4),
}  # ? c

random_agent = {
    "e": [5000],  # episodes
    "s": [150],  # steps
    "A": [1],  # algorithm
    "a": [0],
}  # action selector

agents = [soft_agent, greedy_agent, ucb_agent]

base_agent1 = {
    "e": [5000],  # episodes
    "s": [150],  # steps
    "A": [0, 2],  # algorithm
    "x": [0.3, 0.5, 0.7],
    "g": [0.75, 0.85, 0.95],
}  # np.linspace(0.65, 1, num=3)

soft_agent1 = {
    **base_agent1,
    "a": [2],  # action selector
    "t": [0.5, 0.1, 0.01, 0.005, 0.0005],
}  # np.linspace(0.05, 0.5, num=3)

greedy_agent1 = {
    **base_agent1,
    "a": [1],  # action selector
    "y": [0.001, 0.0005, 0.00005],
}  # np.linspace(0.05, 0.5, num=3)

agents1 = [soft_agent1, greedy_agent1, random_agent]

soft_agent2 = {
    "e": [5000],  # episodes
    "s": [150],  # steps
    "A": [0, 2],  # algorithm
    "x": [0.2, 0.4, 0.6],
    "g": [0.5, 0.6, 0.7, 0.8],
    "a": [2],  # action selector
    "t": [0.0075, 0.0025, 0.0005, 0.00005],
}

greedy_agent2 = {
    "e": [5000],  # episodes
    "s": [150],  # steps
    "A": [0],  # algorithm
    "x": [0.2, 0.4, 0.6],
    "g": [0.5, 0.6, 0.7, 0.8],
    "a": [1],  # action selector
    "y": [0.001, 0.0005, 0.00005, 0.01],
}

agents2 = [greedy_agent2, random_agent]

base_agent3 = {
    "e": [5000],  # episodes
    "s": [150],  # steps
    "A": [0, 2, 3],  # algorithm
    "x": [0.3, 0.15, 0.1],
    "g": [0.3, 0.4, 0.45, 0.55],
}

soft_agent3 = {**base_agent3, "a": [2], "t": [0.001, 0.0055, 0.01]}  # action selector

greedy_agent3 = {
    **base_agent3,
    "a": [1],  # action selector
    "y": [0.000005, 0.00075, 0.00025],
}

ucb_agent3 = {**base_agent3, "a": [3], "c": [0.01, 0.5, 0.8, 1.4]}  # action selector

agents3 = [soft_agent3, greedy_agent3, ucb_agent3]

ucb_agent3 = {**base_agent3, "a": [3], "c": [0.01, 0.5, 0.8, 1.4]}  # action selector

agents3 = [soft_agent3, greedy_agent3, ucb_agent3]

ucb_agent4 = {
    "e": [5000],  # episodes
    "s": [150],  # steps
    "A": [0, 2, 3],  # algorithm
    "x": [0.15],
    "g": [0.4, 0.55],
    "a": [3],  # action selector
    "c": [0.000001],
}

agents4 = [ucb_agent4]
