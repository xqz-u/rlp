//#include "test5.h"
//#include "qlDouble.h"
#include "ops.h"
extern "C" {
#include "zhelpers.h"
#include <zmq.h>
}

void makeExperiment(int episodes, int steps, int runs, string port,
			string stats_file, string env_t, double alpha, double gamma,
			double eps, double tau, double c, size_t algooIdx,
			size_t expplIdx) {
  void *context = zmq_ctx_new();
  void *requester = zmq_socket(context, ZMQ_REQ);
  string con = "tcp://localhost:";
  con.append(port);
  char const *conCor = con.data();
  zmq_connect(requester, conCor);
  maxSteps = steps;
  nrRuns = runs;
  algoIdx = algooIdx;
  explIdx = expplIdx;
  seed = 0;
  seed2 = 0;
  outFileName = stats_file;
  env_tt = env_t;
  lr = alpha;
  gammma = gamma;
  softmaxTemperature = tau;
  ucbConstC = c;

  if (algooIdx == 2) {
    if (expplIdx == 1)
      runExpGreedy(requester, episodes);
    if (expplIdx == 3)
      runExpUcb(requester, episodes);
    if (expplIdx == 2)
      runExpSoftmax(requester, episodes);
  } else {
    if (expplIdx == 1)
      runExpGreedyDql(requester, episodes);
    if (expplIdx == 3)
      {
	cout << "gnaa" << endl;
	runExpUcbDql(requester, episodes);
	cout << "gnee??" << endl;
      }
    if (expplIdx == 2)
      runExpSoftmaxDql(requester, episodes);
  }
}
