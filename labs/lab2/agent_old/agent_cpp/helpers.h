#include <vector>
#include <iostream>
#include <unordered_set>
#include <unordered_map>
#include <algorithm>

// FIXED
// Used to find a duplicate of a specific value within an array (Close to O(n))
template<typename Iter>
std::vector<int> get_duplicate_indices(Iter first, Iter last, double val){
    std::unordered_set<int> rtn;
    std::unordered_map<double, double> dup;
    for(std::size_t i = 0; first != last; ++i, ++first){
        auto iter_pair = dup.insert(std::make_pair(*first, i));
        //std::cout << "this is first: " << *first << " this is valll: "<< val <<"\n";
        //std::cout << "iter pair second: " << iter_pair.second << "\n";
        if(!iter_pair.second && *first == val){
            rtn.insert(iter_pair.first->second);
            //std::cout << "i: " << i << "\n";
            //std::cout << "This is first: "<< *first << " This is val:"  << val << "\n";
            rtn.insert(i);
        }
    }
    return {rtn.begin(), rtn.end()};
}

/*
qTable[1][0] = -0.999992;
qTable[1][1] = -0.960392;
qTable[1][2] = -1;
qTable[1][3] = -0.979789;
*/
