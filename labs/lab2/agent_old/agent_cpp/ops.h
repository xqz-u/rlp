// TODO make sure data is updated correctly - DONE
// TODO make sure data dump is in every experiment - DONE
// TODO change messaging - DONE
// TODO integrate global variables in combinedAlgos
// TODO change rewards to doubles - DONE
// TODO total reward - DONE
// TODO runs - DONE
// TODO create file at begining of run then append - DONE
// TODO re-init counters for UCB at each run - DONE
#include "helpers.h"
#include <cmath>
#include <fstream>
#include <iostream>
#include <map>
#include <random>
#include <sstream>
#include <string>
extern "C" {
#include "zhelpers.h"
#include <zmq.h>
}
using namespace std;

int seed = 223;
int seed2 = 223;

// Stuff for data dump + extras
string outFileName; // output filename
size_t algoIdx;     // index of algorithm running experiment
size_t explIdx;     // index of exploration strategy
string env_tt;      // name of env policy
double food_left;
size_t nrSteps = 0;
size_t wrldSize;
size_t maxSteps;
double totalReward = 0;
size_t nrRuns;
//

std::random_device rd; // obtain a random number from hardware
std::uniform_int_distribution<>
    distr(0, 3); // define the range (0 to 3 - reflects actions)

bool dead = 0;
double epsilon = 0.13; // chance of random action
double lr = 0.9;       // learning rate
double gammma = 0.8;   // discount factor
long int currentState;
size_t currentEpisode = 0;

// For ucb
map<long int, int> stateCounter;
typedef array<int, 4> ucbArr;
double ucbConstC = 1;

// For softMax
double softmaxTemperature = 0.5;

typedef array<double, 4> actionArr; // used for map

char *commands[] = {"0", "1", "2", "3"};

void openDataDump(string fileName) {
  ofstream outfile;
  outfile.open(fileName);
  outfile
      << "ep,learner,act_sel,alpha,gamma,eps,tau,c,env_t,rwd,food_left,steps\n";
  outfile.close();
}

/*
void openDataDump(string fileName, size_t algoIdx, size_t explIdx, double alpha,
  double discountFactor, double eps, double tau, double C, string env_tt, double
rwd, double food_left, size_t steps, size_t world_sz)
{
  ofstream outfile;
  outfile.open(fileName);
  if (currentEpisode == 0)
  {
    outfile <<
"ep,learner,act_sel,alpha,gamma,eps,tau,c,env_t,rwd,food_left,steps,world_sz\n";
    outfile << currentEpisode << "," << algoIdx << "," << explIdx << "," <<
alpha << ","; outfile << discountFactor << "," << eps << "," << tau << "," << C
<< "," << env_tt; outfile << "," << rwd << "," << rwd << "," << food_left << ","
<< steps << "," << world_sz; outfile << "\n";
  }
}
*/

void appendDataDump(string fileName, size_t algoIdx, size_t explIdx,
			   double alpha, double discountFactor, double eps, double tau,
			   double C, string env_tt, double rwd, double food_left,
			   size_t steps, size_t world_sz) {
  ofstream outfile;
  outfile.open(fileName, ios_base::app);
  outfile << currentEpisode << "," << algoIdx << "," << explIdx << "," << alpha
          << ",";
  outfile << discountFactor << "," << eps << "," << tau << "," << C << ","
          << env_tt;
  outfile << "," << rwd << "," << food_left << "," << steps;
  outfile << "\n";
  outfile.close();
}

void split(const string &s, double *output) {
  char delim = ',';
  vector<string> result;
  stringstream ss(s);
  string item;

  while (getline(ss, item, delim))
    result.push_back(item);
  for (size_t i = 0; i < 4; ++i)
    output[i] = stod(result[i]);
}

int findMaxIndex(map<long int, actionArr> &qTable) {
  int index = -1;
  double max = -100000;
  for (size_t i = 0; i < 4; ++i)
    if (qTable[currentState][i] > max) {
      max = qTable[currentState][i];
      index = i;
    }
  return index;
}

double findMax(map<long int, actionArr> &qTable, long int state) {
  double max = -100000;
  for (size_t i = 0; i < 4; ++i)
    if (qTable[state][i] > max)
      max = qTable[state][i];
  return max;
}

void updateTableDql(map<long int, actionArr> &qTableA,
                    map<long int, actionArr> &qTableB, long int initState,
                    int action, double reward, long int newState) {
  double some = qTableB[newState][findMaxIndex(qTableA)];
  qTableA[initState][action] =
      qTableA[initState][action] +
      lr * (reward + (gammma * some) - qTableA[initState][action]);
}

void updateTable(map<long int, actionArr> &qTable, long int oldState,
                 long int newState, double reward, int action) {
  qTable[oldState][action] =
      qTable[oldState][action] +
      (lr * (reward + (gammma * findMax(qTable, newState)) -
             qTable[oldState][action]));
}

void greedyRun(map<long int, actionArr> &qTable, auto &requester) {
  double chance = (double)rand() / RAND_MAX;
  int action = 5;
  bool check = 0;
  for (size_t i = 0; i < 4; ++i) {
    if (qTable[currentState][i] != 0)
      check = 1;
  }

  if (check == 0) {
    std::mt19937 gen(seed2); // seed the generator
    action = distr(gen);
    seed2++;
  } else {
    if (chance < epsilon) {
      std::mt19937 gen(seed2); // seed the generator
      action = distr(gen);
      seed2++;
    } else {
      action = findMaxIndex(qTable);
    }
  }
  s_send(requester, commands[action]);
  string reply = s_recv(requester);
  long int oldState = currentState;
  double replyArg[4];
  split(reply, replyArg);
  currentState = (long int)replyArg[0];
  dead = (bool)replyArg[1];
  food_left = replyArg[3];
  totalReward += replyArg[2];
  updateTable(qTable, oldState, currentState, replyArg[2], action);
}

void start(auto &requester) {
  s_send(requester, "4");
  string reply = s_recv(requester);
  double replyArg[4];
  split(reply, replyArg);
  currentState = (long int)replyArg[0];
  dead = (bool)replyArg[1];
  food_left = replyArg[3];
}

void sendActionAndUpdate(map<long int, actionArr> &qTable, auto &requester,
                            int action) {
  s_send(requester, commands[action]);
  string reply = s_recv(requester);
  double replyArg[4];
  split(reply, replyArg);
  long int oldState = currentState;
  currentState = (long int)replyArg[0];
  dead = (bool)replyArg[1];
  food_left = replyArg[3];
  totalReward += replyArg[2];
  updateTable(qTable, oldState, currentState, replyArg[2], action);
}

void sendActionAndUpdateUcb(map<long int, actionArr> &qTable,
                            map<long int, ucbArr> &ucbTable, auto &requester,
                            int action) {
  stateCounter[currentState]++;
  ucbTable[currentState][action]++;
  s_send(requester, commands[action]);
  string reply = s_recv(requester);
  double replyArg[4];
  split(reply, replyArg);
  long int oldState = currentState;
  currentState = (long int)replyArg[0];
  dead = (bool)replyArg[1];
  food_left = replyArg[3];
  totalReward += replyArg[2];
  updateTable(qTable, oldState, currentState, replyArg[2], action);
}

void sendActionAndUpdateDqn(map<long int, actionArr> &qTableA,
                            map<long int, actionArr> &qTableB, auto &requester,
                            long int action) {
  s_send(requester, commands[action]);
  string reply = s_recv(requester);
  double replyArg[4];
  split(reply, replyArg);
  long int oldState = currentState;
  currentState = (long int)replyArg[0];
  dead = (bool)replyArg[1];
  food_left = replyArg[3];
  totalReward += replyArg[2];
  double chance = (double)rand() / RAND_MAX;

  if (chance > 0.5)
    updateTableDql(qTableA, qTableB, oldState, action, replyArg[2],
                   currentState);
  else
    updateTableDql(qTableB, qTableA, oldState, action, replyArg[2],
                   currentState);
}

void sendActionAndUpdateDqnUcb(map<long int, actionArr> &qTableA,
                               map<long int, actionArr> &qTableB,
                               map<long int, ucbArr> &ucbTable, auto &requester,
                               long int action) {
  s_send(requester, commands[action]);
  stateCounter[currentState]++;
  ucbTable[currentState][action]++;
  string reply = s_recv(requester);
  double replyArg[4];
  split(reply, replyArg);
  long int oldState = currentState;
  currentState = (long int)replyArg[0];
  dead = (bool)replyArg[1];
  food_left = replyArg[3];
  totalReward += replyArg[2];
  double chance = (double)rand() / RAND_MAX;

  if (chance > 0.5)
    updateTableDql(qTableA, qTableB, oldState, action, replyArg[2],
                   currentState);
  else
    updateTableDql(qTableB, qTableA, oldState, action, replyArg[2],
                   currentState);
}

void ucbActionDql(map<long int, actionArr> &qTableA,
                  map<long int, actionArr> &qTableB,
                  map<long int, ucbArr> &ucbTable, auto &requester) {
  // determine whether all actions have been selected
  int actionZero = -1;
  for (size_t i = 0; i < 4; ++i) {
    if (ucbTable[currentState][i] == 0) {
      actionZero = i;
      break;
    }
  }
  if (actionZero != -1)
    sendActionAndUpdateDqnUcb(qTableA, qTableB, ucbTable, requester,
                              actionZero);
  else {
    // find argmax(action = qTableA + qTableB + bonus)
    double max = -1000000;
    int index = -1;
    map<long int, actionArr> total;
    for (size_t i = 0; i < 4; ++i) {
      double bonus = ucbConstC * sqrt(2 * ((log(stateCounter[currentState]) /
                                            ucbTable[currentState][i])));
      total[currentState][i] =
          qTableA[currentState][i] + qTableB[currentState][i] + bonus;
      if (total[currentState][i] > max) {
        max = total[currentState][i];
        index = i;
      }
    }
    sendActionAndUpdateDqnUcb(qTableA, qTableB, ucbTable, requester, index);
  }
}

void ucbAction(map<long int, actionArr> &qTable,
               map<long int, ucbArr> &ucbTable, auto &requester) {
  // determine whether all actions have been selected
  int actionZero = -1;
  for (size_t i = 0; i < 4; ++i) {
    if (ucbTable[currentState][i] == 0) {
      actionZero = i;
      break;
    }
  }
  if (actionZero != -1)
    sendActionAndUpdateUcb(qTable, ucbTable, requester, actionZero);
  else {
    // find argmax(action = qTable + bonus)
    double max = -1000000;
    int index = -1;
    map<long int, actionArr> total;
    for (size_t i = 0; i < 4; ++i) {
      double bonus = ucbConstC * sqrt(2 * ((log(stateCounter[currentState]) /
                                            ucbTable[currentState][i])));
      total[currentState][i] = qTable[currentState][i] + bonus;
      if (qTable[currentState][i] + bonus > max) {
        max = qTable[currentState][i] + bonus;
        index = i;
      }
    }
    sendActionAndUpdateUcb(qTable, ucbTable, requester, index);
  }
}

void softMax(map<long int, actionArr> &qTable, auto &requester) {
  // softmax P(s_t, a) = (e^(Q(s,a)/T)) / (Sum e^(Q(s,a)/T))
  long int oldState = currentState;
  int action = -1;
  bool check = 0;
  for (size_t i = 0; i < 4; ++i) {
    if (qTable[currentState][i] != 0)
      check = 1;
  }
  if (check == 0) {
    std::mt19937 gen(seed2); // seed the generator
    action = distr(gen);
    seed2++;
  } else {
     long double sum = 0;
    for (size_t i = 0; i < 4; ++i)
      sum += exp(( long double)qTable[oldState][i] / softmaxTemperature);

     long double prob[4];
    for (size_t i = 0; i < 4; ++i)
      prob[i] = exp(( long double)qTable[oldState][i] / softmaxTemperature) / sum;

     long double choice =
        ( long double)rand() / RAND_MAX; // samples uniformally in range [0, 1]
     long double cumulativeProb = 0;
    for (size_t i = 0; i < 4; ++i) {
      cumulativeProb += prob[i];
      if (cumulativeProb > choice) {
        action = i;
        break;
      }
    }
  }
  sendActionAndUpdate(qTable, requester, action);
}

void softMaxDql(map<long int, actionArr> &qTableA,
                   map<long int, actionArr> &qTableB, auto &requester) {
  // softmax P(s_t, a) = (e^(Q(s,a)/T)) / (Sum e^(Q(s,a)/T))
  long int oldState = currentState;
  int action = -1;
  bool check = 0;
  for (size_t i = 0; i < 4; ++i)
    if (qTableA[currentState][i] != 0 || qTableB[currentState][i] != 0)
      check = 1;
  if (check == 0) {
    std::mt19937 gen(seed2); // seed the generator
    action = distr(gen);
    seed2++;
  } else {
    long double sum = 0;
    for (size_t i = 0; i < 4; ++i)
      sum += exp((long double)(qTableA[oldState][i] + qTableB[oldState][i]) /
                 softmaxTemperature);

    long double prob[4];
    for (size_t i = 0; i < 4; ++i)
      prob[i] = exp((long double)(qTableA[oldState][i] + qTableB[oldState][i]) /
                    softmaxTemperature) /
                sum;

    long double choice =
        (long double)rand() / RAND_MAX; // samples uniformally in range [0, 1]
    long double cumulativeProb = 0;
    for (size_t i = 0; i < 4; ++i) {
      cumulativeProb += prob[i];
      if (cumulativeProb > choice) {
        action = i;
        break;
      }
    }
  }
  sendActionAndUpdateDqn(qTableA, qTableB, requester, action);
}

void runExpSoftmax(auto &requester, size_t maxEpisodes) {
  openDataDump(outFileName);
  for (size_t run = 0; run < nrRuns; ++run) {
    map<long int, actionArr> qTable;
    currentEpisode = 0;
    for (size_t i = 0; i < maxEpisodes; ++i) {
      totalReward = 0;
      nrSteps = 0;
      // cout << currentEpisode << "\n";
      srand(seed);
      start(requester);
      bool check = 0;
      for (size_t i = 0; i < 4; ++i) {
        if (qTable[currentState][i] != 0)
          check = 1;
      }
      if (check == 0) {
        std::mt19937 gen(seed2); // seed the generator
        int action = distr(gen);
        seed2++;
        sendActionAndUpdate(qTable, requester, action);
      }
      nrSteps++;
      while (!dead) {
        softMax(qTable, requester);
        nrSteps++;
        if (nrSteps >= maxSteps)
          break;
      }
      appendDataDump(outFileName, algoIdx, explIdx, lr, gammma, epsilon,
                     softmaxTemperature, ucbConstC, env_tt, totalReward,
                     food_left, nrSteps, wrldSize);
      currentEpisode++;
      seed++;
      seed2 = seed;
    }
  }
}

void runExpUcb(auto &requester, size_t maxEpisodes) {
  openDataDump(outFileName);
  for (size_t run = 0; run < nrRuns; ++run) {
    stateCounter.clear();
    map<long int, actionArr> qTable;
    map<long int, ucbArr> ucbTable;
    currentEpisode = 0;
    for (size_t i = 0; i < maxEpisodes; ++i) {
      totalReward = 0;
      nrSteps = 0;
      // cout << currentEpisode << "\n";
      srand(seed);
      start(requester);
      bool check = 0;
      for (size_t i = 0; i < 4; ++i) {
        if (qTable[currentState][i] != 0)
          check = 1;
      }
      if (currentEpisode == 0 && check == 0) // select randomly at first episode
      {
        std::mt19937 gen(seed2); // seed the generator
        int action = distr(gen);
        seed2++;
        sendActionAndUpdateUcb(qTable, ucbTable, requester, action);
        nrSteps++;
      }
      while (!dead) {
        ucbAction(qTable, ucbTable, requester);
        nrSteps++;
        if (nrSteps >= maxSteps)
          break;
      }
      appendDataDump(outFileName, algoIdx, explIdx, lr, gammma, epsilon,
                     softmaxTemperature, ucbConstC, env_tt, totalReward,
                     food_left, nrSteps, wrldSize);
      currentEpisode++;
      seed++;
      seed2 = seed;
    }
  }
}

void runExpGreedy(auto &requester, size_t maxEpisodes) {
  openDataDump(outFileName);
  for (size_t run = 0; run < nrRuns; ++run) {
    map<long int, actionArr> qTable;

    currentEpisode = 0;

    for (size_t i = 0; i < maxEpisodes; ++i) {
      totalReward = 0;
      nrSteps = 0;
      // cout << currentEpisode << "\n";
      srand(seed);
      start(requester);

      bool allZero = 1;

      for (size_t i = 0; i < 4; ++i)
        if (qTable[currentState][i] != 0)
          allZero = 0;

      int action = 5;
      if (allZero == 1) {
        std::mt19937 gen(seed2); // seed the generator
        action = distr(gen);
        seed2++;
      } else
        greedyRun(qTable, requester);
      // sendActionAndUpdate(qTable, requester, action);
      nrSteps++;
      while (!dead) {
        greedyRun(qTable, requester);
        nrSteps++;
        if (nrSteps >= maxSteps)
          break;
      }
      appendDataDump(outFileName, algoIdx, explIdx, lr, gammma, epsilon,
                     softmaxTemperature, ucbConstC, env_tt, totalReward,
                     food_left, nrSteps, wrldSize);

      currentEpisode++;
      seed++;
      seed2 = seed;
    }
  }
}

int findMaxIndexDQN(map<long int, actionArr> &qTableA,
                    map<long int, actionArr> &qTableB, long int state) {
  double max = -10000000;
  int index = -1;
  for (size_t i = 0; i < 4; ++i)
    if (qTableA[state][i] + qTableB[state][i] > max) {
      max = qTableA[state][i] + qTableB[state][i];
      index = i;
    }
  return index;
}

void greedyDql(map<long int, actionArr> &qTableA,
               map<long int, actionArr> &qTableB, auto &requester) {
  double chance = (double)rand() / RAND_MAX;
  bool check = 0;
  for (size_t i = 0; i < 4; ++i)
    if (qTableA[currentState][i] != 0 || qTableB[currentState][i] != 0)
      check = 1;

  if (check == 0) {
    std::mt19937 gen(seed2); // seed the generator
    int action = distr(gen);
    seed2++;
    sendActionAndUpdateDqn(qTableA, qTableB, requester, action);
  } else {
    if (chance < epsilon) {
      std::mt19937 gen(seed2); // seed the generator
      int action = distr(gen);
      seed2++;
      sendActionAndUpdateDqn(qTableA, qTableB, requester, action);
    } else {
      int index = findMaxIndexDQN(qTableA, qTableB, currentState);
      sendActionAndUpdateDqn(qTableA, qTableB, requester, index);
    }
  }
}

void initDql(map<long int, actionArr> &qTableA,
             map<long int, actionArr> &qTableB, auto &requester) {
  long int initState = currentState;
  std::mt19937 gen(seed2); // seed the generator
  int action = distr(gen);
  seed2++;
  s_send(requester, commands[action]); // do action
  string replyTwo = s_recv(requester);
  double replyArg[4];
  split(replyTwo, replyArg);
  currentState = (long int)replyArg[0];
  dead = (bool)replyArg[1];
  food_left = replyArg[3];
  totalReward += replyArg[2];

  double chance =
      (double)rand() / RAND_MAX; // samples uniformally in range [0, 1]
  if (chance > 0.5)              // update qTableA
    updateTableDql(qTableA, qTableB, initState, action, replyArg[2],
                   currentState);
  else // update qTableB
    updateTableDql(qTableB, qTableA, initState, action, replyArg[2],
                   currentState);
}

void initDqlUcb(map<long int, actionArr> &qTableA,
                map<long int, actionArr> &qTableB,
                map<long int, ucbArr> &ucbTable, auto &requester) {
  long int initState = currentState;
  std::mt19937 gen(seed2); // seed the generator
  int action = distr(gen);
  seed2++;
  s_send(requester, commands[action]); // do action
  ucbTable[initState][action]++;
  stateCounter[initState]++;
  string replyTwo = s_recv(requester);
  double replyArg[4];
  split(replyTwo, replyArg);
  currentState = (long int)replyArg[0];
  dead = (bool)replyArg[1];
  food_left = replyArg[3];
  totalReward += replyArg[2];

  double chance =
      (double)rand() / RAND_MAX; // samples uniformally in range [0, 1]
  if (chance > 0.5)              // update qTableA
    updateTableDql(qTableA, qTableB, initState, action, replyArg[2],
                   currentState);
  else // update qTableB
    updateTableDql(qTableB, qTableA, initState, action, replyArg[2],
                   currentState);
}

void runExpGreedyDql(auto &requester, size_t maxEpisodes) {
  openDataDump(outFileName);
  for (size_t run = 0; run < nrRuns; ++run) {
    map<long int, actionArr> qTableA;
    map<long int, actionArr> qTableB;
    currentEpisode = 0;

    for (size_t i = 0; i < maxEpisodes; ++i) {
      totalReward = 0;
      nrSteps = 0;
      // cout << currentEpisode << "\n";
      srand(seed);
      start(requester);
      initDql(qTableA, qTableB, requester);
      nrSteps++;
      while (!dead) {
        greedyDql(qTableA, qTableB, requester);
        nrSteps++;
        if (nrSteps >= maxSteps)
          break;
      }
      appendDataDump(outFileName, algoIdx, explIdx, lr, gammma, epsilon,
                     softmaxTemperature, ucbConstC, env_tt, totalReward,
                     food_left, nrSteps, wrldSize);
      currentEpisode++;
      seed++;
      seed2 = seed;
    }
  }
}

void runExpUcbDql(auto &requester, size_t maxEpisodes) {
  openDataDump(outFileName);
  for (size_t run = 0; run < nrRuns; ++run) {
    stateCounter.clear();
    map<long int, actionArr> qTableA;
    map<long int, actionArr> qTableB;
    map<long int, ucbArr> ucbTable;
    currentEpisode = 0;
    // cout << "DQL ucb\n";

    for (size_t i = 0; i < maxEpisodes; ++i) {
      totalReward = 0;
      nrSteps = 0;
      // cout << currentEpisode << "\n";
      // cout << epsilon << "\n";
      srand(seed);
      start(requester);
      bool check = 0;
      for (size_t i = 0; i < 4; ++i)
        if (qTableA[currentState][i] != 0 || qTableB[currentState][i] != 0)
          check = 1;
      break;
      if (check == 0) {
        initDqlUcb(qTableA, qTableB, ucbTable, requester);
        nrSteps++;
      }
      while (!dead) {
        ucbActionDql(qTableA, qTableB, ucbTable, requester);
        nrSteps++;
        if (nrSteps > maxSteps)
          break;
      }
      appendDataDump(outFileName, algoIdx, explIdx, lr, gammma, epsilon,
                     softmaxTemperature, ucbConstC, env_tt, totalReward,
                     food_left, nrSteps, wrldSize);
      currentEpisode++;
      seed++;
      seed2 = seed;
    }
  }
}

void runExpSoftmaxDql(auto &requester, size_t maxEpisodes) {
  openDataDump(outFileName);
  for (size_t run = 0; run < nrRuns; ++run) {
    map<long int, actionArr> qTableA;
    map<long int, actionArr> qTableB;
    currentEpisode = 0;

    for (size_t i = 0; i < maxEpisodes; ++i) {
      totalReward = 0;
      nrSteps = 0;
      // cout << currentEpisode << "\n";
      srand(seed);
      start(requester);
      bool check = 0;
      for (size_t i = 0; i < 4; ++i)
        if (qTableA[currentState][i] != 0 || qTableB[currentState][i] != 0)
          check = 1;
      if (check == 0) {
        initDql(qTableA, qTableB, requester);
        nrSteps++;
      }
      while (!dead) {
        softMaxDql(qTableA, qTableB, requester);
        nrSteps++;
        if (nrSteps >= maxSteps)
          break;
      }
      appendDataDump(outFileName, algoIdx, explIdx, lr, gammma, epsilon,
                     softmaxTemperature, ucbConstC, env_tt, totalReward,
                     food_left, nrSteps, wrldSize);
      currentEpisode++;
      seed++;
      seed2 = seed;
    }
  }
}
