#include "combinedAlgos.h"
#include <iostream>
#include <unistd.h>

#define _(CHAR, BODY)                                                          \
  case CHAR:                                                                   \
    WHENARG(BODY)
#define WHENARG(BODY)                                                          \
  if (optarg)                                                                  \
    BODY;                                                                      \
  break;
#define GetOption(STRING, BODY)                                                \
  char __gensym;                                                               \
  while ((__gensym = getopt(argc, argv, STRING)) != -1) {                      \
    switch (__gensym) { BODY }                                                 \
  }

using namespace std;

int main(int argc, char **argv)
{
  int episode= 0, steps=0, runs=0, algo=0, action_sel=0;
  double alpha=0, gamma=0, tau=0, eps=0, c=0;
  char *port, *file, *env_type;

  GetOption(
      "e:s:r:A:a:E:x:g:t:y:c:S:p:f:",
      _('e', episode = atoi(optarg)) _('s', steps = atoi(optarg))
          _('r', runs = atoi(optarg)) _('A', algo = atoi(optarg))
              _('a', action_sel = atoi(optarg)) _('p', port = optarg)
                  _('f', file = optarg) _('E', env_type = optarg)
                      _('x', alpha = atof(optarg)) _('g', gamma = atof(optarg))
                          _('t', tau = atof(optarg)) _('y', eps = atof(optarg))
                              _('c', c = atof(optarg)));

  makeExperiment(episode, steps, runs, port, file, env_type, alpha, gamma, eps,
                 tau, c, algo, action_sel);
}
