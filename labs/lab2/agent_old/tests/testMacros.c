#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define allowSemiCol(Expr)                                                     \
  do {                                                                         \
    Expr                                                                       \
  } while (0)

#define timeExpr(Expr)                                                         \
  allowSemiCol({                                                               \
    clock_t begin = clock();                                                   \
    /* here, do your time-consuming job */                                     \
    Expr clock_t end = clock();                                                \
    printf("Timed expression: \'" #Expr "\'\n");                               \
    double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;                \
    printf("t: %lf\n", time_spent);                                            \
  })

void testExpand(void) {
  int i = 0;
  timeExpr(i += 2; printf("this is timed too!"););
  printf("%d\n", i);
}

int main(void) {
  testExpand();
  return 0;
}
