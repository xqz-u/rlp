#include "../include/judyDefs.h"
#include "../include/misc.h"

Performance stupid(double a, int b, int c) { return (Performance){a, b, c}; }

void testt(void) {
  Pvoid_t arr = (Pvoid_t)NULL;
  Performance p, *pp;

  for (int i = 0; i < 4; ++i)
    for (int j = 0; j < 3; ++j)
      saveStats(&arr, i, j, stupid(rand(), (int)rand(), (int)rand()));

  for (int i = 0; i < 4; ++i) {
    printf("i: %d\n", i);
    for (int j = 0; j < 3; ++j) {
      printf("\tj: %d: ", j);
      pp = getStats(arr, i, j);
      printf("%.3lf %d %d\n", pp->ep_rwd, pp->moves, pp->killed);
    }
  }

  freeStats(arr);
  /* freeJLM(arr); */
}

/* void testStats(void) { */
/*   Pvoid_t arr = (Pvoid_t)NULL; */

/*   Performance *cont = malloc(sizeof(Performance)); */

/*   Performance p = stupid(2.4, 4, 1); */
/*   saveStats(&arr, 1, 1, cont); */

/*   p = stupid(47.3, 3, 4); */
/*   saveStats(&arr, 1, 2, &p); */

/*   Performance *pp = getStats(arr, 1, 1); */
/*   printf("%.3lf %d %d\n", pp->ep_rwd, pp->moves, pp->killed); */

/*   pp = getStats(arr, 1, 2); */
/*   printf("%.3lf %d %d\n", pp->ep_rwd, pp->moves, pp->killed); */

/*   freeJLM(arr); */
/* } */

void test_ins(PPvoid_t parr, Performance *pp, Word_t idx) {
  PPvoid_t ppv;
  JLI(ppv, *parr, idx);
  *ppv = pp;
}

void testPerf(void) {
  Pvoid_t arr = (Pvoid_t)NULL;
  Performance p = (Performance){2.4, 4, 1};

  test_ins(&arr, &p, 5);

  Performance **pp;
  JLG(pp, arr, 5);
  printf("%.3lf %d %d\n", (*pp)->ep_rwd, (*pp)->moves, (*pp)->killed);

  int rc;
  JLFA(rc, arr);
}

void testDoubleArr(void) {
  Pvoid_t arr = (Pvoid_t)NULL;
  double *st = calloc(3, sizeof(*st));
  st[1] = 5;
  st[2] = 6.8;

  PPvoid_t pw;
  JLI(pw, arr, 5);
  *pw = st;

  double **pst;
  JLG(pst, arr, 5);
  for (int i = 0; i < 3; ++i)
    printf("%.2lf\n", (*pst)[i]);

  int rc;
  JLFA(rc, arr);
  free(st);
}

/* void testArr(void) { */
/*   Pvoid_t arr = (Pvoid_t)NULL; */
/*   Performance p = (Performance){2.4, 4, 1}; */
/*   setJLAPerf(&arr, 5, p); */

/*   Performance fb = (Performance){0, 0, 0}; */
/*   Performance *pp = safe_getJLA_Performance_with(&arr, 5, fb); */
/*   printf("%.3lf %d %d\n", pp->ep_rwd, pp->moves, pp->killed); */

/*   freeJLA(arr); */
/* } */

/* void test(void) { */
/*   Pvoid_t arr = (Pvoid_t)NULL; */
/*   Performance p = (Performance){2.4, 4, 1}; */
/*   setJLMPerf(&arr, 5, 10, p); */

/*   Performance fb = (Performance){0, 0, 0}; */
/*   Performance *pp = safe_getJLM_Performance_with(&arr, 5, 10, fb); */
/*   printf("%.3lf %d %d\n", pp->ep_rwd, pp->moves, pp->killed); */

/* p = (Performance){2.5, 3, 0}; */
/* setJLMPerf(&arr, 5, 11, p); */
/* setJLMPerf(&arr, 10, 11, p); */

/* setJLMD(&arr, 5, 10, 10.4); */
/* setJLMD(&arr, 5, 11, 10); */
/* setJLMD(&arr, 5, 12, 0); */

/* printJLMD(arr); */

/* freeJLM(arr); */
/* } */

int main(void) {
  /* testArr(); */
  /* testDoubleArr(); */
  /* testStats(); */
  testt();
  /* testPerf(); */
  /* test(); */
  return 0;
}
