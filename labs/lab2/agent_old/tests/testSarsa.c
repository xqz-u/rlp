#include <stdio.h>
#include <stdlib.h>

#include "../include/experiment.h"
#include "../include/utils.h"

int main(int argc, char **argv) {
  char *port = "4444";
  char *fout =
      "/home/marcog/Desktop/uni/thirdYear/block-1b/RLP/labs/lab2/agent/tests/"
      "test_sarsa.csv";
  char *env_t = "test_env";

  Params *pp = parseParams(port, fout, env_t, 0.5, 0.95, 0.1, 0.5, 0.1);

  int n_episodes = 10;
  int steps = 100;
  int runs = 3;
  Algorithm algo = Sarsa;
  ActionSelector act_sel = EGreedy;
  Experiment exp;

  exp = makeExperiment(n_episodes, steps, runs, pp, algo, act_sel);
  timeExpr(repeatExp(&exp););
  freeExperiment(exp);

  return 0;
}
