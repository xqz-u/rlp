#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <zmq.h>

#include "../include/zhelpers.h"

void testController(void) {
  void *context = zmq_ctx_new();

  //  Socket to talk to server (works in step-lock)
  void *requester = zmq_socket(context, ZMQ_REQ);
  zmq_connect(requester, "tcp://localhost:5555");

  char *msg, *reply, *commands[] = {"U", "D", "L", "R"};
  int command_sz = sizeof(commands) / sizeof(commands[0]);

  char *state, *dead, *rwd;
  // get initial percept
  s_send(requester, "S");
  reply = s_recv(requester);
  state = strtok(reply, ",");
  dead = strtok(NULL, ",");
  rwd = strtok(NULL, ",");
  printf("%d: Initial state: [%s,%s,%s]\n", 0, state, dead, rwd);
  free(reply);

  int i = 1;
  while (1) {
    asprintf(&msg, "%s", commands[rand() % (command_sz - 1)]);
    s_send(requester, msg);
    free(msg);
    reply = s_recv(requester);
    state = strtok(reply, ",");
    dead = strtok(NULL, ",");
    rwd = strtok(NULL, ",");
    printf("%d: Received reply [%s,%s,%s]\n", i, state, dead, rwd);
    if (!strcmp(dead, "1")) {
      printf("agent is dead!\n");
      free(reply);
      break;
    }
    free(reply);
    i += 1;
  }

  zmq_close(requester);
  zmq_ctx_destroy(context);
}

void testStrTok(void) {
  char *example;
  asprintf(&example, "ciao,come,stai");
  char *tok;
  tok = strtok(example, ",");
  printf("%s\n", tok);
  tok = strtok(NULL, ",");
  printf("%s\n", tok);
  tok = strtok(NULL, ",");
  printf("%s\n", tok);
  tok = strtok(NULL, ",");
  printf("%p\n", tok);
  free(example);
}

int main(void) {
  srand(time(NULL));
  /* testStrTok(); */
  testController();
  return 0;
}
