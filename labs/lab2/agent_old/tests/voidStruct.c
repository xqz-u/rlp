#include <stdio.h>
#include <stdlib.h>

typedef struct Contain {
  int cnt;
  void *store;
} Contain;

typedef int (*member)(Contain);

typedef struct Child {
  char *literal;
  double n;
  member memb_fn;
} Child;

int example_memb(Contain c) {
  printf("%d\n", c.cnt);
  printf("%p\n", c.store);
  return c.cnt + 1;
}

int inFn(Child *c, Contain cc) {
  printf("%s %.2lf\n", c->literal, c->n);
  return c->memb_fn(cc);
}

Child makeChild(char *s, double n) { return (Child){s, n, example_memb}; }

void testStr(void) {
  Child f = makeChild("mimmo", random());
  Contain x = (Contain){.cnt = 10, .store = &f};
  printf("%d\n", ((Child *)x.store)->memb_fn(x));
  printf("%d\n", inFn(x.store, x));
}

int main(void) {
  int b = 6;
  int *a = &b;
  void *pa = a;
  printf("%d\n", *(int *)pa);
  printf("here\n");
  testStr();
  return 0;
}
