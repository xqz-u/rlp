#include <stdio.h>

#include "../include/judyUtils.h"

#define set(_, b) b
/* #define  */
double add(double a, double b) { return a + b; }
double sub(double a, double b) { return a - b; }

/* void withJLAD(PPvoid_t parr, Word_t idx, double val, */
/*               double (*fn)(double x, double y)) { */
/*   double *pval; */
/*   JLI(pval, *parr, idx); */
/*   *pval = fn(*pval, val); */
/* } */

#define defSetterJLA(TYPE)                                                     \
  void withJLA(PPvoid_t parr, Word_t idx, TYPE val,                            \
               double (*fn)(double x, double y)) {                             \
    TYPE *pval;                                                                \
    JLI(pval, *parr, idx);                                                     \
    *pval = fn(*pval, val);                                                    \
  }

#define setJLAD(pparr, idx, val) withJLA(pparr, idx, double, val, set);
#define addJLAD(pparr, idx, val) withJLA(pparr, idx, double, val, add);

/* defSetterJLA(double) */

void testUpdateJA(void) {
  Pvoid_t parr = (Pvoid_t)NULL;

  setJLAD(&parr, 10, 10.4);
  setJLAD(&parr, 120, 11.4);

  printJLAD(parr, "");

  addJLAD(&parr, 10, 6);
  withJLAD(&parr, 11, 4, set);

  printJLAD(parr, "");

  freeJLA(parr);
}

void testUpdate(void) {
  Pvoid_t pmatrix = (Pvoid_t)NULL;

  setJLMD(&pmatrix, 1, 1, 2.6);
  setJLMD(&pmatrix, 1, 2, 3.6);
  setJLMD(&pmatrix, 4, 4, 6);
  setJLMD(&pmatrix, 20, 150, 1837.7);

  printJLMD(pmatrix);

  printf("%lf\n", *getJLMD(pmatrix, 1, 1));
  printf("%sEXISTS\n", getJLMD(pmatrix, 1, 3) == NULL ? "NOT " : "");

  freeJLM(pmatrix);
}

int main(void) {
  testUpdateJA();
  /* testUpdate(); */
  return 0;
}
