#include "../include/experiment.h"
#include "../include/action_selectors.h"
#include "../include/learners.h"
#include "../include/utils.h"

#define N_ALGO 2
#define N_ACT_SEL 4

act_select act_selectors[N_ACT_SEL] = {uniform_sel, egreedy_sel, softmax_sel,
                                       ucb_sel};
unit_step learners[N_ALGO] = {sarsaAct, randomAct};
init_ep handshakes[N_ALGO] = {sarsaSetup, basicSetup};

Experiment makeExperiment(int episodes, int steps, int runs, Params *params,
                          Algorithm algo, ActionSelector actSelect) {
  return (Experiment){.episodes = episodes,
                      .steps = steps,
                      .runs = runs,
                      .agent = initAgent(act_selectors[actSelect],
                                         learners[algo], handshakes[algo]),
                      .params = params,
                      .stats = (Pvoid_t)NULL,
                      .algo = algo,
                      .actSelect = actSelect};
}

void freeExperiment(Experiment exp) {
  freeAgent(exp.agent);
  free(exp.agent);
  freeStats(exp.stats);
  freeParams(exp.params);
}

void refreshExperiment(Experiment *exp) {
  exp->params->seed += 1;
  changeSeed(exp->params, exp->params->seed);
  refreshAgent(exp->agent);
}

Performance runEpisode(int episode, Experiment *exp) {
  Agent *ag = exp->agent;
  double episodeRwd = 0;
  Percept perc;
  int i;
  for (i = 1; i < exp->steps; ++i) {
    /* printf("time: %d\n", ag->time); */
    perc = ag->act(ag, exp->params);
    /* printf("%d:percept: ", i); */
    /* printPercept(perc); */
    ag->state = perc.state;
    /* printf("update state: %ld\n", ag->state); */
    episodeRwd += perc.reward;
    ag->time += 1;
    if (perc.dead)
      break;
    /* printf(".................\n"); */
  }
  /* printf("death: ep %d t_step: %d, type: %s\n", episode, i, */
  /*        perc.dead == 1 ? "killed" */
  /*                       : (perc.dead == 2 ? "starve" : "max epochs")); */
  Performance p = (Performance){
      .ep_rwd = episodeRwd, .food_left_prop = perc.food_left, .steps = i};
  /* printf("%.2lf %.3lf %d\n", p.ep_rwd, p.food_left_prop, p.steps); */
  return p;
}

void runAlgo(int run, Experiment *exp) {
  int i;
  for (i = 0; i < exp->episodes; ++i) {
    /* printf("first percept:\n"); */
    exp->agent->start_episode(exp->agent, exp->params);
    /* printf("first perc\n"); */
    /* printf(".................\n"); */
    saveStats(&exp->stats, run, i, runEpisode(i, exp));
    /* refresh environment for new episode */
    /* printf("-------------------------\n"); */
  }
}

void repeatExp(Experiment *exp) {
  int i;
  openEnv(exp->agent, exp->params->port);
  for (i = 0; i < exp->runs; ++i) {
    runAlgo(i, exp);
    refreshExperiment(exp);
    /* printf("Run %d\n", i); */
    /* printf("===============================\n"); */
  }
  killEnv(exp->agent->channel);
  dumpStats(*exp);
}

void dumpStats(Experiment exp) {
  FILE *fp;

  fp = freopen(exp.params->stats_file, "w", stdout);
  if (fp == NULL) {
    perror("failed to open stats file:");
    exit(1);
  }

  /* write csv header */
  printf(
      "ep,learner,act_sel,alpha,gamma,eps,tau,c,env_t,rwd,food_left,steps\n");

  int i, j;
  Performance *pperf;

  for (i = 0; i < exp.runs; ++i) {
    for (j = 0; j < exp.episodes; ++j) {
      pperf = getStats(exp.stats, i, j);
      printf("%d,%d,%d,%.3lf,%.3lf,%.3lf,%.3lf,%.3lf,%s,%.3lf,%.3lf,%d\n", j,
             exp.algo, exp.actSelect, exp.params->alpha, exp.params->gamma,
             exp.params->eps, exp.params->tau, exp.params->c, exp.params->env_t,
             pperf->ep_rwd, pperf->food_left_prop, pperf->steps);
    }
  }

  fclose(fp);
}
