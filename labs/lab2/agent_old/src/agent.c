#include "../include/agent.h"
#include "../include/ZEnv.h"
#include "../include/utils.h"

Agent *initAgent(act_select act_select_pol, unit_step learn_step_pol,
                 init_ep ep_setupper) {
  Agent *ag = safeMalloc(sizeof(*ag));
  *ag = (Agent){.qtable = (Pvoid_t)NULL,
                .act_selector = act_select_pol,
                .time = 0,
                .N = safeMalloc(Start * sizeof(int)),
                .act = learn_step_pol,
                .start_episode = ep_setupper};
  return ag;
}

void refreshAgent(Agent *ag) {
  freeAgent(ag);
  ag->qtable = (Pvoid_t)NULL;
  ag->N = safeMalloc(Start * sizeof(*ag->N));
  ag->time = 0;
}

void openEnv(Agent *ag, char *port) { ag->channel = makeZenv(port); }

void freeAgent(Agent *ag) {
  freeJLM(ag->qtable);
  free(ag->N);
}

Percept basicSetup(Agent *ag, Params *p) {
  Percept birth = envPercept(Start, ag->channel);
  ag->state = birth.state;
  return birth;
}
