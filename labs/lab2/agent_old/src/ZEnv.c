#include <stdlib.h>

#include "../include/ZEnv.h"
#include "../include/utils.h"
#include "../include/zhelpers.h"

Zenv makeZenv(char *port) {
  /* dinamycally set port */
  char *endpoint = to_string("tcp://localhost:%s", port);
  /* create context and steplock request-reply socket (here request) */
  void *context = zmq_ctx_new();
  void *requester = zmq_socket(context, ZMQ_REQ);
  /* connect to the replier on the given port */
  zmq_connect(requester, endpoint);
  /* printf("[connection to env on port %s started]\n", port); */
  /* pack into struct */
  return (Zenv){.ctx = context, .sock = requester, .endpoint = endpoint};
}

void destroyZenv(Zenv channel)
{
  /* cleanup zmq */
  zmq_close(channel.sock);
  zmq_ctx_destroy(channel.ctx);
  /* printf("[Connection with env %s closed]\n", channel.endpoint); */
  /* free the memory allocated when the endpoint was created */
  free(channel.endpoint);
}

Percept envPercept(Action act, Zenv channel) {
  /* send action */
  char *str_act = to_string("%d", act);
  s_send(channel.sock, str_act);
  free(str_act);
  /* receive percept */
  char *reply = s_recv(channel.sock);
  /* printf("env reply: %s\n", reply); */
  /* parse percept, structure: "state,dead,reward" */
  Percept ret = (Percept){.state = atol(strtok(reply, ",")),
                          .dead = atoi(strtok(NULL, ",")),
                          .reward = atof(strtok(NULL, ",")),
                          .food_left = atof(strtok(NULL, ","))};
  free(reply);
  return ret;
}

void send_msg(Zenv channel, Action act) {
  char *str_act = to_string("%d", act);
  s_send(channel.sock, str_act);
  free(str_act);
}

void killEnv(Zenv channel) {
  send_msg(channel, End);
  destroyZenv(channel);
}
void printPercept(Percept pt) {
  printf("Percept: %ld %d %.3lf\n", pt.state, pt.dead, pt.reward);
}
