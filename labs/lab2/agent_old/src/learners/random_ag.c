#include "../../include/learners.h"

Percept randomAct(Agent *ag, Params *params) {
  return envPercept(ag->act_selector(ag, params, -1), ag->channel);
}
