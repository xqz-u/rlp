#include "../../include/learners.h"

Percept sarsaAct(Agent *ag, Params *params) {
  Percept perc = envPercept(ag->action, ag->channel);
  Action a_n = ag->act_selector(ag, params, perc.state);
  PPvoid_t qtable = &(ag->qtable);
  double q_sa = *safe_getJLM_double(qtable, ag->state, ag->action);
  /* printf("sarsa action value state %ld: %.3lf\n", ag->state, q_sa); */
  double q_sa_n = *safe_getJLM_double(qtable, perc.state, a_n);
  /* printf("sarsa action value next state %ld: %.3lf\n", perc.state, q_sa_n);
   */
  addJLMD(qtable, ag->state, ag->action,
          params->alpha * (perc.reward + params->gamma * q_sa_n - q_sa));
  /* printf("update sarsa action value: %.3lf\n", */
  /* *safe_getJLM_double(qtable, ag->state, ag->action)); */
  ag->action = a_n;
  /* printf("update sarsa action: %d\n", (Action)ag->action); */
  return perc;
}

Percept sarsaSetup(Agent *ag, Params *params) {
  Percept birth = basicSetup(ag, params);
  ag->action = ag->act_selector(ag, params, birth.state);
  return birth;
}
