#include "../../include/action_selectors.h"

Action ucb_sel(Agent *ag, Params *params, long state) {
  vector_double q_vals = make_vector_double(Start);
  int i, act;

  for (i = 0; i < Start; ++i) {
    if (!ag->N[i]) { // when action has not been tried yet, choose it
      free(q_vals.store);
      ag->N[i] += 1;
      return i;
    }
    q_vals.store[i] = *safe_getJLM_double(&ag->qtable, state, i) +
                      params->c * sqrt(log(ag->time) / ag->N[i]);
  }

  act = argmax_double(q_vals, params->r);
  ag->N[act] += 1;

  free(q_vals.store);
  return (Action)act;
}
