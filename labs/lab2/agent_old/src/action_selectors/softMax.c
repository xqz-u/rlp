#include "../../include/action_selectors.h"

Action softmax_sel(Agent *ag, Params *params, long state) {
  int i;
  long double thr, sum = 0, cum_prob = 0;

  vector_double q_vals =
      collectJLA_double_range(safe_getJLM_row(&ag->qtable, state), 0, Start);

  for (i = 0; i < Start; ++i)
    sum += expl((long double)q_vals.store[i] / params->tau);

  thr = gsl_rng_uniform(params->r);
  for (i = 0; i != Start; ++i) {
    cum_prob += expl((long double)q_vals.store[i] / params->tau) / sum;
    if (cum_prob >= thr)
      break;
  }

  free(q_vals.store);
  return (Action)i;
}
