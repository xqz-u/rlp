#include "../../include/action_selectors.h"

Action egreedy_sel(Agent *ag, Params *params, long state) {
  return gsl_rng_uniform(params->r) < params->eps
             ? gsl_rng_uniform_int(params->r, Start)
             : argmaxJLM_row_double_range_bt(&ag->qtable, state, 0, Start,
                                             params->r);
}
