#include "../../include/action_selectors.h"

Action uniform_sel(Agent *ag, Params *params, long state) {
  return gsl_rng_uniform_int(params->r, Start);
}
