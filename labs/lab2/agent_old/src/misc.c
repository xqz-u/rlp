#include "../include/misc.h"

void changeSeed(Params *p, int seed) {
  gsl_rng_free(p->r);
  p->r = gsl_rng_alloc(gsl_rng_default);
  gsl_rng_set(p->r, seed);
}

/* TODO modify judyMacros so that they can store structs too and can just save
   whole Performance */
/* FIXME danger!!! if entry at idx is overwritten that malloc is lost! */
void saveStats(PPvoid_t parr, int run, int ep, Performance perf) {
  Performance *pperf = safeMalloc(sizeof(*pperf));
  *pperf = perf;
  /* get/create the pointer to current run (matrix row) */
  PPvoid_t run_row = safe_getJLM_row(parr, run);
  /* Performance at ep just needs to be set and not updated, so do not
     JLG before JLI */
  PPvoid_t ppperf; // need to store as ** pointer
  JLI(ppperf, *run_row, ep);
  *ppperf = pperf;
}

Performance *getStats(Pvoid_t arr, int run, int ep) {
  PPvoid_t prow;
  Performance **ppperf;
  JLG(prow, arr, run);
  JLG(ppperf, *prow, ep);
  return *ppperf;
}

void freeStats(Pvoid_t arr) {
  PPvoid_t prow;
  Performance **ppperf;
  Word_t i = 0, j = 0; // j ur a bitch

  JLF(prow, arr, i);
  while (prow != NULL) {
    JLF(ppperf, *prow, j);
    while (ppperf != NULL) {
      free(*ppperf);
      JLN(ppperf, *prow, j);
    }
    j = 0;
    JLN(prow, arr, i);
  }

  freeJLM(arr);
}

Params *parseParams(char *port, char *fout, char *env_t, double alpha,
                    double gamma, double eps, double tau, double c) {
  gsl_rng *r = gsl_rng_alloc(gsl_rng_default);
  gsl_rng_set(r, 0);
  Params *pp = safeMalloc(sizeof(*pp));
  *pp = (Params){.port = port,
                 .stats_file = fout,
                 .env_t = env_t,
                 .seed = 0,
                 .r = r,
                 .alpha = alpha,
                 .gamma = gamma,
                 .eps = eps,
                 .tau = tau,
                 .c = c};
  return pp;
}

void freeParams(Params *pp) {
  gsl_rng_free(pp->r);
  free(pp);
}
