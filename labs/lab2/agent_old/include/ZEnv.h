#ifndef ZENV_H
#define ZENV_H

#include <zmq.h>

#include "misc.h"

typedef struct Percept {
  long state;
  int dead;
  double reward, food_left;
} Percept;

typedef struct Zenv {
  void *ctx, *sock;
  char *endpoint;
} Zenv;

Zenv makeZenv(char *port);
void destroyZenv(Zenv channel);
Percept envPercept(Action act, Zenv channel);
void send_msg(Zenv channel, Action act);
void killEnv(Zenv channel);
void printPercept(Percept pt);

#endif
