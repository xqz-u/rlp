#ifndef LIBVECTOR_H
#define LIBVECTOR_H

#include <gsl/gsl_rng.h>

#include "utils.h"

#define defVector(TYPE, FMT)                                                   \
  typedef struct {                                                             \
    TYPE *store;                                                               \
    int size;                                                                  \
  } vector_##TYPE, *vector_p_##TYPE;                                           \
                                                                               \
  static inline vector_##TYPE make_vector_##TYPE(int size) {                   \
    return (vector_##TYPE){.size = size,                                       \
                           .store = safeMalloc(size * sizeof(TYPE))};          \
  }                                                                            \
                                                                               \
  static inline vector_p_##TYPE make_vector_p_##TYPE(int size) {               \
    vector_p_##TYPE vp = safeMalloc(sizeof(*vp));                              \
    *vp = make_vector_##TYPE(size);                                            \
    return vp;                                                                 \
  }                                                                            \
                                                                               \
  static inline void free_vector_p_##TYPE(vector_p_##TYPE vp) {                \
    free(vp->store);                                                           \
    free(vp);                                                                  \
  }                                                                            \
                                                                               \
  static inline void vector_##TYPE##_resize(vector_p_##TYPE vp, int new_sz) {  \
    vp->size = new_sz;                                                         \
    resize((void **)&(vp->store), vp->size, sizeof(*vp->store));               \
  }                                                                            \
                                                                               \
  static inline void vector_##TYPE##_checkresize(vector_p_##TYPE vp,           \
                                                 int n_filled) {               \
    if (n_filled >= vp->size)                                                  \
      vector_##TYPE##_resize(vp, vp->size * 2);                                \
  }                                                                            \
                                                                               \
  static inline void vector_##TYPE##_reset(vector_##TYPE v, int until,         \
                                           TYPE el) {                          \
    int i;                                                                     \
    for (i = 0; i < until; ++i)                                                \
      v.store[i] = el;                                                         \
  }                                                                            \
                                                                               \
  static inline void vector_##TYPE##_print(vector_##TYPE v, int printlen) {    \
    if (!v.store) {                                                            \
      printf("vector (" #TYPE ", %d) is nil\n", v.size);                       \
      return;                                                                  \
    }                                                                          \
    printf("%d:[" FMT, v.size, *v.store);                                      \
    int i;                                                                     \
    for (i = 1; i < printlen; ++i)                                             \
      printf("," FMT, v.store[i]);                                             \
    printf("]\n");                                                             \
  }

/* NOTE idea: save indices of greatest entries (multiple entries can have */
/* same maximum value) while scanning through the vector, reset all the saved */
/* indices and continue if an index better than the current maximum is found */
#define defArgmaxVec(TYPE, CMP_FN, EQ_FN)                                      \
  static inline vector_int vector_##TYPE##_argmax(vector_##TYPE v) {           \
    /* printf("comp argmax of: "); \ */                                        \
    /* vector_##TYPE##_print(v, v.size); \ */                                  \
    int i, maxIdx, strike;                                                     \
    maxIdx = strike = 0;                                                       \
                                                                               \
    vector_int best = make_vector_int(v.size);                                 \
    /* -1 is arbitrary sentinel */                                             \
    vector_int_reset(best, best.size, -1);                                     \
                                                                               \
    for (i = 0; i < v.size; ++i) {                                             \
      if (CMP_FN(v.store[i], v.store[maxIdx])) {                               \
        maxIdx = i;                                                            \
        vector_int_reset(best, strike, -1);                                    \
        best.store[0] = maxIdx;                                                \
        strike = 1;                                                            \
      } else if (EQ_FN(v.store[i], v.store[maxIdx])) {                         \
        best.store[strike++] = i;                                              \
      }                                                                        \
    }                                                                          \
                                                                               \
    vector_int_resize(&best, strike);                                          \
    /* vector_int_print(best, best.size); \ */                                                                          \
    return best;                                                               \
  }                                                                            \
  static inline int argmax_##TYPE(vector_##TYPE v, gsl_rng *r) {               \
    vector_int argmax = vector_##TYPE##_argmax(v);                             \
    int break_tie = argmax.store[gsl_rng_uniform_int(r, argmax.size)];         \
    free(argmax.store);                                                        \
    return break_tie;                                                          \
  }

/* NOTE move to separate definitions file if definitions increase */
defVector(double, "%.3lf") defVector(int, "%d")
    defArgmaxVec(double, maxD, compareDouble)

#endif
