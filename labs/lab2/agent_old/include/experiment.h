#ifndef EXPERIMENT_H
#define EXPERIMENT_H

#include "agent.h"

typedef struct Experiment {
  int episodes, steps, runs;
  Agent *agent;
  Params *params;
  Pvoid_t stats;
  Algorithm algo;
  ActionSelector actSelect;
} Experiment;

Experiment makeExperiment(int episodes, int steps, int runs, Params *params,
                          Algorithm algo, ActionSelector actSelect);
void freeExperiment(Experiment exp);
void refreshExperiment(Experiment *exp);
Performance runEpisode(int episode, Experiment *exp);
void runAlgo(int run, Experiment *exp);
void repeatExp(Experiment *exp);
void dumpStats(Experiment exp);

#endif
