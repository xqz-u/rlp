#ifndef LEARNERS_H
#define LEARNERS_H

#include "agent.h"

Percept randomAct(Agent *ag, Params *params);

Percept sarsaAct(Agent *ag, Params *params);
Percept sarsaSetup(Agent *ag, Params *params);

#endif
