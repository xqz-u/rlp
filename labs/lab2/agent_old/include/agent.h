#ifndef AGENT_H
#define AGENT_H

#include "ZEnv.h"
#include "judyDefs.h"
#include "misc.h"

typedef struct Agent Agent;

typedef Action (*act_select)(Agent *agent, Params *params, long state);
typedef Percept (*unit_step)(Agent *agent, Params *params);
typedef Percept (*init_ep)(Agent *agent, Params *params);

struct Agent {
  Zenv channel;
  long state;
  int time, *N;
  Action action;
  Pvoid_t qtable;
  act_select act_selector;
  unit_step act;
  init_ep start_episode;
};

Agent *initAgent(act_select act_select_pol, unit_step learn_step_pol,
                 init_ep ep_setupper);
void refreshAgent(Agent *ag);
void openEnv(Agent *ag, char *port);
Percept basicSetup(Agent *ag, Params *p);
void freeAgent(Agent *ag);

#endif
