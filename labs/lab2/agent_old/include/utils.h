#ifndef LIBUTILS_H
#define LIBUTILS_H

#include <assert.h>
#include <float.h>
#include <math.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define allowSemiCol(Expr)                                                     \
  do {                                                                         \
    Expr                                                                       \
  } while (0)

#define timeExpr(Expr)                                                         \
  allowSemiCol({                                                               \
    clock_t begin = clock();                                                   \
    /* here, do your time-consuming job */                                     \
    Expr clock_t end = clock();                                                \
    printf("Timed expression: \'" #Expr "\'\n");                               \
    double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;                \
    printf("t: %lf\n", time_spent);                                            \
  })

static inline int maxD(double a, double b) { return a > b; }

/* NOTE makes sense only for small double values; as they get larger, the
 * difference  does too, and epsilon is not a good threshold anymore */
static inline int compareDouble(double a, double b) {
  return fabs(a - b) < DBL_EPSILON;
}

static inline void *safeMalloc(size_t size) {
  void *store = calloc(size, 1);
  assert(store);
  return store;
}

/* NOTE size should be sizeof(memb) where memb is a member of the array */
static inline void resize(void **store, int new_sz, size_t memb_sz) {
  *store = realloc(*store, new_sz * memb_sz);
  assert(*store);
}

/* from man pages NOTE free return string */
static inline char *to_string(const char *fmt, ...) {
  int n = 0;
  size_t size = 0;
  char *p = NULL;
  va_list ap;

  /* Determine required size */
  va_start(ap, fmt);
  n = vsnprintf(p, size, fmt, ap);
  va_end(ap);

  if (n < 0)
    return NULL;

  /* One extra byte for '\0' */
  size = (size_t)n + 1;
  p = malloc(size);
  if (p == NULL)
    return NULL;

  va_start(ap, fmt);
  n = vsnprintf(p, size, fmt, ap);
  va_end(ap);

  if (n < 0) {
    free(p);
    return NULL;
  }

  return p;
}

#endif
