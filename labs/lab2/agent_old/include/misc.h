#ifndef MISC_H
#define MISC_H

#include <gsl/gsl_rng.h>

#include "judyDefs.h"
#include "utils.h"

typedef struct Params {
  char *port, *stats_file, *env_t;
  gsl_rng *r;
  int seed;
  double eps, alpha, gamma, tau, c;
} Params;

typedef struct Performance {
  double ep_rwd, food_left_prop;
  int steps;
} Performance;

typedef enum { Up, Down, Left, Right, Start, End } Action;

typedef enum { Sarsa, Random } Algorithm;

typedef enum { Uniform, EGreedy, Softmax, UCB } ActionSelector;

void changeSeed(Params *p, int seed);
void saveStats(PPvoid_t parr, int run, int ep, Performance perf);
Performance *getStats(Pvoid_t arr, int run, int ep);
void freeStats(Pvoid_t arr);
Params *parseParams(char *port, char *fout, char *env_t, double alpha,
                    double gamma, double eps, double tau, double c);
void freeParams(Params *pp);

#endif
