#ifndef ACTION_SEL_H
#define ACTION_SEL_H

#include <math.h>

#include "agent.h"

Action egreedy_sel(Agent *ag, Params *params, long state);
Action softmax_sel(Agent *ag, Params *params, long state);
Action uniform_sel(Agent *ag, Params *params, long state);
Action ucb_sel(Agent *ag, Params *params, long state);

#endif
