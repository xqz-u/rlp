#ifndef JUDYDEFS_H
#define JUDYDEFS_H

#include "judyMacros.h"
#include "utils.h"

defSetter(double) defAdder(double)

    /* ----------------- Array methods --------------- */
    existChecker(double)

        JLAType(double) defSetterJLA(double, setJLAD)
            defAdderJLA(double, addJLAD) defGetterJLA(double, Pvoid_t)
                defPrinterJLA(double, printJLAD, "%lf") defCollectorJLA(double)

    /* ----------------- Matrix methods --------------- */
    existChecker(Pvoid_t)

    /* general matrix row getter returning PPvoid_t (**void) */
    defGetterJLA(Pvoid_t, PPvoid_t)

        JLMType(double) defSetterJLM(double, setJLMD)
            defAdderJLM(double, addJLMD) defGetterJLM(double)
                defPrinterJLM(printJLMD, printJLAD)

    /* ------------------- Useful function defs -------------------- */

    defArgMaxJLA(double, maxD) defArgMaxJLM(double, maxD)

#endif
