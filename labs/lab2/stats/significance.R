library(tidyverse)
library(zeallot)
library(parallel)

source("./shiny/processing/prodata.r")
source("./shiny/processing/filter.r")
source("./shiny/processing/utils.r")

envs <- c("env_4",
          "env_4_v3",
          "env_8_v3",
          "env_8_v3_fl",
          "env_8_v3_h3_fl",
          "env_8_v3_fl_rw")

preproc_run <- function(df, llag=FALSE) {
  df %>%
    filter(env_t %in% envs) %>%
    mutate(env_sz = case_when(
             grepl("env_4", env_t) ~ 4,
             grepl("env_8", env_t) ~ 8),
           v_sz = case_when(
             grepl("v3", env_t) ~ 3,
             TRUE ~ 2),
           across(c(env_sz, v_sz), as.factor)) -> ret
  if (llag) return(ret %>% filter.llag())
  ret
}

filter_fitted <- function(df, config) {
  c(learner_, alpha_, gamma_, param_) %<-% config
  df %>%
    filter(act_sel == "Softmax" &
           learner == !!learner_ &
           alpha == !!alpha_ &
           gamma == !!gamma_ &
           param == !!param_)
}

fitted_learners <- function(run_data, fitted_confs) {
  fitted_confs %>%
    map_dfr(~ filter_fitted(run_data, .))
}

## ideally:
## Sarsa & 0.15 & 0.4(4x4) 0.45(8x8) & 0.001\\
## Q-Learning & 0.15 & 0.45(4x4) 0.4(8x8) & 0.001\\
## DQ-Learning & 0.1 & 0.45(4x4) 0.4(8x8) & 0.01(4x4) 0.0055(8x8)\
## adapted:
f_sarsa <- c("Sarsa", 0.15, 0.45, 0.001)
f_ql <- c("Q-Learning", 0.15, 0.4, 0.001)
f_dql <- c("DQ-Learning", 0.1, 0.4, 0.0055)

last_run <- read_run("./data/last_run.csv")
proc_run <- last_run %>% preproc_run()
fitted_run <- proc_run %>% fitted_learners(list(f_sarsa, f_ql, f_dql))

############### claims ##############
bin_env_comp <- function(df, comp_envs, show=FALSE) {
  comps <- df %>% filter.env(comp_envs)
  if (show) print(comps)
  t.test(formula = win.p ~ env_t, data = comps)
}

comp_v <- c("env_4", "env_4_v3")
comp_f <- c("env_8_v3", "env_8_v3_fl")
comp_r <- c("env_8_v3_fl", "env_8_v3_fl_rw")
comp_h <- c("env_8_v3_fl", "env_8_v3_h3_fl")

## "Vision size v = 3 delivers the best results across algorithms and
## exploration strategies"
fitted_run %>% bin_env_comp(comp_v)

## "With food growing in lumps patterns, the agent achieves significantly higher
## performance than with food growing randomly."
## env 8 v3 / env 8 v3 fl
fitted_run %>% bin_env_comp(comp_f)

## "Enforcing punishment for bouncing against the grid borders does not
## significantly impact overall algorithms performance"
## env 8 v3 fl / env 8 v3 fl rw
fitted_run %>% bin_env_comp(comp_r)

## "Increasing the number of hunters from 2 to 3 has a significant impact on
## performance of all learners."
## env 8 v3 fl / env 8 v3 fl h3
fitted_run %>% bin_env_comp(comp_h)


## unspecified (not really sure of these ones):
## Overall best performance in env 8 v3 fl is reached with Q-Learning
x <- fitted_run %>%
  filter.env("env_8_v3_fl") %>%
  aov(win.p ~ learner, data = .)
summary(x)
xx <- TukeyHSD(x)
xx ## Q-Learning not significantly better than SARSA
plot(xx)

## When food grows randomly, all algorithms show high variance in their
## score metrics.
## ??

## When comparing reward systems, Q-Learning is the best performing algorithm
z <- aov(win.p ~ learner * env_t, data = fitted_run %>% filter.env(comp_r))
library(MASS)
w <- stepAIC(z)
summary(w)
ww <- TukeyHSD(w)
ww ## Q-Learning not significantly better than SARSA
plot(ww)
