make.plot <- function(df, metric) {
  eval(substitute(ggplot(df, aes(x = lag, y = metric)) +
    geom_point() +
    geom_smooth()))
}

plot.win <- function(df) make.plot(df, win.p)
plot.rwd <- function(df) make.plot(df, r_mean)
plot.step <- function(df) make.plot(df, steps_mean)

make.tile.plot <- function(df, xaxis, yaxis, formula, viridis = TRUE,
                           fixed = TRUE) {
  scale <-
    if (fixed) {
      scale_fill_viridis_c(limits = c(0, 1), option = "plasma")
    } else {
      scale_fill_viridis_c(option = "plasma")
    }
  if (viridis) {
    substitute(ggplot(df, aes(x = as.factor(eval(xaxis)), y = as.factor(eval(yaxis)), fill = win.p)) +
      scale +
      geom_tile() +
      labs(
        x = paste0(xaxis),
        y = paste0(yaxis)) +
      facet_grid(formula) +
      theme_classic() +
      theme(legend.position = "top"))
  } else {
    substitute(ggplot(df, aes(x = as.factor(eval(xaxis)), y = as.factor(eval(yaxis)), fill = win.p)) +
      geom_tile() +
      labs(
        x = paste0(xaxis),
        y = paste0(yaxis)) +
      facet_grid(formula) +
      theme_classic() +
      theme(legend.position = "top"))
  }
}

make.best.plot <- function(df, color, metric, formula) {
  substitute(ggplot(df, aes(x = lag, y = eval(metric), color = eval(color))) +
    geom_point() +
    geom_smooth() +
    facet_grid(formula) +
    labs(
      color = paste0(color),
      y = paste0(metric)) +
    theme(legend.position = "top"))
}
