## module_backend(
##   name = selectorR,
##   args = alist(
##     choices = ,
##     selected = NULL),
##   body = {
##     observe(
##       updateSelectizeInput(session, ID(selector),
##         selected = if (is.null(selected)) first(choices()) else selected,
##         choices = choices(),
##         server = TRUE))
##     reactive(get_in(selector))})

## module_backend(
##   name = selectorS,
##   args = alist(
##     choices = ,
##     selected = NULL),
##   body = {
##     reactive(get_in(selector))})

make.selector <- function(session, id, choices, selected = NULL) {
  updateSelectizeInput(session, id,
    selected = selected,
    choices = choices,
    server = TRUE)
}

make.slider_range <- function(id, label, min = 0, max = 1) {
  renderUI({
    req(min)
    req(max)
    sliderInput(id, label,
      min = min, max = max,
      value = c(min, max))})
}


server <- function(input, output, session)
{

  make.selector(session, "env", prepend("ALL", levels(data$env_t)), "ALL")
  make.selector(session, "algo", prepend("ALL", levels(data$learner)), "ALL")
  make.selector(
    session, "policy",
    prepend("ALL", levels(data$act_sel)), "ALL")
  make.selector(session, "xaxis", names(data), "param")
  make.selector(session, "yaxis", names(data), "env_t")
  make.selector(session, "col", names(data), "gamma")
  make.selector(session, "row", names(data), "alpha")
  make.selector(session, "col_best", names(data), "env_t")
  make.selector(session, "row_best", names(data), "learner")
  make.selector(session, "color", names(data), "act_sel")
  make.selector(session, "metric", names(data), "win.p")

  bests <- reactive({
    req(input$env)
    req(input$algo)
    req(input$policy)
    filter.id(data, filter.best_id(data)) %>%
      filter.env(input$env) %>%
      filter.learner(input$algo) %>%
      filter.sel(input$policy)

  })

  last.lag <- reactive({
    req(input$env)
    req(input$algo)
    req(input$policy)
    data %>%
      filter.llag() %>%
      filter.env(input$env) %>%
      filter.learner(input$algo) %>%
      filter.sel(input$policy)
  })

    output$slider_param <-
        renderUI({
            req(last.lag())
            sliderInput("param", "Policy Parameter:",
                        min = min(last.lag()$param), max = max(last.lag()$param),
                        value = c(min(last.lag()$param), max(last.lag()$param)))})


  output$slider_alpha <- make.slider_range(
    "alpha", "Alpha:",
    min(last.lag()$alpha), max(last.lag()$alpha))

  output$slider_gamma <- make.slider_range(
    "gamma", "Gamma:",
    min(last.lag()$gamma), max(last.lag()$gamma))

  last.lag.filter <- reactive({
    req(last.lag())
    req(input$alpha)
    req(input$gamma)
    req(input$param)
    last.lag() %>%
      filter.alpha(input$alpha) %>%
      filter.gamma(input$gamma) %>%
      filter.param(input$param)})

  output$llag_graph <- renderPlot({
    req(last.lag.filter())
    scale <- if (input$scale_color) TRUE else FALSE
    fixed <- if (input$scale_fixed) TRUE else FALSE

    eval(make.tile.plot(
      last.lag.filter(),
      sym(input$xaxis),
      sym(input$yaxis),
      eval(sym(input$row)) ~ eval(sym(input$col)),
      scale, fixed))})

  output$best_graph <- renderPlot({
    req(bests())
    eval(make.best.plot(
      bests(),
      sym(input$color),
      sym(input$metric),
      eval(sym(input$row_best)) ~ eval(sym(input$col_best))))})

    output$best_table <- renderTable({
        req(bests())
        bests() %>%
            filter.llag()
    })


  ## data <- pre_proc()

  ## updateSelectizeInput(session, "stations",
  ##   choices = levels(factor(data$name)),
  ##   server = TRUE)

  ## filt_data <- reactive(
  ##   if (is.null(input$stations)
  ##       | length(input$stations) == 0) data
  ##   else data [data$name %in% input$stations, ])

  ## delay_data <- reactive({
  ##   req(input$delay_range)
  ##   if (is.null(input$delay_range)) filt_data()
  ##   else filter(filt_data(), delay > input$delay_range [1]
  ##                           & delay < input$delay_range [2])}) %>%
  ## debounce(500)

  ## cause_data <- reactive({
  ##   req(input$cause)
  ##   tmp <- delay_data()
  ##   if (input$cause == "ALL" | input$cause == "") tmp
  ##   else tmp [tmp$group == input$cause, ]})


  ## xaxis <- reactive({
  ##   req(input$cause)
  ##   if (input$cause == "ALL") "Cause group"
  ##   else "Cause"})


  ## updateSelectizeInput(
  ##   session, "cause",
  ##   choices = prepend("ALL", levels(factor(data$group))),
  ##   selected = "ALL", server = TRUE)

  ## xaxis.col <- reactive({
  ##   req(input$cause)
  ##   if (input$cause == "ALL"
  ##       | input$cause == "") quote(group)
  ##   else quote(cause)})


  ## output$delay_graph <-
  ##   renderPlotly({
  ##       g <- eval(make_graph(
  ##                   cause_data(),
  ##                   xaxis.col(),
  ##                   xaxis()))
  ##       Sys.sleep(0.3)
  ##       g})


  ## output$delay_slider <- renderUI(
  ##   sliderInput("delay_range",
  ##     label = tags$div(
  ##       h2("Delay range"),
  ##       tags$p("The delay is expressed in hours")),
  ##     min = 0, max = floor(max(filt_data()$delay, na.rm = TRUE)),
  ##     value = c(min, max)))
}
