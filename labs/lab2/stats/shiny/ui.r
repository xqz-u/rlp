make.selectize <- function(id, label, multiple = TRUE) {
                                                        selectizeInput(id,
                                                                       choices = NULL, selected = NULL,
                                                                       multiple = multiple,
                                                                       label = label)
                                                        }

llag_tab <-
  tabItem(
    tabName = "last_lag",
    h1("Last Lag Performance",
      style = "text-align:center;font-size:300%;"),
    fluidRow(
      column(width = 3, make.selectize("xaxis", "Select xaxis", FALSE)),
      column(width = 3, make.selectize("yaxis", "Select yaxis", FALSE)),
      column(width = 2, make.selectize("col", "Select col facet", FALSE)),
      column(width = 2, make.selectize("row", "Select row facet", FALSE)),
      box(
        width = 2, checkboxInput("scale_color", label = "Viridis", value = TRUE),
        checkboxInput("scale_fixed", label = "Color Fixed", value = FALSE))),
    fluidRow(
      box(
        width = 12,
        height = "80rem",
        withSpinner(plotOutput("llag_graph", height = "70rem")))))

best_tab <-
  tabItem(
    tabName = "best",
    h1("Best Performance",
      style = "text-align:center;font-size:300%;"),
    column(width = 3, make.selectize("color", "Select color", FALSE)),
    column(width = 3, make.selectize("metric", "Select metric", FALSE)),
    column(width = 3, make.selectize("col_best", "Select col facet", FALSE)),
    column(width = 3, make.selectize("row_best", "Select row facet", FALSE)),
    fluidRow(
      box(
        width = 12,
        height = "80rem",
        withSpinner(plotOutput("best_graph", height = "70rem")))),
    fluidRow(
        tableOutput("best_table")))

module_body <-
  tabItems(
    llag_tab,
    best_tab

  )

module_sidebar <-
  sidebarMenu(
    id = "sidebar",
    menuItem("Bests", tabName = "best", icon = icon("chart-line")),
    menuItem("Last Lag", tabName = "last_lag", icon = icon("chart-bar")),
    make.selectize("env", "Select Environment"),
    make.selectize("algo", "Select Algorithm"),
    make.selectize("policy", "Select Policy"),
    uiOutput("slider_param"),
    uiOutput("slider_alpha"),
    uiOutput("slider_gamma")
  )

ui <-
  dashboardPage(
    dashboardHeader(
      title = "RL"),
    dashboardSidebar(module_sidebar),
    dashboardBody(
      tags$style(".nav  { padding:1%}"),
      module_body),
    skin = "blue")
