#!/usr/bin/Rscript
source("../requirements.R")

library(shiny)
library(shinydashboard)
library(shinycssloaders)
library(tidyverse)
library(parallel)
library(data.table)
library(viridis)

## set firefox as browser for testing
options(browser = "/usr/bin/firefox")

source("processing/utils.r")
source("processing/filter.r")
source("processing/prodata.r")

cat("Reading data in `../data`...")
data <- read_all("../data") %>%
  do.factor() %>%
  do.param() %>%
  do.id()
cat("DONE")

local_ip <- function() {
  system(
    "ifconfig | grep -Eo 'inet (addr:)?([0-9]*\\.){3}[0-9]*' | grep -Eo '([0-9]*\\.){3}[0-9]*' | grep -v '127.0.0.1'",
    intern = TRUE)
}

run <- function() {
  source("plot.r")
  source("server.r")
  source("ui.r")
  runApp(list(ui = ui, server = server),
    launch.browser = TRUE,
    port = getOption("shiny.port", 8080),
    host = getOption("shiny.host", local_ip()))
}

run()
