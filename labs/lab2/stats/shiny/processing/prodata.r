learners <- c(
  "Sarsa",
  "Random",
  "Q-Learning",
  "DQ-Learning")

selectors <- c(
  "Random",
  "E-Greedy",
  "Softmax",
  "UCB")

read_all <- function(path) {
  do.call(rbind, mclapply(list.files(
    path = path,
    full.names = TRUE, recursive = TRUE),
  fread,
  mc.cores = detectCores())) %>% as_tibble()
}

do.avg <- function(df) {
  df %>%
    group_by(ep, learner, act_sel, alpha, gamma, eps, tau, c, env_t) %>%
    summarise(
      r_mean = mean(rwd), r_sd = sd(rwd),
      steps_mean = mean(steps), steps_sd = sd(steps),
      win.p = win_prob(food_left)) %>%
    ungroup()
}

do.factor <- function(df) {
  df %>%
    group_by(lag, learner, act_sel, alpha, gamma, eps, tau, c, env_t) %>%
    mutate(
      learner = as.factor(learners[learner + 1]),
      act_sel = as.factor(selectors[act_sel + 1]),
      env_t = as.factor(env_t))
}

do.lag <- function(df, LAG = 100) {
  df %>%
    mutate(lag = ep %/% LAG) %>%
    group_by(lag, learner, act_sel, alpha, gamma, eps, tau, c, env_t) %>%
    summarise_if(is.numeric, list(~ mean(.))) %>%
    dplyr::select(-ep)
}


do.pre.all <- function(path) {
  path %>%
    read_all() %>%
    do.avg() %>%
    do.lag()
}

bur <- function(df) {
  df %>%
    do.factor() %>%
    do.param() %>%
    do.id() %>%
    ungroup()
}

read_multi <- function(data_dir) {
  data_dir %>%
    do.pre.all() %>%
    bur()
}

read_run <- function(data_path) {
  data_path %>%
    read.csv() %>%
    as_tibble() %>%
    bur()
}

dump <- function(df, path) {
  write.csv(df, file = path)
}

do.param <- function(df) {
  df %>%
    gather(name_p, param, c(7:9)) %>%
    filter(param != 0 & param != 0.13) %>%
    dplyr::select(-name_p)
}

do.id <- function(df) {
  df %>%
    group_by(lag, learner, act_sel, alpha, gamma, param, env_t) %>%
    mutate(id = paste(learner, act_sel, alpha, gamma, param, env_t))
}
