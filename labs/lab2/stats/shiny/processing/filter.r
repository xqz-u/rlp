make.filter <- function(formula) {
  eval(substitute(
    function(df, value = 0) {
      if (value == "ALL") {
        df
      } else {
        df %>%
          filter(formula)
      }
    }))
}

filter.env <- make.filter(env_t %in% value)
filter.learner <- make.filter(learner %in% value)
filter.sel <- make.filter(act_sel %in% value)
filter.lag <- make.filter(lag %in% value)
filter.llag <- function(df) filter.lag(df, max(df$lag))
filter.alpha <- make.filter(alpha <= value[2] & alpha >= value[1])
filter.gamma <- make.filter(gamma <= value[2] & gamma >= value[1])
filter.param <- make.filter(param <= value[2] & param >= value[1])
filter.id <- make.filter(id %in% value)
filter._best <- make.filter(win.p %in% eval(value))
filter.best_id <- function(df) {
  (df %>%
   filter.llag() %>%
   group_by(learner, act_sel, env_t) %>%
   mutate(param = param) %>%
   filter._best(quote(max(win.p))))$id
}
