#ifndef MISC_H
#define MISC_H

#include <gsl/gsl_rng.h>

#include "judyDefs.h"
#include "utils.h"

/* container for command line parameters and random number generator */
typedef struct Misc {
  int episodes, steps, runs;
  char *port, *stats_file, *env_t;
  double alpha, gamma, eps, tau, c;
} Misc;

/* struct holding whole episode statistics */
typedef struct Performance {
  double ep_rwd, food_left_prop;
  int steps;
} Performance;

typedef enum { Up, Down, Left, Right, Start, End } Action;

typedef enum { Sarsa, Random, QLearn, DQLearn } Algorithm;

typedef enum { uniform, egreedy, softmax, ucb } ActionSelector;

void changeSeed(gsl_rng **r, int seed);
void saveStats(PPvoid_t parr, int run, int ep, Performance perf);
Performance *getStats(Pvoid_t arr, int run, int ep);
void freeStats(Pvoid_t arr);

#endif
