#ifndef EXPERIMENT_H
#define EXPERIMENT_H

#include "agent.h"

typedef struct Experiment {
  Agent *agent;
  Misc params;
  Pvoid_t stats;
  Algorithm algo;
  ActionSelector actSelect;
} Experiment;

Experiment makeExperiment(Misc params, Algorithm algo,
                          ActionSelector actSelect);
void freeExperiment(Experiment exp);
void refreshExperiment(Experiment *exp);
Performance runEpisode(int episode, Experiment *exp);
void runAlgo(int run, Experiment *exp);
void repeatExp(Experiment *exp);
void dumpStats(Experiment *exp);

#endif
