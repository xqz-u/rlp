#ifndef JUDYMACROS_H
#define JUDYMACROS_H

#include <Judy.h>
#include <gsl/gsl_rng.h>

#include "vector.h"

/* NOTE whenever a Judy array/matrix is needed, first define the type it will */
/* contain with the correspoding JL(A|M)Type function!!! */
/* Necessary: existChecker, JL(A|M)Type, defGetterJL(A|M) */

/* TODO accept body of code to further customize available datatypes */
/* TODO be compliant with the getters and do not pass NAME in setters, adders
 * etc. */
/* TODO many functions need to add _##TYPE or similar to their names or it won't
   compile ofc. Moreover cannot pass pointer types, which are also required to
   store structs */

/* General functions used to operate on a Judy array single entry */
#define defSetter(TYPE)                                                        \
  static inline TYPE set(TYPE _, TYPE b) { return b; }
#define defAdder(TYPE)                                                         \
  static inline TYPE add(TYPE a, TYPE b) { return a + b; }

/* Define funcion which will store pointer to initVal into parr at idx if idx
 * is empty */
#define existChecker(TYPE)                                                     \
  static inline TYPE *maybeFallback_##TYPE(PPvoid_t parr, Word_t idx,          \
                                           TYPE initVal) {                     \
    TYPE *pval;                                                                \
    JLG(pval, *parr, idx);                                                     \
    if (pval == NULL) {                                                        \
      JLI(pval, *parr, idx);                                                   \
      *pval = initVal;                                                         \
    }                                                                          \
    return pval;                                                               \
  }

/* ----------------- Array methods --------------- */
/* NOTE fn should just modify the value of the referred array entry */
#define JLAType(TYPE)                                                          \
  static inline void withJLA(PPvoid_t parr, Word_t idx, TYPE val,              \
                             TYPE (*fn)(TYPE x, TYPE y)) {                     \
    TYPE *pval;                                                                \
    JLI(pval, *parr, idx);                                                     \
    *pval = fn(*pval, val);                                                    \
  }
/* NOTE safe_ version sets initVal when index did not have a (pointer to)  */
/* value yet, returns (pointer to) value otherwise */
/* NOTE pass EL_TYPE or the array getter won't work for e.g. matrix (an entry */
/* of an array is pointer to val, entry of matrix is pointer to array) */
#define defGetterJLA(TYPE, EL_TYPE)                                            \
  static inline TYPE *getJLA_##TYPE(Pvoid_t arr, Word_t idx) {                 \
    EL_TYPE pval;                                                              \
    JLG(pval, arr, idx);                                                       \
    return (TYPE *)pval;                                                       \
  }                                                                            \
  static inline TYPE *safe_getJLA_##TYPE##_with(PPvoid_t parr, Word_t idx,     \
                                                TYPE initVal) {                \
    return maybeFallback_##TYPE(parr, idx, initVal);                           \
  }                                                                            \
  static inline TYPE *safe_getJLA_##TYPE(PPvoid_t parr, Word_t idx) {          \
    return safe_getJLA_##TYPE##_with(parr, idx, 0);                            \
  }

#define defSetterJLA(TYPE, NAME)                                               \
  static inline void NAME(PPvoid_t parr, Word_t idx, TYPE val) {               \
    withJLA(parr, idx, val, set);                                              \
  }

#define defAdderJLA(TYPE, NAME)                                                \
  static inline void NAME(PPvoid_t parr, Word_t idx, TYPE val) {               \
    withJLA(parr, idx, val, add);                                              \
  }

#define defPrinterJLA(TYPE, NAME, FMT)                                         \
  static inline void NAME(Pvoid_t arr, char *s) {                              \
    Pvoid_t pv;                                                                \
    Word_t i = 0;                                                              \
                                                                               \
    JLF(pv, arr, i);                                                           \
    while (pv != NULL) {                                                       \
      printf("%s%lu: " FMT "\n", s, i, *(TYPE *)pv);                           \
      JLN(pv, arr, i);                                                         \
    }                                                                          \
  }

static inline void freeJLA(Pvoid_t parr) {
  int rc;
  JLFA(rc, parr);
}

/* util to collct a JLA values into a struct of size == #array entries */
/* NOTE free memory returned */
#define defCollectorJLA(TYPE)                                                  \
  static inline vector_p_##TYPE collectJLA_##TYPE##_existing(Pvoid_t arr) {    \
    Pvoid_t pv;                                                                \
    vector_p_##TYPE vp = make_vector_p_##TYPE(1);                              \
    Word_t i = 0;                                                              \
    int cnt = 0;                                                               \
    JLF(pv, arr, i);                                                           \
    while (pv != NULL) {                                                       \
      vector_##TYPE##_checkresize(vp, cnt);                                    \
      vp->store[cnt] = *(TYPE *)pv;                                            \
      JLN(pv, arr, i);                                                         \
      cnt += 1;                                                                \
    }                                                                          \
    /* if no element was present in the JLA, vp->store is resized to 0 == NULL \
     */                                                                        \
    if (!cnt) {                                                                \
      free_vector_p_##TYPE(vp);                                                \
      return NULL;                                                             \
    }                                                                          \
    /* resize to actual size of content */                                     \
    vector_##TYPE##_resize(vp, cnt);                                           \
    return vp;                                                                 \
  }                                                                            \
  static inline vector_##TYPE collectJLA_##TYPE##_range_with(                  \
      PPvoid_t parr, Word_t start, Word_t end, TYPE initVal) {                 \
    vector_##TYPE v = make_vector_##TYPE(end - start);                         \
    int i = 0;                                                                 \
    for (i = start; i < end; ++i)                                              \
      v.store[i] = *safe_getJLA_##TYPE##_with(parr, i, initVal);               \
    return v;                                                                  \
  }                                                                            \
  static inline vector_##TYPE collectJLA_##TYPE##_range(                       \
      PPvoid_t parr, Word_t start, Word_t end) {                               \
    return collectJLA_##TYPE##_range_with(parr, start, end, 0);                \
  }

/* ----------------- Matrix methods --------------- */
#define JLMType(TYPE)                                                          \
  static inline void withJLM(PPvoid_t parr, Word_t xidx, Word_t yidx,          \
                             TYPE val, TYPE (*fn)(TYPE x, TYPE y)) {           \
    /* create pointer to row if it doesn't exist yet */                        \
    PPvoid_t prow = safe_getJLA_Pvoid_t_with(parr, xidx, NULL);                \
    /* operate on a Judy array value with proper type, using given fn */       \
    withJLA(prow, yidx, val, fn);                                              \
  }

#define defGetterJLM(TYPE)                                                     \
  static inline PPvoid_t safe_getJLM_row(PPvoid_t parr, Word_t xidx) {         \
    return safe_getJLA_Pvoid_t_with(parr, xidx, NULL);                         \
  }                                                                            \
  static inline TYPE *safe_getJLM_##TYPE##_with(PPvoid_t parr, Word_t xidx,    \
                                                Word_t yidx, TYPE initVal) {   \
    /* safely get the row, create it if not present */                         \
    PPvoid_t prow = safe_getJLM_row(parr, xidx);                               \
    /* get the column entry, create it if not present */                       \
    return maybeFallback_##TYPE(prow, yidx, initVal);                          \
  }                                                                            \
  static inline TYPE *safe_getJLM_##TYPE(PPvoid_t parr, Word_t xidx,           \
                                         Word_t yidx) {                        \
    return safe_getJLM_##TYPE##_with(parr, xidx, yidx, 0);                     \
  }

#define defSetterJLM(TYPE, NAME)                                               \
  static inline void NAME(PPvoid_t parr, Word_t xidx, Word_t yidx, TYPE val) { \
    withJLM(parr, xidx, yidx, val, set);                                       \
  }

#define defAdderJLM(TYPE, NAME)                                                \
  static inline void NAME(PPvoid_t parr, Word_t xidx, Word_t yidx, TYPE val) { \
    withJLM(parr, xidx, yidx, val, add);                                       \
  }

/* NOTE fn must be of type void (*JLAprinter)(Pvoid_t parr, char *s) */
#define defPrinterJLM(NAME, JLAprinter)                                        \
  static inline void NAME(Pvoid_t pm) {                                        \
    PPvoid_t prow;                                                             \
    Word_t i = 0;                                                              \
                                                                               \
    JLF(prow, pm, i);                                                          \
    while (prow != NULL) {                                                     \
      printf("%lu:\n", i);                                                     \
      JLAprinter(*prow, " ");                                                  \
      JLN(prow, pm, i);                                                        \
    }                                                                          \
  }

static inline void freeJLM(Pvoid_t pm) {
  PPvoid_t prow;
  Word_t i = 0;
  int rc;

  JLF(prow, pm, i);
  while (prow != NULL) {
    JLFA(rc, *prow);
    JLN(prow, pm, i);
  }

  JLFA(rc, pm);
}

/* ------------------- Useful function defs -------------------- */
/* NOTE CMP_FN must be a function of type int (*fn)(TYPE a, TYPE b) which
 * returns 1 if a > b and 0 otherwise */

#define defArgMaxJLA(TYPE, CMP_FN)                                             \
  /* return indeces of maximum values among existing entries in a Judy */      \
  /* array */                                                                  \
  static inline vector_int argmaxJLA_##TYPE##_existing(Pvoid_t arr) {          \
    vector_p_##TYPE pvals = collectJLA_##TYPE##_existing(arr);                 \
    if (!pvals)                                                                \
      return (vector_int){NULL, 0};                                            \
    vector_int argmax = vector_##TYPE##_argmax(*pvals);                        \
    free_vector_p_##TYPE(pvals);                                               \
    return argmax;                                                             \
  }                                                                            \
  /* return indeces of maximum values in a Judy array between */               \
  /* contiguous indexes start and end, setting void entries in this range */   \
  /* to initVal */                                                             \
  static inline vector_int argmaxJLA_##TYPE##_range_with(                      \
      PPvoid_t parr, Word_t start, Word_t end, TYPE initVal) {                 \
    vector_##TYPE vals =                                                       \
        collectJLA_##TYPE##_range_with(parr, start, end, initVal);             \
    vector_int argmax = vector_##TYPE##_argmax(vals);                          \
    free(vals.store);                                                          \
    return argmax;                                                             \
  }                                                                            \
  static inline vector_int argmaxJLA_##TYPE##_range(                           \
      PPvoid_t parr, Word_t start, Word_t end) {                               \
    return argmaxJLA_##TYPE##_range_with(parr, start, end, 0);                 \
  }                                                                            \
  static inline int argmaxJLA_##TYPE##_range_toss(PPvoid_t parr, Word_t start, \
                                                  Word_t end, gsl_rng *r) {    \
    vector_int argmax = argmaxJLA_##TYPE##_range(parr, start, end);            \
    int break_tie = argmax.store[gsl_rng_uniform_int(r, argmax.size)];         \
    free(argmax.store);                                                        \
    return break_tie;                                                          \
  }

/* These functions are equivalent to their array counterpart, but work on a */
/* matrix row */
#define defArgMaxJLM(TYPE, CMP_FN)                                             \
  /* to comply with the *_existing array version, return NULL vector if the */ \
  /* given row does not exist */                                               \
  static inline vector_int argmaxJLM_row_##TYPE##_existing(Pvoid_t arr,        \
                                                           Word_t row) {       \
    PPvoid_t prow;                                                             \
    JLG(prow, arr, row);                                                       \
    return prow ? argmaxJLA_##TYPE##_existing(*prow) : (vector_int){NULL, 0};  \
  }                                                                            \
  static inline vector_int argmaxJLM_row_##TYPE##_range_with(                  \
      PPvoid_t parr, Word_t row, Word_t start, Word_t end, TYPE initVal) {     \
    return argmaxJLA_##TYPE##_range_with(safe_getJLM_row(parr, row), start,    \
                                         end, initVal);                        \
  }                                                                            \
  static inline vector_int argmaxJLM_row_##TYPE##_range(                       \
      PPvoid_t parr, Word_t row, Word_t start, Word_t end) {                   \
    return argmaxJLM_row_##TYPE##_range_with(parr, row, start, end, 0);        \
  }                                                                            \
  static inline int argmaxJLM_row_##TYPE##_range_toss(                         \
      PPvoid_t parr, Word_t row, Word_t start, Word_t end, gsl_rng *r) {       \
    return argmaxJLA_##TYPE##_range_toss(safe_getJLM_row(parr, row), start,    \
                                         end, r);                              \
  }                                                                            \
  static inline TYPE *maxJLM_row_##TYPE##_range_toss(                          \
      PPvoid_t parr, Word_t row, Word_t start, Word_t end, gsl_rng *r) {       \
    int argmax = argmaxJLM_row_##TYPE##_range_toss(parr, row, start, end, r);  \
    return safe_getJLM_##TYPE(parr, row, argmax);                              \
  }

#endif
