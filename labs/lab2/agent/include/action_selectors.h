#ifndef ACTION_SEL_H
#define ACTION_SEL_H

#include <math.h>

#include "agent.h"
#include "utils.h"

/* attach a new action selector struct to an Agent, together with its
 * exploration  function and freer. Accept a symbol and body of code to perform
 * explorer-based initialization */
#define defExplorer(EXPL, EXPL_FN, FREE_FN, sym, BODY)                         \
  static inline void init_##EXPL(Agent *ag, Misc p) {                          \
    EXPL *sym = safeMalloc(sizeof(*sym));                                      \
    BODY;                                                                      \
    ag->explorer = sym;                                                        \
    ag->explore = EXPL_FN;                                                     \
    ag->freeExplorer = FREE_FN;                                                \
  }

/* structs holding the parameters relative to an action selector */
typedef struct EGreedy {
  double e;
} EGreedy;

typedef struct UCB1 {
  double C;
  Pvoid_t Nsa;
} UCB1;

typedef struct Softmax {
  double tau;
} Softmax;

void init_uniform(Agent *ag, Misc params);

Action egreedy_explore(Agent *ag, PPvoid_t qtablep, long state);
Action ucb_explore(Agent *ag, PPvoid_t qtablep, long state);
void freeUCB(void *explorer);
Action softmax_explore(Agent *ag, PPvoid_t qtablep, long state);

/* explorers definitions */
defExplorer(EGreedy, egreedy_explore, basicFree, egp, egp->e = p.eps)
    defExplorer(UCB1, ucb_explore, freeUCB, ucbp, ucbp->C = p.c;
                ucbp->Nsa = (Pvoid_t)NULL)
        defExplorer(Softmax, softmax_explore, basicFree, softp,
                    softp->tau = p.tau)

#endif
