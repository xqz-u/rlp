#ifndef AGENT_H
#define AGENT_H

#include "ZEnv.h"
#include "judyDefs.h"
#include "misc.h"

typedef struct Agent Agent;

typedef void (*initter)(Agent *agp, Misc p);

typedef Action (*act_selector)(Agent *ag, PPvoid_t qtablep, long state);
typedef Percept (*unit_learn)(Agent *agp);
typedef void (*freer)(void *vp);

struct Agent {
  Zenv channel;
  long state;
  int seed;
  gsl_rng *r;
  void *explorer, *learner;
  act_selector explore;
  unit_learn learn, learn_setup;
  freer freeLearner, freeExplorer;
};

Agent *initAgent(initter expl_initter, initter ep_initter, Misc p);
void openEnv(Agent *ag, char *port);
void freeAgent(Agent *agp);
Percept basicSetup(Agent *ag);

#endif
