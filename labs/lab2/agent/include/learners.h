#ifndef LEARNERS_H
#define LEARNERS_H

#include "agent.h"
#include "utils.h"

#define defLearner(LEARN, LEARN_FN, FREE_FN, SETUP_FN)                         \
  static inline void init_##LEARN(Agent *agp, Misc p) {                        \
    LEARN *sym = safeMalloc(sizeof(*sym));                                     \
    sym->alpha = p.alpha;                                                      \
    sym->gamma = p.gamma;                                                      \
    agp->learner = sym;                                                        \
    agp->learn = LEARN_FN;                                                     \
    agp->learn_setup = SETUP_FN;                                               \
    agp->freeLearner = FREE_FN;                                                \
  }

typedef struct SARSA {
  double alpha, gamma;
  Pvoid_t q_table;
  Action action;
} SARSA;

typedef struct QL {
  double alpha, gamma;
  Pvoid_t q_table;
} QL;

typedef struct QL2 {
  double alpha, gamma;
  Pvoid_t q_table1, q_table2;
} QL2;

void init_Random(Agent *ag, Misc params);

Percept sarsaAct(Agent *agp);
Percept sarsaSetup(Agent *agp);
void freeSarsa(void *learner);
Percept qlAct(Agent *agp);
void freeQL(void *learner);
Percept ql2Act(Agent *agp);
void freeQL2(void *learner);

defLearner(SARSA, sarsaAct, freeSarsa, sarsaSetup)
    defLearner(QL, qlAct, freeQL, basicSetup)
        defLearner(QL2, ql2Act, freeQL2, basicSetup)

#endif
