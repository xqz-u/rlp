#include <getopt.h>

#include "../include/experiment.h"

void help(char *program) {
  char *usage =
      "-e INT -s INT -r INT -A [0,3] -a [0,3] -E STR -p STR -f STR "
      "-x FLOAT -g FLOAT [-t FLOAT -y FLOAT -c FLOAT] -h.\n\t-e\t\t"
      "training games for the agent\n\t-s\t\ttimesteps for an "
      "episode\n\t-r\t\trepetitions of -e games\n\t-A\t\tRL algorithm "
      "[SARSA, "
      "Random, Q-Learning, Double Q-Learning]\n\t-a\t\texploration policy "
      "[Uniform, eGreedy, Softmax, UCB]\n\t-E\t\tenvironment type (string "
      "encoding environment attributes)\n\t-p\t\tport the environment is "
      "listening on\n\t-f\t\tfilename of statistics dump\n\t-x\t\talpha (all "
      "implemented RL algos)\n\t-g\t\tgamma (all implemented RL "
      "algos)\n\t-t\t\ttau (Softmax exploration rate)\n\t-y\t\tepsilon "
      "(eGreedy exploration rate)\n\t-c\t\tC (UCB exploration rate)\n";
  printf("Usage: %s %s", program, usage);
}

#define _(CHAR, BODY)                                                          \
  case CHAR:                                                                   \
    WHENARG(BODY)
#define WHENARG(BODY)                                                          \
  if (optarg)                                                                  \
    BODY;                                                                      \
  break;
#define GetOption(STRING, PROGRAM, BODY)                                       \
  char __gensym;                                                               \
  while ((__gensym = getopt(argc, argv, STRING)) != -1) {                      \
    switch (__gensym) {                                                        \
      BODY default : help(PROGRAM);                                            \
      return 1;                                                                \
    }                                                                          \
  }

int main(int argc, char **argv) {
  Algorithm algo;
  ActionSelector explorer;
  Misc params = {0};

  GetOption(
      ":e:s:r:A:a:E:x:g:t:y:c:p:f:h:", argv[0],
      _('e', params.episodes = atoi(optarg)) _('s', params.steps = atoi(optarg))
          _('r', params.runs = atoi(optarg)) _('A', algo = atoi(optarg))
              _('a', explorer = atoi(optarg)) _('p', params.port = optarg)
                  _('f', params.stats_file = optarg)
                      _('E', params.env_t = optarg)
                          _('x', params.alpha = atof(optarg))
                              _('g', params.gamma = atof(optarg))
                                  _('t', params.tau = atof(optarg))
                                      _('y', params.eps = atof(optarg))
                                          _('c', params.c = atof(optarg))
                                              _('h', help(argv[0]); return 0));

  if (algo == 1 || explorer == 0) {
    if (!(explorer == 0 && algo == 1)) {
      fprintf(
          stderr,
          "Random agent [1] must be paired with uniform action selector [0]\n");
      return 1;
    }
  }

  Experiment exp = makeExperiment(params, algo, explorer);
  timeExpr(repeatExp(&exp));
  freeExperiment(exp);

  return 0;
}
