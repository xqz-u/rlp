#include "../include/experiment.h"
#include "../include/action_selectors.h"
#include "../include/learners.h"

#define LEARNERS 4
#define EXPLORERS 4

initter learn_initters[LEARNERS] = {init_SARSA, init_Random, init_QL, init_QL2};
initter expl_initters[EXPLORERS] = {init_uniform, init_EGreedy, init_Softmax,
                                    init_UCB1};

Experiment makeExperiment(Misc params, Algorithm algo,
                          ActionSelector actSelect) {
  return (Experiment){.agent = initAgent(expl_initters[actSelect],
                                         learn_initters[algo], params),
                      .params = params,
                      .stats = (Pvoid_t)NULL,
                      .algo = algo,
                      .actSelect = actSelect};
}

void freeExperiment(Experiment exp) {
  gsl_rng_free(exp.agent->r);
  freeAgent(exp.agent);
  free(exp.agent);
  freeStats(exp.stats);
}

void refreshExperiment(Experiment *exp) {
  Agent *ag = exp->agent;
  freeAgent(ag);
  expl_initters[exp->actSelect](ag, exp->params);
  learn_initters[exp->algo](ag, exp->params);
  ag->seed += 1;
  changeSeed(&ag->r, ag->seed);
}

Performance runEpisode(int episode, Experiment *exp) {
  Agent *ag = exp->agent;
  double episodeRwd = 0;
  Percept perc;
  int i;
  for (i = 1; i < exp->params.steps; ++i) {
    perc = ag->learn(ag);
    ag->state = perc.state;
    episodeRwd += perc.reward;
    if (perc.dead)
      break;
  }
  return (Performance){
      .ep_rwd = episodeRwd, .food_left_prop = perc.food_left, .steps = i};
}

void runAlgo(int run, Experiment *exp) {
  int i;
  for (i = 0; i < exp->params.episodes; ++i) {
    exp->agent->learn_setup(exp->agent);
    saveStats(&exp->stats, run, i, runEpisode(i, exp));
  }
}

void repeatExp(Experiment *exp) {
  int i;
  openEnv(exp->agent, exp->params.port);
  for (i = 0; i < exp->params.runs; ++i) {
    runAlgo(i, exp);
    refreshExperiment(exp);
  }
  killEnv(exp->agent->channel);
  dumpStats(exp);
}

void dumpStats(Experiment *exp) {
  FILE *fp;

  fp = freopen(exp->params.stats_file, "w", stdout);
  if (fp == NULL) {
    perror("failed to open stats file:");
    exit(1);
  }

  /* write csv header */
  printf(
      "ep,learner,act_sel,alpha,gamma,eps,tau,c,env_t,rwd,food_left,steps\n");

  int i, j;
  Performance *pperf;

  for (i = 0; i < exp->params.runs; ++i) {
    for (j = 0; j < exp->params.episodes; ++j) {
      pperf = getStats(exp->stats, i, j);
      printf("%d,%d,%d,%.3lf,%.3lf,%lf,%lf,%lf,%s,%.3lf,%.3lf,%d\n", j,
             exp->algo, exp->actSelect, exp->params.alpha, exp->params.gamma,
             exp->params.eps, exp->params.tau, exp->params.c, exp->params.env_t,
             pperf->ep_rwd, pperf->food_left_prop, pperf->steps);
    }
  }

  fclose(fp);
}
