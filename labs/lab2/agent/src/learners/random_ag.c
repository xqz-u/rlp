#include "../../include/learners.h"

Percept randomAct(Agent *ag) {
  return envPercept(ag->explore(ag, NULL, ag->state), ag->channel);
}

void init_Random(Agent *ag, Misc params) {
  ag->learn = randomAct;
  ag->freeLearner = basicFree;
  ag->learn_setup = basicSetup;
}
