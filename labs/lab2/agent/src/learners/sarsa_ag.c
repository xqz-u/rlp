#include "../../include/learners.h"

Percept sarsaAct(Agent *agp) {
  SARSA *sarsap = (SARSA *)agp->learner;
  PPvoid_t qtablep = &sarsap->q_table;
  Percept perc = envPercept(sarsap->action, agp->channel);
  Action a_n = agp->explore(agp, qtablep, perc.state);
  double q_sa = *safe_getJLM_double(qtablep, agp->state, sarsap->action);
  double q_sa_n = *safe_getJLM_double(qtablep, perc.state, a_n);
  addJLMD(qtablep, agp->state, sarsap->action,
          sarsap->alpha * (perc.reward + sarsap->gamma * q_sa_n - q_sa));
  sarsap->action = a_n;
  return perc;
}

Percept sarsaSetup(Agent *agp) {
  SARSA *sarsap = (SARSA *)agp->learner;
  Percept birth = basicSetup(agp);
  sarsap->action = agp->explore(agp, &sarsap->q_table, agp->state);
  return birth;
}

void freeSarsa(void *learner) {
  SARSA *sarsap = (SARSA *)learner;
  freeJLM(sarsap->q_table);
  basicFree(sarsap);
}
