#include "../../include/learners.h"

Pvoid_t sumQrow(PPvoid_t qt1, PPvoid_t qt2, long state) {
  Pvoid_t sumStatesQ = (Pvoid_t)NULL;
  int i;
  for (i = 0; i < Start; ++i)
    setJLMD(&sumStatesQ, state, i,
            *safe_getJLM_double(qt1, state, i) +
                *safe_getJLM_double(qt2, state, i));
  return sumStatesQ;
}

Percept ql2Act(Agent *agp) {
  QL2 *ql2 = (QL2 *)agp->learner;
  PPvoid_t qtable1 = &ql2->q_table1;
  PPvoid_t qtable2 = &ql2->q_table2;
  Pvoid_t sumQstates = sumQrow(qtable1, qtable2, agp->state);
  Action a_s = agp->explore(agp, &sumQstates, agp->state);
  Percept perc = envPercept(a_s, agp->channel);
  int choose = rand() % 2;
  PPvoid_t q1 = choose ? qtable2 : qtable1;
  PPvoid_t q2 = choose ? qtable1 : qtable2;
  addJLMD(q1, agp->state, a_s,
          ql2->alpha *
              (perc.reward +
               ql2->gamma *
                   *safe_getJLM_double(q2, perc.state,
                                       argmaxJLM_row_double_range_toss(
                                           q1, perc.state, 0, Start, agp->r)) -
               *safe_getJLM_double(q1, agp->state, a_s)));
  freeJLM(sumQstates);
  return perc;
}

void freeQL2(void *learner) {
  QL2 *qlp = (QL2 *)learner;
  freeJLM(qlp->q_table1);
  freeJLM(qlp->q_table2);
  free(qlp);
}
