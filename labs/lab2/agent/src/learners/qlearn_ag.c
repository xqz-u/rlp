#include "../../include/learners.h"

Percept qlAct(Agent *agp) {
  QL *qlp = (QL *)agp->learner;
  PPvoid_t qtablep = &qlp->q_table;
  Action a_s = agp->explore(agp, qtablep, agp->state);
  Percept perc = envPercept(a_s, agp->channel);
  addJLMD(qtablep, agp->state, a_s,
          qlp->alpha *
              (perc.reward +
               qlp->gamma * *maxJLM_row_double_range_toss(qtablep, perc.state,
                                                          0, Start, agp->r) -
               *safe_getJLM_double(qtablep, agp->state, a_s)));
  return perc;
}

void freeQL(void *learner) {
  QL *qlp = (QL *)learner;
  freeJLM(qlp->q_table);
  free(qlp);
}
