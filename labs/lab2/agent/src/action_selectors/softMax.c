#include "../../include/action_selectors.h"

Action softmax_explore(Agent *ag, PPvoid_t qtablep, long state) {
  Softmax *softp = (Softmax *)ag->explorer;
  long double thr, sum = 0, cum_prob = 0;
  int i;

  vector_double q_vals =
      collectJLA_double_range(safe_getJLM_row(qtablep, state), 0, Start);

  for (i = 0; i < Start; ++i)
    sum += expl((long double)q_vals.store[i] / softp->tau);

  thr = gsl_rng_uniform(ag->r);
  for (i = 0; i != Start; ++i) {
    cum_prob += expl((long double)q_vals.store[i] / softp->tau) / sum;
    if (cum_prob >= thr)
      break;
  }

  free(q_vals.store);
  return (Action)i;
}
