#include "../../include/action_selectors.h"

Action uniform_explore(Agent *ag, PPvoid_t qtablep, long state) {
  return gsl_rng_uniform_int(ag->r, Start);
}

void init_uniform(Agent *ag, Misc params) {
  ag->explore = uniform_explore;
  ag->freeExplorer = basicFree;
}
