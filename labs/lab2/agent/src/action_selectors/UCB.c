#include "../../include/action_selectors.h"

Action ucb_explore(Agent *ag, PPvoid_t qtablep, long state) {
  UCB1 *ucbp = (UCB1 *)ag->explorer;
  PPvoid_t Nsa = &ucbp->Nsa;
  double a_s[Start], Ns = 0;
  int i, argmax;

  /* count total number of time been in this state */
  for (i = 0; i < Start; ++i) {
    a_s[i] = *safe_getJLM_double(Nsa, state, i);
    if (!a_s[i]) { /* first visit all actions in this state */
      addJLMD(Nsa, state, i, 1);
      return (Action)i;
    }
    Ns += a_s[i];
  }

  /* add bonus to q action-value of state */
  vector_double q_bonus = make_vector_double(Start);
  for (i = 0; i < Start; ++i)
    q_bonus.store[i] = *safe_getJLM_double(qtablep, state, i) +
                       100 * ucbp->C * sqrt(2 * (log(Ns) / a_s[i]));

  /* return best action */
  argmax = argmax_double(q_bonus, ag->r);
  addJLMD(Nsa, state, argmax, 1);
  free(q_bonus.store);
  return argmax;
}

void freeUCB(void *explorer) {
  freeJLM(((UCB1 *)explorer)->Nsa);
  basicFree(explorer);
}
