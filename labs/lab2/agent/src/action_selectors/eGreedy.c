#include "../../include/action_selectors.h"

Action egreedy_explore(Agent *ag, PPvoid_t qtablep, long state) {
  return gsl_rng_uniform(ag->r) < ((EGreedy *)ag->explorer)->e
             ? gsl_rng_uniform_int(ag->r, Start)
             : argmaxJLM_row_double_range_toss(qtablep, state, 0, Start, ag->r);
}
