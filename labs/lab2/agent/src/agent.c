#include "../include/agent.h"
#include "../include/ZEnv.h"
#include "../include/utils.h"

Agent *initAgent(initter expl_initter, initter learn_initter, Misc p) {
  Agent *ag = safeMalloc(sizeof(*ag));
  expl_initter(ag, p);
  learn_initter(ag, p);
  gsl_rng *r = gsl_rng_alloc(gsl_rng_default);
  gsl_rng_set(r, 0);
  ag->seed = 0;
  ag->r = r;
  return ag;
}

void freeAgent(Agent *agp) {
  agp->freeExplorer(agp->explorer);
  agp->freeLearner(agp->learner);
}

void openEnv(Agent *ag, char *port) { ag->channel = makeZenv(port); }

Percept basicSetup(Agent *ag) {
  Percept birth = envPercept(Start, ag->channel);
  ag->state = birth.state;
  return birth;
}
