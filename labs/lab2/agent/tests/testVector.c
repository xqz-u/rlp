#include "../include/vector.h"

/* void dummy(void) { */
/*   int i, size = 1; */
/*   vector_int v = make_vector_int(size); */
/*   printf("%zu\n", sizeof(int)); */
/*   printf("og %p (%p)\n", v.store, &v.store); */
/*   for (i = 0; i < size + 2; ++i) { */
/*     printf("before %p %d\n", v.store, v.size); */
/*     vector_int_checkresize(&v, i); */
/*     printf("after %p %d\n", v.store, v.size); */
/*     v.store[i] = i; */
/*     vector_int_print(v, i); */
/*     printf("...........\n"); */
/*   } */
/*   printf("then %p (%p)\n", v.store, &v.store); */
/*   printf("-------------\n"); */
/*   vector_int_print(v, i); */
/*   free(v.store); */
/* } */

/* void dummy(void) { */
/*   int i, size = 1; */
/*   vector v = vector_int(size); */
/*   printf("og %p (%p)\n", v.store, &v.store); */
/*   for (i = 0; i < size + 2; ++i) { */
/*     vector_int_insert(&v, i, (int)rand()); */
/*     vector_int_print(v, v.size); */
/*     printf("...........\n"); */
/*   } */
/*   printf("then %p (%p)\n", v.store, &v.store); */
/*   printf("-------------\n"); */
/*   vector_int_print(v, v.size); */
/*   free_vector(v); */
/* } */

void testargmaxvp(void) {
  int i, size = 10;
  vector_p vp = vector_p_double(size);
  for (i = 0; i < size + 4; ++i)
    vector_p_double_insert(vp, i, rand());
  vector_p_double_print(vp, i);
  /* vector_p_double_print(vp, vp->size); */
  /* vector_p best = vector_p_double_argmax(vp); */
  /* vector_p_double_print(best, best->size); */
  free_vector_p(vp);
  /* free_vector_p(best); */
}

void insert(void *store, int idx, double val) {
  *(double *)((char *)store + (idx * sizeof(double))) = val;
}

void print(void *store, int len) {
  for (int i = 0; i < len; ++i)
    printf("%lf ", *(double *)((char *)store + (i * sizeof(double))));
  printf("\n");
}

void test(void) {
  void *vp = safeMalloc(5 * sizeof(double));
  for (int i = 0; i < 3; ++i)
    insert(vp, i, rand());
  print(vp, 5);
  free(vp);
}

int main(void) {
  /* test(); */
  testargmaxvp();
  /* dummy(); */
  return 0;
}
