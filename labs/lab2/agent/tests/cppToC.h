#ifndef CPPTOC_H
#define CPPTOC_H

#ifdef __cplusplus
extern "C" {
#endif

extern void myCppFunction(int n);

#ifdef __cplusplus
}
#endif

#endif
