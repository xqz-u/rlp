#include "../include/judyUtils.h"

void testArr(void) {
  Pvoid_t parr = (Pvoid_t)NULL;

  addJLAD(&parr, 10, 7);
  addJLAD(&parr, 15, 8.7);

  printJLAD(parr, "");

  addJLAD(&parr, 10, 10.4);
  setJLAD(&parr, 15, 21);

  printJLAD(parr, "");
  printf("%lf\n", *getJLAD(parr, 10));
  /* printf("%lf\n", *getJLAD(parr, 21)); */
  printf("%lf\n", *safe_getJLAD(&parr, 21));
  setJLAD(&parr, 21, 34);
  printf("%lf\n", *getJLAD(parr, 21));
  printf("%lf\n", *safe_getJLAD_with(&parr, 21, 349.5));
  safe_getJLAD_with(&parr, 22, 349.5);
  setJLAD(&parr, 22, 344.5);

  printJLAD(parr, "");
  freeJLA(parr);
}

void testone(PPvoid_t pm) {
  setJLMD(pm, 4, 100, 6);
  printf("%lf\n", *safe_getJLMD_with(pm, 19, 150, 11.1));
}

void testMatrix(void) {
  Pvoid_t pmatrix = (Pvoid_t)NULL;

  setJLMD(&pmatrix, 1, 1, 2.6);
  addJLMD(&pmatrix, 1, 2, 50);
  printJLM(pmatrix, printJLAD);
  printf("---------\n");

  setJLMD(&pmatrix, 1, 2, 3.6);
  setJLMD(&pmatrix, 4, 4, 6);
  setJLMD(&pmatrix, 20, 150, 1837.7);
  printJLM(pmatrix, printJLAD);
  printf("---------\n");

  printf("%lf\n", *safe_getJLMD(&pmatrix, 20, 150));
  printf("%lf\n", *safe_getJLMD(&pmatrix, 20, 150));
  printf("%lf\n", *safe_getJLMD(&pmatrix, 21, 150));
  printf("%lf\n", *safe_getJLMD_with(&pmatrix, 22, 150, 11.1));
  safe_getJLMD_with(&pmatrix, 23, 150, 11.1);
  printJLM(pmatrix, printJLAD);
  printf("---------\n");

  testone(&pmatrix);
  printJLM(pmatrix, printJLAD);

  freeJLM(pmatrix);
}

int main(void) {
  testArr();
  testMatrix();
  return 0;
}
