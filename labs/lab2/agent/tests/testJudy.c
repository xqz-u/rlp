#include <Judy.h>
#include <stdio.h>
#include <stdlib.h>

#include "../include/judyUtils.h"

void testJudy(void) {
  Pvoid_t PJLarr = (Pvoid_t)NULL;

  Word_t idx = 100;
  PWord_t pval;

  /* store the pointer to val in the array (ocio Judy changes the pointer) */
  JLI(pval, PJLarr, idx);
  printf("%p\n", (void *)pval);
  *pval = 1;
  printf("%lu\n", *pval);

  /* get the pointer associated with idx in the array */
  JLG(pval, PJLarr, idx);
  printf("%p\n", (void *)pval);
  printf("%lu\n", *pval);

  /* modify it */
  *pval += 10;
  JLG(pval, PJLarr, idx);
  printf("%p\n", (void *)pval);
  printf("%lu\n", *pval);

  int rc;
  JLFA(rc, PJLarr);
  printf("%d %p\n", rc, (void *)PJLarr);
}

void testJudyD(void) {
  Pvoid_t PJLarr = (Pvoid_t)NULL;

  double *idx = malloc(sizeof(*idx));
  *idx = 10.5;
  double *pval;

  JLI(pval, PJLarr, (void *)idx);
  printf("%p\n", (void *)pval);
  *pval = 1.5;
  printf("%lf\n", *pval);

  JLG(pval, PJLarr, (void *)idx);
  printf("%p\n", (void *)pval);
  printf("%lf\n", *pval);

  free(idx);

  int rc;
  JLFA(rc, PJLarr);
  printf("%d %p\n", rc, (void *)PJLarr);
}

void manExample(void) {
  Word_t Index;   // array index
  Word_t Value;   // array element value
  Word_t *PValue; // pointer to array element value
  int Rc_int;     // return code

  Pvoid_t PJLArray = (Pvoid_t)NULL; // initialize JudyL array

  while (scanf("%lu %lu", &Index, &Value)) {
    JLI(PValue, PJLArray, Index);
    if (PValue == PJERR)
      exit(1);
    *PValue = Value; // store new value
  }

  // Next, visit all the stored indexes in sorted order, first ascending,
  // then descending, and delete each index during the descending pass.
  Index = 0;
  JLF(PValue, PJLArray, Index);
  while (PValue != NULL) {
    printf("%lu %lu\n", Index, *PValue);
    JLN(PValue, PJLArray, Index);
  }

  Index = -1;
  JLL(PValue, PJLArray, Index);
  while (PValue != NULL) {
    printf("%lu %lu\n", Index, *PValue);
    JLD(Rc_int, PJLArray, Index);
    if (Rc_int == JERR)
      exit(1);
    JLP(PValue, PJLArray, Index);
  }
}

double *set(PPvoid_t parr, Word_t idx, double val) {
  double *pval;
  printf("%p\n", (void *)pval);
  JLI(pval, *parr, idx);
  printf("%p\n", (void *)pval);
  *pval = val;
  return pval;
}

double *get(PPvoid_t parr, Word_t idx) {
  double *pval;
  JLG(pval, *parr, idx);
  return pval;
}

void test(void) {
  Pvoid_t PJLarr = (Pvoid_t)NULL;
  double *r = set(&PJLarr, 10, 10.4);
  printf("%p %lf\n", (void *)r, *r);

  double *rr = get(&PJLarr, 10);
  printf("%p %lf\n", (void *)rr, *rr);

  int rc;
  JLFA(rc, PJLarr);
}

void testMatrix(void) {
  Pvoid_t PJLarr = (Pvoid_t)NULL;
  Pvoid_t PJLarr1 = (Pvoid_t)NULL;

  Word_t x = 10, y = 3;
  PPvoid_t px;
  double *py;

  double val = 5759.6;

  JLI(px, PJLarr, x);
  /* printf("%p %p\n", (void *)px, (void *)PJLarr1); */
  px = &PJLarr1;
  /* printf("%p %p\n", (void *)px, (void *)PJLarr1); */

  JLI(py, *px, y);
  *py = val;

  printf("%lf\n", *py);
  /* double *g; */
  /* JLG(g, ); */

  int rc;
  JLFA(rc, *px);
  JLFA(rc, PJLarr);
}

void mset(PPvoid_t parr, Word_t xidx, Word_t yidx, double val) {
  Pvoid_t PJLarr = (Pvoid_t)NULL;
  PPvoid_t pcol;

  JLG(pcol, *parr, xidx);
  printf("%p\n", (void *)pcol);

  if (pcol == NULL) {
    printf("create new column\n");
    JLI(pcol, *parr, xidx);
    *pcol = PJLarr;
  }

  double *pval;
  JLI(pval, *pcol, yidx);
  *pval = val;
}

double *mget(PPvoid_t parr, Word_t xidx, Word_t yidx) {
  PPvoid_t pcol;
  double *r;
  JLG(pcol, *parr, xidx);
  JLG(r, *pcol, yidx);
  return r;
}

void printFreeJM(Pvoid_t pm) {
  Pvoid_t p;
  Word_t i = 0;
  int rc;
  JLF(p, pm, i);
  while (p != NULL) {
    printf("%p\n", (void *)p);
    JLFA(rc, *p);
    JLN(p, pm, i);
  }
}

void testMset(void) {
  Pvoid_t pmatrix = (Pvoid_t)NULL;

  mset(&pmatrix, 1, 1, 2.6);
  printf("%lf\n", *mget(&pmatrix, 1, 1));

  mset(&pmatrix, 1, 2, 3.6);
  printf("%lf\n", *mget(&pmatrix, 1, 2));

  mset(&pmatrix, 4, 4, 6);
  printf("%lf\n", *mget(&pmatrix, 4, 4));

  printFreeJM(pmatrix);

  int rc;
  /* JLFA(rc, *pcol); */
  JLFA(rc, pmatrix);
}

int main(void) {
  /* testJudy(); */
  /* testJudyD(); */
  /* manExample(); */
  /* test(); */
  /* testMatrix(); */
  /* testMset(); */
  testUpdate();
  return 0;
}
