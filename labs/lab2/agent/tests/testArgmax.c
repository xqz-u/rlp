#include <stdio.h>
#include <stdlib.h>

#include "../include/judyDefs.h"

void testMacroArgmaxContM(void) {
  Pvoid_t arr = (Pvoid_t)NULL;

  int j;
  int row = 5;
  for (j = 0; j < 10; ++j)
    setJLMD(&arr, row, j, rand());
  printJLMD(arr);

  int s = 0, e = 10;

  vector_int argm = argmaxJLM_row_double_range(&arr, row, s, e);
  vector_int_print(argm, argm.size);
  free(argm.store);

  argm = argmaxJLM_row_double_range(&arr, row + 10, s, e);
  vector_int_print(argm, argm.size);
  printJLMD(arr);
  free(argm.store);

  freeJLM(arr);
}

void testArgmaxExistM(void) {
  Pvoid_t arr = (Pvoid_t)NULL;
  int j;
  int row = 5;
  for (j = 0; j < 10; ++j)
    setJLMD(&arr, row, j, rand());
  printJLMD(arr);

  vector_int argm = argmaxJLM_row_double_existing(arr, row);
  vector_int_print(argm, argm.size);
  free(argm.store);

  argm = argmaxJLM_row_double_existing(arr, row + 10);
  vector_int_print(argm, argm.size);
  free(argm.store);

  freeJLM(arr);
}

void testArrArgmax(void) {
  Pvoid_t arr = (Pvoid_t)NULL;

  int j;
  for (j = 0; j < 10; ++j) {
    setJLAD(&arr, j, rand());
  }

  Pvoid_t pv;
  Word_t i = 0, argmax;
  double currV, maxV;

  JLF(pv, arr, i);
  argmax = i;
  maxV = *(double *)pv;
  while (pv != NULL) {
    currV = *(double *)pv;
    /* if (compareDouble(currV, maxV)) */
    /*   printf("%lf %lf are equal\n", currV, maxV); */
    if (currV > maxV) {
      maxV = currV;
      argmax = i;
      printf("updated max %lf\n", maxV);
    }

    JLN(pv, arr, i);
  }

  printf("max val is: %lf\n", maxV);
  printf("argMax is: %d\n", (int)argmax);

  printJLAD(arr, "");
  freeJLA(arr);
}

int main(void) {
  /* testArrArgmax(); */
  /* testMacroArgmaxExist(); */
  /* testMacroArgmaxCont(); */
  /* testMacroArgmaxExistM(); */
  /* testCollect(); */
  /* testArgmaxExistM(); */
  testMacroArgmaxContM();
  return 0;
}
