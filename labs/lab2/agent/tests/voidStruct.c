#include <stdio.h>
#include <stdlib.h>

typedef struct Contain {
  int cnt;
  void *store;
} Contain;

typedef int (*member)(Contain);

typedef struct Child {
  char *literal;
  double n;
  member memb_fn;
} Child;

typedef struct Child1 {
  char *literal;
  double n;
  member memb_fn;
} Child1;

int example_memb(Contain c) {
  printf("%d\n", c.cnt);
  printf("%p\n", c.store);
  return c.cnt + 1;
}

int inFn(Child *c, Contain cc) {
  printf("%s %.2lf\n", c->literal, c->n);
  return c->memb_fn(cc);
}

Child makeChild(char *s, double n) { return (Child){s, n, example_memb}; }

void testStr(void) {
  Child f = makeChild("mimmo", random());
  Child1 f1 = (Child1){"mimmo1", random(), example_memb};
  Contain x = (Contain){.cnt = 10, .store = &f};
  Contain xx = (Contain){.cnt = 10, .store = &f1};
  printf("%d\n", ((Child *)x.store)->memb_fn(x));
  printf("%d\n", ((Child1 *)x.store)->memb_fn(xx));
  /* printf("%d\n", inFn(x.store, x)); */
}

typedef void (*sameFn)(void);

struct Member {
  sameFn fn;
};

struct Member1 {
  sameFn fn;
  int boh;
};

struct Cont {
  void *child;
};

void fn(void) { printf("some print!\n"); }

void testStupid(void) {
  struct Member *memb = malloc(sizeof(*memb));
  *memb = (struct Member){fn};
  struct Cont c = (struct Cont){memb};
  ((struct Member *)c.child)->fn();
  free(c.child);
}

int main(void) {
  testStupid();
  /* testStr(); */
  return 0;
}
