#include <stdio.h>
#include <stdlib.h>

#include "../include/experiment.h"
#include "../include/utils.h"

int main(int argc, char **argv) {
  char port[100];
  printf("Give port: ");
  scanf("%s", port);

  int seed = 0;
  gsl_rng *r = gsl_rng_alloc(gsl_rng_default);
  gsl_rng_set(r, seed);

  Params p = (Params){.r = r, .port = port, .seed = seed};

  int n_episodes, steps, runs, act_sel;
  Algorithm algo = Sarsa;
  Experiment exp;

  printf("Give episodes, steps, runs, action_selector[0,1]: ");
  scanf("%d %d %d %d", &n_episodes, &steps, &runs, &act_sel);
  if (act_sel == 1) {
    double eps;
    printf("epsilon for egreedy: ");
    scanf("%lf", &eps);
    p.eps = eps;
  }
  exp = makeExperiment(n_episodes, steps, runs, NULL, &p, algo,
                       (ActionSelector)act_sel);
  timeExpr(repeatExp(&exp););
  freeExperiment(exp);
  /* timeExpr(exp=Experiment(n_episodes, steps, runs, NULL, &p, 1, 0);) */

  gsl_rng_free(r);

  return 0;
}
