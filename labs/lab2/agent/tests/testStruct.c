#include <stdio.h>

#include "../include/judyDefs.h"

typedef struct A {
  int val;
} A;

typedef struct B {
  A *pa;
} B;

typedef struct J {
  Pvoid_t m;
} J;

typedef struct C {
  J *pj;
} C;

void mod(A *pa) { pa->val += 10; }

void mod2(C *cc) {
  int i;
  PPvoid_t pm = &(cc->pj->m);
  for (i = 1; i < 6; ++i)
    setJLMD(pm, i, i * 2, (double)i / 6.5);

  printf("%lf\n", *safe_getJLMD(pm, 4, 6));
}

void testJ(void) {
  J ex = (J){(Pvoid_t)NULL};
  C c_ex = (C){&ex};

  printJLMD(c_ex.pj->m);
  mod2(&c_ex);
  printJLMD(c_ex.pj->m);

  freeJLM(c_ex.pj->m);
}

int test(void) {

  A ex = (A){100};
  B cont = (B){&ex};

  printf("%d\n", cont.pa->val);

  mod(cont.pa);

  printf("%d\n", cont.pa->val);

  return 0;
}

int main(void) {
  testJ();
  return 0;
}
