#!/usr/bin/python
import itertools as it
import json


def agent_configs(agent_map: dict):
    headers_switch = [f"-{fl}" for fl in agent_map.keys()]
    for i, comb in enumerate(it.product(*agent_map.values())):
        yield {**dict(zip(headers_switch, comb)), "-f": f"{i}.csv", "-r": 10}


def single_env_config(world_size, gath_vision, hunt_n, food_spec, reward_spec) -> list:
    gath_config = features_dict(
        "dist",
        spawn=agent_spawn_locs((world_size, world_size), "gatherer"),
        r=[gath_vision],
    )
    food_map = features_dict(food_spec[0], **food_spec[1])
    reward_map = features_dict(reward_spec[0], **reward_spec[1])
    conf = [
        f"-d {world_size} {world_size}",
        *map(
            lambda conf_opt: f"{conf_opt[0]} '{json.dumps(conf_opt[1])}'",
            zip(
                ["-g", "-f", "-r"],
                [gath_config, food_map, reward_map],
            ),
        ),
    ]
    hunt_config = None
    if hunt_n > 0:
        hunt_config = features_dict(
            "random",
            spawn=agent_spawn_locs((world_size, world_size), "hunter", n_agent=hunt_n),
        )
    return conf if not hunt_config else [*conf, f"-hn '{json.dumps(hunt_config)}'"]


def env_configs(env_map: dict):
    worlds = env_map["grid_size"]  # range
    vision_r = env_map["vision_rad"]  # range
    hunters = env_map["hunters"]  # range
    food = env_map["food"]  # {food: {random: {...}, {lumps: {...}}}}
    # {reward: {basic: {empty: range0, food: range1, ...}}, basic+walls: {...}}
    for j, (rwd_name, rwd_dict) in enumerate(env_map["reward"].items()):
        for i, comb in enumerate(
            it.product(worlds, vision_r, hunters, food.items(), *rwd_dict.values())
        ):
            comb = list(comb)
            # print(comb)
            food_name, food_props = comb[3]
            if food_name == "lumps":
                comb[3] = (food_name, {**food_props, "grid_size": (comb[0], comb[0])})
            conf = single_env_config(
                *[*comb[:4], [rwd_name, dict(zip(rwd_dict.keys(), comb[4:]))]]
            )
            yield [*conf, f"-s {str(i)+str(j)}"]


def agent_spawn_locs(world_sz: tuple, ag_type: str, n_agent=1) -> list:
    h, w = world_sz
    if ag_type == "gatherer":
        return [(1, w // 2)]
    if n_agent == 1:
        return [(h, 1)]
    hunters = [(h, c) for n in range(n_agent // 2) for c in (n % w + 1, w - n % w)]
    return hunters if not n_agent % 2 else hunters + [(h, w // 2)]


def features_dict(policy: str, **params):
    return {"policy": [policy], "params": {policy: params}}
