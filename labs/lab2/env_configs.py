#!/usr/bin/python

defhunter = {"hunters": [2]}

defv2 = {"vision_rad": [2]}

deffr = {"food": {"random": {"p": 0.4}}}

deffl = {"food": {"lumps": {"spacing": (4, 4), "radius": 2}}}

defenv4 = {"food": {"random": {"p": 0.4}}, "grid_size": [4]}

defrb = {
    "reward": {"basic": {"empty": [-0.005], "food": [0.05], "hunt": [-1], "win": [1]}}
}
defrb2 = {
    "reward": {"basic": {"empty": [-0.001], "food": [0.05], "hunt": [-1], "win": [1]}}
}
defrb3 = {
    "reward": {"basic": {"empty": [-0.01], "food": [0.05], "hunt": [-1], "win": [1]}}
}

defrw = {
    "reward": {
        "basic+walls": {
            "empty": [-0.005],
            "food": [0.05],
            "hunt": [-1],
            "wall": [-0.005],
            "win": [1],
        }
    }
}

defenv8 = {
    **defhunter,
    "grid_size": [8],
    "food": {"lumps": {"spacing": (4, 4), "radius": 2}},
}


env_4_v1 = {**defenv4, **defhunter, **defrb, "vision_rad": [1]}

env_4 = {**defenv4, **defhunter, **defrb, **defv2}

env_4_h0 = {**defenv4, **defv2, **defrb, "hunters": [0]}

env_4_rw = {**defenv4, **defhunter, **defrw, **defv2}

env_8_fl_rw = {**defenv8, **defv2, **defrw}

env_8_v3_fl_rw = {**defenv8, **defrw, "vision_rad": [3]}

env_8_fl = {**defenv8, **defv2, **defrb}

envs = {
    "env_4_v1": env_4_v1,
    "env_4": env_4,
    "env_4_h0": env_4_h0,
    "env_4_rw": env_4_rw,
    "env_8_fl_rw": env_8_fl_rw,
    "env_8_v3_fl_rw": env_8_v3_fl_rw,
    "env_8_fl": env_8_fl,
}


defv3 = {"vision_rad": [3]}

env_4 = {**defenv4, **defhunter, **defrb, **defv2}

env_4_fl = {
    "grid_size": [4],
    "food": {"lumps": {"spacing": (3, 3), "radius": 1}},
    **defhunter,
    **defrb,
    **defv2,
}

env_4_v3 = {**defenv4, **defhunter, **defrb, **defv3}

env_4_rw = {**defenv4, **defhunter, **defrw, **defv2}


env_8_fl = {**defenv8, **defv2, **defrb}

env_8_h0 = {
    "grid_size": [8],
    "food": {"lumps": {"spacing": (4, 4), "radius": 2}},
    **defv2,
    **defrb,
    "hunters": [0],
}


env_8_v3_h0 = {
    "grid_size": [8],
    "food": {"lumps": {"spacing": (4, 4), "radius": 2}},
    **defv3,
    **defrb,
    "hunters": [0],
}

env_8_v3_fl_rw = {**defenv8, **defv3, **defrw}


env_8_v3_fl = {**defenv8, **defv3, **defrb}

env_8_v3 = {"grid_size": [8], **defhunter, **defrb, **deffr, **defv3}

env_8_v3_h3_fl = {"grid_size": [8], "hunters": [3], **defrb, **deffl, **defv3}


env_10_v3 = {"grid_size": [10], **defhunter, **defrb, **deffr, **defv3}

env_10_v3_h3_fl = {"grid_size": [10], "hunters": [3], **defrb, **deffl, **defv3}

env_10_v3_h3 = {"grid_size": [10], "hunters": [3], **defrb, **deffr, **defv3}


envs1 = {
    "env_4": env_4,
    "env_4_fl": env_4_fl,
    "env_4_rw": env_4_rw,
    "env_4_v3": env_4_v3,
    "env_8_fl": env_8_fl,
    "env_8_h0": env_8_h0,
    "env_8_v3_h0": env_8_v3_h0,
    "env_8_v3_fl_rw": env_8_v3_fl_rw,
    "env_8_v3_fl": env_8_v3_fl,
    "env_8_v3": env_8_v3,
    "env_8_v3_h3_fl": env_8_v3_h3_fl,
    "env_10_v3": env_10_v3,
    "env_10_v3_h3_fl": env_10_v3_h3_fl,
}


env_4_v3_fl = {
    "grid_size": [4],
    "food": {"lumps": {"spacing": (3, 3), "radius": 1}},
    **defhunter,
    **defrb,
    **defv3,
}

env_4_v3 = {**defenv4, **defhunter, **defrb, **defv3}

env_4_v3_rw = {**defenv4, **defhunter, **defrw, **defv3}


env_8_v3_rb2 = {"grid_size": [8], **defhunter, **defrb2, **deffr, **defv3}
env_4_v3_rb2 = {**defenv4, **defhunter, **defrb2, **defv3}

env_8_v3_h3 = {"grid_size": [8], "hunters": [3], **defrb, **deffr, **defv3}


envs2 = {
    "env_4_v3_fl": env_4_v3_fl,
    "env_4_v3_rw": env_4_v3_rw,
    "env_4_v3_rb2": env_4_v3_rb2,
    "env_4_v3": env_4_v3,
    "env_8_v3": env_8_v3,
    "env_8_v3_rb2": env_8_v3_rb2,
    "env_8_v3_fl_rw": env_8_v3_fl_rw,
    "env_8_v3_fl": env_8_v3_fl,
    "env_8_v3_h3_fl": env_8_v3_h3_fl,
    "env_8_v3_h3": env_8_v3_h3,
}

env_8_v3_fl_rb3 = {"grid_size": [8], **defhunter, **defrb3, **deffl, **defv3}

envs3 = {
    "env_4": env_4,
    "env_4_v3": env_4_v3,
    "env_8_v3": env_8_v3,
    "env_8_v3_fl": env_8_v3_fl,
    "env_8_v3_h3_fl": env_8_v3_h3_fl,
    "env_8_v3_fl_rw": env_8_v3_fl_rw,
    "env_8_v3_fl_rb3": env_8_v3_fl_rb3,
}

if __name__ == "__main__":
    import combiner as c

    for k, v in envs3.items():
        print(f"CONF :{k}")
        print(list(c.env_configs(v))[0])
        print()
